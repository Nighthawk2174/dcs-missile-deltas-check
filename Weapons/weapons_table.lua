db_path    = db_path or "./Scripts/Database/";
dofile(db_path.."scripts/utils.lua")

-- Gettext
local gettext = i_18n

if gettext then
	if not i18n then -- mega hack whwn it called from ME we don't havee any gettext in global namespace , but have i18n
		gettext.add_package("dcs", "./l10n")
		_ = function (str)		return gettext.dtranslate("dcs", str) 	end
	else
		_ = gettext.translate
	end
else
	_ = function (str)		return str	end
end

function namespace()
  local res = {};
  
  if (__meta_namespace == nil) then
    __meta_namespace = { namespace = true };
  end
  
  setmetatable(res, __meta_namespace);
  
  return res;
end

function dbtype(name, table)
  local res = table;
  local meta;
  
  if name ~= "" then
    meta = { typename = name .. "Descriptor"};
  else
    meta = { anonymous = true };
  end

  if res == nil then
    res = {}
  end
  
  setmetatable(res, meta);
  
  return res;
end

weapons_table 		  = namespace();
weapons_table.weapons = namespace();


dofile (db_path.."Types.lua");
dofile (db_path.."Weapons/weapon_utils.lua")
dofile (db_path.."Weapons/warheads.lua")
-----------------------------------------------------------------
-- guns
-----------------------------------------------------------------
dofile (db_path.."Weapons/shell_table.lua")
dofile (db_path.."Weapons/shell_aiming_table.lua")
dofile (db_path.."Weapons/aircraft_gun_mounts.lua")
dofile (db_path.."Weapons/aircraft_guns.lua")
dofile (db_path.."Weapons/aircraft_gunpods.lua")
----------------------------------------------------------------
dofile (db_path.."Weapons/cluster_data.lua")
dofile (db_path.."Weapons/bombs_table.lua")		-- bombs
dofile (db_path.."Weapons/bombs_data.lua")
dofile (db_path.."Weapons/nurs_table.lua")		-- rockets
dofile (db_path.."Weapons/missiles_table.lua")	-- guided missiles
dofile (db_path.."Weapons/missiles_data.lua")
dofile (db_path.."Weapons/DrawInfo.lua")
dofile (db_path.."Weapons/missiles_prb_coeff.lua")

