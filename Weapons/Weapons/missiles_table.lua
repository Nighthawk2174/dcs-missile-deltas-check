weapons_table.weapons.missiles = namespace();

function form_missile(name, user_name, model, class, level3, scheme, data, add_data,wstype_name)

	local wstype_name = wstype_name
	if  wstype_name == nil then
		wstype_name = _G[name]
	end

	local res = dbtype(class or "wAmmunition",
	{
		ws_type = {wsType_Weapon,wsType_Missile, level3, wstype_name},
		
		model = model,
	})
	
	res.server = 	{}
	res.client = 	{}

	if data.launcher ~= nil then
	    if data.launcher.ammunition_name ~= nil then
		   data.launcher.ammunition = weapons_table.weapons.missiles[data.launcher.ammunition_name]
		end 
	end
	
	-- ��� �����, ������ ��� ��� ���������, ����� �������� ����� ���� ������
	-- ����� � �����
	if data.warhead == nil then data.warhead = {} end
	if data.warhead_air == nil then data.warhead_air = {} end
	
	copy_recursive_with_metatables(res.server, data);
	copy_recursive_with_metatables(res.client, data);

	res.server.scheme = "schemes/missiles/"..scheme..".sch";	
	res.client.scheme = "schemes/missiles/"..scheme..".sch";	
	
	res.server.warhead.fantom = 0;
	res.client.warhead.fantom = 1;
	res.server.warhead_air.fantom = 0;
	res.client.warhead_air.fantom = 1;
	
	res.name 		 = name;
	res.display_name = user_name;
	res.type_name = _("missile");

	if data.launcher ~= nil then
        res.server.launcher.server = 1
        res.client.launcher.server = 0
    end
	
	if not res.caliber then
		res.caliber = data.fm.caliber;
	end
	
	if not res.sounderName then 
		res.sounderName = "Weapons/Missile"
	end
	
	if add_data then
	   copy_recursive(res, add_data);
    end
	
	if not res.add_attributes then
		res.add_attributes = data.add_attributes;
	end
	
	if not res.mass then
		res.mass = data.mass or data.fm.mass
	end
	return res;
end

function declare_missile(name, user_name, model, class, level3, scheme, data, add_data,wstype_name)
	local res = form_missile(name, user_name, model, class, level3, scheme, data, add_data,wstype_name)
	weapons_table.weapons.missiles[res.name] = res
	registerResourceName(res,CAT_MISSILES)
	return res
end
	

declare_missile("P_9M133", _("AT-14 Spriggan"), "9M133", "wAmmunitionVikhr", wsType_SS_Missile, "command_guided_spin_missile", 
{
	controller = {
		boost_start = 0.001,
		march_start = 0.200,
	},
	
	booster = {
		impulse								= 210,
		fuel_mass							= 2.6,
		work_time							= 0.15,
		nozzle_position						= {{-0.5, 0.0, 0.0}},
		nozzle_orientationXYZ				= {{0.0, 0.0, 0.0}},
		tail_width							= 0.05,
		smoke_color							= {0.8, 0.8, 0.8},
		smoke_transparency					= 0.8,
		custom_smoke_dissipation_factor		= 0.0,				
	},
	
	march = {
		impulse								= 240,
		fuel_mass							= 5,
		work_time							= 13,
		nozzle_position						= {{-0.12, 0.071, 0.0},{-0.12, -0.071, 0.0}},
		nozzle_orientationXYZ				= {{0.0, 0.0, 0.15},{0.0, 0.0, -0.15}},
		tail_width							= 0.05,
		smoke_color							= {0.7, 0.7, 0.7},
		smoke_transparency					= 0.08,
		custom_smoke_dissipation_factor		= 0.08,				
	},
	
	spiral_nav = {
		t_cone_near_rad			= 1000,
		def_cone_max_dist		= 5700,
		def_cone_near_rad		= 1000,
		def_cone_near_rad_st	= 0,
		def_cone_time_stab_rad	= 0,
		gb_angle				= 0.005,
		gb_min_dist				= 1.0,
		gb_use_time				= 0.70,
		gb_max_retW				= 0.6,
		gb_ret_Kp				= 1.2,
	},
	
	autopilot = {
		Kp					= 0.24,
		Ki					= 0.038,
		Kd					= 0.06,
		max_ctrl_angle		= 1.35,
		delay				= 0.3,
		op_time				= 23.0,
		fins_discreet		= 0.03,
		no_ctrl_center_ang	= 0.00008,
	},

	fm  = {
		mass        = 26,  
		caliber     = 0.152,
		L           = 1.2,
		I           = 1 / 12 * 26 * 1.2 * 1.2,
		Ma          = 0.85,
		Mw          = 2.75,		
		cx_coeff    = {1,0.29,0.45,0.15,1.12},
		Sw			= 0.125,
		dCydA		= {0.024, 0.018},
		A			= 0.6,
		maxAoa		= 0.2,
		finsTau		= 0.1,
		freq		= 4.0,
	},
	
	eng_err = {
		y_error = 0.0,
		z_error = 0.26,
		min_time_interval = 0.2,
		max_time_interval = 1.1,
	},
	
	warhead = warheads["P_9M133"], 
	warhead_air = warheads["P_9M133"],

}, 
{ mass = 26 });


declare_missile("REFLEX", _("AT-11 Sniper"), "9m119m", "wAmmunitionVikhr", wsType_SS_Missile, "command_guided_spin_missile", 
{
controller = {
		boost_start = 0.001,
		march_start = 0.200,
	},
	
	booster = {
		impulse								= 170,
		fuel_mass							= 2.2,
		work_time							= 0.1,
		nozzle_position						= {{-0.26, 0.0, 0.0}},
		nozzle_orientationXYZ				= {{0.0, 0.0, 0.0}},
		tail_width							= 0.04,
		smoke_color							= {0.8, 0.8, 0.8},
		smoke_transparency					= 0.05,
		custom_smoke_dissipation_factor		= 0.0,				
	},
	
	march = {
		impulse								= 200,
		fuel_mass							= 1.5,
		work_time							= 9,
		nozzle_position						= {{0.18, 0.068, 0.0},{0.18, -0.068, 0.0}},
		nozzle_orientationXYZ				= {{0.0, 0.0, 0.18},{0.0, 0.0, -0.18}},
		tail_width							= 0.04,
		smoke_color							= {0.7, 0.7, 0.7},
		smoke_transparency					= 0.01,
		custom_smoke_dissipation_factor		= 0.01,
	},
	
	spiral_nav = {
		t_cone_near_rad			= 1000,
		def_cone_max_dist		= 5200,
		def_cone_near_rad		= 10,
		def_cone_near_rad_st	= 100,
		def_cone_time_stab_rad	= 5,
		gb_angle				= 0.005,
		gb_min_dist				= 1,
		gb_use_time				= 0.3,
		gb_max_retW				= 0.55,
		gb_ret_Kp				= 1.4,
	},
	
	
	fm = {
		mass        = 17.2,  
		caliber     = 0.125,  
		cx_coeff    = {1,0.22,0.6,0.15,1.28},
		L           = 0.69,
		I           = 1 / 12 * 17.2 * 0.69 * 0.69,
		Ma          = 3.0,
		Mw          = 6.0,
		Sw			= 0.075,
		dCydA		= {0.024, 0.017},
		A			= 0.6,
		maxAoa		= 0.2,
		finsTau		= 0.05,
		freq		= 5.0,
	},
	
	autopilot = {
		Kp					= 0.42,
		Ki					= 0.06,
		Kd					= 0.044,
		max_ctrl_angle		= 1.0,
		delay				= 0.23,
		op_time				= 20.0,
		fins_discreet		= 0.01,
		no_ctrl_center_ang	= 0.00002,
	},
	
	eng_err = {
		y_error = 0.0,
		z_error = 0.1,
		min_time_interval = 0.1,
		max_time_interval = 1.0,
	},

	warhead = warheads["P_9M119"], 
	warhead_air = warheads["P_9M119"],
}, 
{ mass = 17.2 });


declare_missile("SVIR", _("AT-11 Sniper"), "9m119m", "wAmmunitionVikhr", wsType_SS_Missile, "command_guided_spin_missile", 
{
	controller = {
		boost_start = 0.001,
		march_start = 0.250,
	},
	
	booster = {
		impulse								= 170,
		fuel_mass							= 1.8,
		work_time							= 0.1,
		nozzle_position						= {{-0.26, 0.0, 0.0}},
		nozzle_orientationXYZ				= {{0.0, 0.0, 0.0}},
		tail_width							= 0.04,
		smoke_color							= {0.6, 0.6, 0.6},
		smoke_transparency					= 0.05,
		custom_smoke_dissipation_factor		= 0.0,				
	},
	
	march = {
		impulse								= 200,
		fuel_mass							= 1.5,
		work_time							= 9,
		nozzle_position						= {{0.18, 0.068, 0.0},{0.18, -0.068, 0.0}},
		nozzle_orientationXYZ				= {{0.0, 0.0, 0.18},{0.0, 0.0, -0.18}},
		tail_width							= 0.04,
		smoke_color							= {0.7, 0.7, 0.7},
		smoke_transparency					= 0.01,
		custom_smoke_dissipation_factor		= 0.01,				
	},
	
	spiral_nav = {
		t_cone_near_rad			= 1000,
		def_cone_max_dist		= 4200,
		def_cone_near_rad		= 10,
		def_cone_near_rad_st	= 100,
		def_cone_time_stab_rad	= 5,
		gb_angle				= 0.01,
		gb_min_dist				= 1,
		gb_use_time				= 0.3,
		gb_max_retW				= 0.55,
		gb_ret_Kp				= 1.2,
	},
	
	
	autopilot = {
		Kp					= 0.45,
		Ki					= 0.05,
		Kd					= 0.055,
		max_ctrl_angle		= 1.0,
		delay				= 0.23,
		op_time				= 20.0,
		fins_discreet		= 0.01,
		no_ctrl_center_ang	= 0.00003,
	},

	fm = {
		mass        = 17.2,  
		caliber     = 0.125,  
		cx_coeff    = {1,0.22,0.6,0.15,1.28},
		L           = 0.69,
		I           = 1 / 12 * 17.2 * 0.69 * 0.69,
		Ma          = 2.0,
		Mw          = 5.0,
		Sw			= 0.07,
		dCydA		= {0.024, 0.017},
		A			= 0.6,
		maxAoa		= 0.2,
		finsTau		= 0.05,
		freq		= 5.0,
	},
	
	eng_err = {
		y_error = 0.0,
		z_error = 0.1,
		min_time_interval = 0.1,
		max_time_interval = 1.0,
	},

	warhead = warheads["P_9M119"], 
	warhead_air = warheads["P_9M119"],
}, 
{ mass = 17.2 });


declare_missile("P_9M117", _("AT-10 Stabber"), "9m117", "wAmmunitionVikhr", wsType_SS_Missile, "command_guided_spin_missile", 
{
		controller = {
		boost_start = 0.001,
		march_start = 0.200,
	},
	
	booster = {
		impulse								= 190,
		fuel_mass							= 2.4,
		work_time							= 0.2,
		nozzle_position						= {{-0.26, 0.0, 0.0}},
		nozzle_orientationXYZ				= {{0.0, 0.0, 0.0}},
		tail_width							= 0.04,
		smoke_color							= {0.7, 0.7, 0.7},
		smoke_transparency					= 0.05,
		custom_smoke_dissipation_factor		= 0.0,				
	},
	
	march = {
		impulse								= 190,
		fuel_mass							= 1.7,
		work_time							= 6,
		nozzle_position						= {{-0.02, 0.066, 0.0},{-0.02, -0.066, 0.0}},
		nozzle_orientationXYZ				= {{0.0, 0.0, 0.18},{0.0, 0.0, -0.18}},
		tail_width							= 0.04,
		smoke_color							= {0.7, 0.7, 0.7},
		smoke_transparency					= 0.01,
		custom_smoke_dissipation_factor		= 0.01,
	},
	
	spiral_nav = {
		t_cone_near_rad			= 1000,
		def_cone_max_dist		= 4200,
		def_cone_near_rad		= 10,
		def_cone_near_rad_st	= 100,
		def_cone_time_stab_rad	= 5,
		gb_angle				= 0.003,
		gb_min_dist				= 1,
		gb_use_time				= 0.6,
		gb_max_retW				= 0.6,
		gb_ret_Kp				= 1.15,
	},
		
	fm = {
		mass        = 17.6,  
		caliber     = 0.100,  
		cx_coeff    = {1,0.3,0.56,0.13,1.2},
		L           = 1.048,
		I           = 1 / 12 * 17.2 * 1.048 * 1.048,
		Ma          = 2.0,
		Mw          = 5.0,
		Sw			= 0.11,
		dCydA		= {0.024, 0.016},
		A			= 0.6,
		maxAoa		= 0.2,
		finsTau		= 0.05,
		freq		= 6,
	},

	autopilot = {
		Kp					= 0.2,
		Ki					= 0.05,
		Kd					= 0.05,
		max_ctrl_angle		= 1.0,
		delay				= 0.23,
		op_time				= 17.0,
		fins_discreet		= 0.04,
		no_ctrl_center_ang	= 0.00003,
	},
	
	eng_err = {
		y_error = 0.0,
		z_error = 0.2,
		min_time_interval = 0.1,
		max_time_interval = 0.8,
	},

	warhead = warheads["P_9M117"], 
	warhead_air = warheads["P_9M117"],
}, 
{ mass = 17.6 }); 


declare_missile("KONKURS", _("AT-5 Spandrel"), "9m113", "wAmmunitionVikhr", wsType_SS_Missile, "command_guided_spin_missile", 
{
	controller = {
		boost_start = 0.001,
		march_start = 0.300,
	},
	
	booster = {
		impulse								= 130,
		fuel_mass							= 2.5,
		work_time							= 0.04,
		nozzle_position						= {{-0.5, 0.0, 0.0}},
		nozzle_orientationXYZ				= {{0.0, 0.0, 0.0}},
		tail_width							= 0.035,
		smoke_color							= {0.8, 0.8, 0.8},
		smoke_transparency					= 0.05,
		custom_smoke_dissipation_factor		= 0.0,				
	},
	
	march = {
		impulse								= 140,
		fuel_mass							= 2.0,
		work_time							= 20,
		nozzle_position						= {{-0.22, 0.0, 0.062},{-0.22, 0.0, -0.062}},
		nozzle_orientationXYZ				= {{0.0, -0.15, 0.0},{0.0, 0.15, 0.0}},
		tail_width							= 0.045,
		smoke_color							= {0.7, 0.7, 0.7},
		smoke_transparency					= 0.013,
		custom_smoke_dissipation_factor		= 0.013,				
	},
	
	spiral_nav = {
		t_cone_near_rad			= 1000,
		def_cone_max_dist		= 4000,
		def_cone_near_rad		= 1000,
		def_cone_near_rad_st	= 0,
		def_cone_time_stab_rad	= 0,
		gb_angle				= 0.003,
		gb_min_dist				= 1,
		gb_use_time				= 0.7,
		gb_max_retW				= 0.4,
		gb_ret_Kp				= 1,
	},
	
		
	fm  = {
		mass        = 14,  
		caliber     = 0.135,
		L           = 1.0,
		I           = 1 / 12 * 14.0 * 1.0 * 1.0,
		Ma          = 2.0,
		Mw          = 5.0,		
		cx_coeff    = {1,0.5,0.4,0.3,1.15},
		dCydA		= {0.024, 0.017},
		rail_open	= 0,
		A			= 0.6,
		Sw			= 0.3,
		maxAoa		= 0.25,
		finsTau		= 0.05,
		freq		= 7,
	},
	
	autopilot = {
		Kp					= 0.12,
		Ki					= 0.038,
		Kd					= 0.023,
		max_ctrl_angle		= 1.0,
		delay				= 0.2,
		op_time				= 22.0,
		fins_discreet		= 0.04,
		no_ctrl_center_ang	= 0.00011,
	},	
	
	eng_err = {
		y_error = 0.0,
		z_error = 0.3,
		min_time_interval = 0.1,
		max_time_interval = 0.9,
	},
	
	warhead = warheads["KONKURS"], 
	warhead_air = warheads["KONKURS"],
}, 
{ mass = 14.6 });


declare_missile("MALUTKA", _("AT-3 Sagger"), "malutka", "wAmmunitionVikhr", wsType_SS_Missile, "command_guided_spin_missile", 
{
	controller = {
		boost_start = 0.001,
		march_start = 0.100,
	},
	
	booster = {
		impulse								= 110,
		fuel_mass							= 1,
		work_time							= 0.1,
		nozzle_position						= {{-0.4, 0.0, 0.0}},
		nozzle_orientationXYZ				= {{0.0, 0.0, 0.0}},
		tail_width							= 0.06,
		smoke_color							= {0.8, 0.8, 0.8},
		smoke_transparency					= 0.1,
		custom_smoke_dissipation_factor		= 0.0,				
	},
	
	march = {
		impulse								= 110,
		fuel_mass							= 1.4,
		work_time							= 26.5,
		nozzle_position						= {{-0.14, 0.065, 0.0},{-0.14, -0.065, 0.0},{-0.14, 0.0, 0.065},{-0.14, 0.0, -0.065}},
		nozzle_orientationXYZ				= {{0.0, 0.0, 0.15},{0.0, 0.0, -0.15},{0.0, -0.15, 0.0},{0.0, 0.15, 0.0}},
		tail_width							= 0.06,
		smoke_color							= {0.8, 0.8, 0.8},
		smoke_transparency					= 0.013,
		custom_smoke_dissipation_factor		= 0.013,
	},
	
	spiral_nav = {
		t_cone_near_rad			= 1000,
		def_cone_max_dist		= 3000,
		def_cone_near_rad		= 1000,
		def_cone_near_rad_st	= 0,
		def_cone_time_stab_rad	= 0,
		gb_angle				= 0.018,
		gb_min_dist				= 1,
		gb_use_time				= 1.4,
		gb_max_retW				= 0.44,
		gb_ret_Kp				= 0.95,
	},
	
	fm  = {
		mass        = 11,  
		caliber     = 0.125,
		L           = 0.87,
		I           = 1 / 12 * 11 * 0.87 * 0.87,
		Ma          = 4.0,
		Mw          = 5.0,	
		cx_coeff    = {1,0.6,0.35,0.4,1.05},
		rail_open	= 0,
		Sw			= 0.35,
		dCydA		= {0.032, 0.024},
		maxAoa		= 0.2,
		finsTau		= 0.05,
		A			= 0.6,
		freq		= 8.5,
	},

	autopilot = {
		Kp					= 0.18,
		Ki					= 0.026,
		Kd					= 0.046,
		max_ctrl_angle		= 1.0,
		delay				= 0.5,
		op_time				= 33.0,
		fins_discreet		= 0.01,
		no_ctrl_center_ang	= 0.0002,
	},
	
	eng_err = {
		y_error = 0.28,
		z_error = 0.36,
		min_time_interval = 0.1,
		max_time_interval = 0.36,
	},
	
	warhead = warheads["MALUTKA"], 
	warhead_air = warheads["MALUTKA"], 

}, 
{ mass = 11 });


declare_missile("TOW2", _("BGM-71 TOW"), "bgm-71e", "wAmmunitionVikhr", wsType_SS_Missile, "command_guided_missile_sfe", 
{
	controller = {
		boost_start = 0.001,
		march_start = 0.13,
	},
	
	booster = {
		impulse								= 160,
		fuel_mass							= 0.8,
		work_time							= 0.04,
		boost_time							= 0,
		boost_factor						= 0,
		nozzle_position						= {{-0.8, 0.0, 0}},
		nozzle_orientationXYZ				= {{0.0, 0.0, 0.0}},
		tail_width							= 0.045,
		smoke_color							= {0.8, 0.8, 0.8},
		smoke_transparency					= 0.2,
		custom_smoke_dissipation_factor		= 0.0,				
	},
	
	march = {
		impulse								= 200,
		fuel_mass							= 2.6,
		work_time							= 1.5,
		boost_time							= 0,
		boost_factor						= 0,
		nozzle_position						= {{-0.23, 0.0, 0.062},{-0.23, 0.0, -0.062}},
		nozzle_orientationXYZ				= {{0.0, -0.9, 0.0},{0.0, 0.9, 0.0}},
		tail_width							= 0.045,
		smoke_color							= {0.7, 0.7, 0.7},
		smoke_transparency					= 0.013,
		custom_smoke_dissipation_factor		= 0.013,
	},
	
	spiral_nav = {
		t_cone_near_rad			= 1000,
		def_cone_max_dist		= 3800,
		def_cone_near_rad		= 1000,
		def_cone_near_rad_st	= 0,
		def_cone_time_stab_rad	= 0,
		gb_angle				= 0.026,
		gb_min_dist				= 1,
		gb_use_time				= 0.28,
		gb_max_retW				= 0.5,
		gb_ret_Kp				= 2.5,
	},
	
	
	autopilot = {
		Kp					= 0.3,
		Ki					= 0.03,
		Kd					= 0.03,
		max_ctrl_angle		= 1.1,
		delay				= 0.2,
		op_time				= 23.0,
		fins_discreet		= 0.01,
		no_ctrl_center_ang	= 0.000035,
	},

	fm = {
		mass        = 21.5,  
		caliber     = 0.152,  
		cx_coeff    = {1,0.28,0.43,0.18,1.12},
		L           = 1.17,
		I           = 1 / 12 * 21.5 * 1.17 * 1.17,
		Ma          = 2.0,
		Mw          = 5.0,
		wind_sigma	= 0.0,
		wind_time	= 0.0,
		Sw			= 0.085,
		dCydA		= {0.024, 0.018},
		A			= 0.6,
		maxAoa		= 0.28,
		finsTau		= 0.05,
		lockRoll	= 1,
	},

	err = {
		y_error = 0.08,
		z_error = 0.02,
		min_time_interval = 0.1,
		max_time_interval = 0.9,
	},
	
	warhead = warheads["TOW"],
	warhead_air = warheads["TOW"]
}, 
{ mass = 21.5 });

declare_missile("AT_6", _("AT-6"), "9m114", "wAmmunitionVikhr", wsType_AS_Missile, "shturm_new", 
{
	booster = {
		impulse								= 220,
		fuel_mass							= 1,
		work_time							= 0.05,
		nozzle_position						= {{-1.0, 0.0, 0.0}},
		tail_width							= 0.3,
		smoke_color							= {0.8, 0.8, 0.8},
		smoke_transparency					= 0.8,
		custom_smoke_dissipation_factor		= 0.0,				
	},
	
	march = {
		impulse								= 240,
		fuel_mass							= 5.0,
		work_time							= 2,
		nozzle_position						= {{-0.45, 0.0, 0.07},{-0.45, 0.0,-0.07}},
		nozzle_orientationXYZ				= {{0.0, -0.2, 0.0},{0.0, 0.2, 0.0}},
		tail_width							= 0.06,
		smoke_color							= {0.8, 0.8, 0.8},
		smoke_transparency					= 0.1,
		custom_smoke_dissipation_factor		= 0.3,				
	},
	
	march2 = {
		impulse								= 250,
		fuel_mass							= 3.6,
		work_time							= 2.5,
		nozzle_position						= {{-0.45, 0.0, 0.07},{-0.45, 0.0, -0.07}},
		nozzle_orientationXYZ				= {{0.0, -0.2, 0.0},{0.0, 0.2, 0.0}},
		tail_width							= 0.06,
		smoke_color							= {0.8, 0.8, 0.8},
		smoke_transparency					= 0.1,
		custom_smoke_dissipation_factor		= 0.3,				
	},
	
	march_smoke = {
		impulse								= 240,
		fuel_mass							= 0.6,
		work_time							= 0.5,
		nozzle_position						= {{-0.45, 0.0, 0.1},{-0.45, 0.0, -0.1}},
		nozzle_orientationXYZ				= {{0.0, -0.2, 0.15},{0.0, 0.2, 0.0}},
		tail_width							= 0.4,
		smoke_color							= {0.1, 0.1, 0.1},
		smoke_transparency					= 0.1,
		custom_smoke_dissipation_factor		= 0.4,				
	},
	
	spiral_nav = {
		t_cone_near_rad			= 1000,
		def_cone_max_dist		= 6500,
		def_cone_near_rad		= 1000,
		def_cone_near_rad_st	= 0,
		def_cone_time_stab_rad	= 0,
		gb_angle				= 0.0036,
		gb_min_dist				= 3200,
		gb_use_time				= 5.3,
		gb_max_retW				= 0.4,
		gb_ret_Kp				= 1.1,
	},
	
	autopilot = {
		Kp					= 0.062,
		Ki					= 0.056,
		Kd					= 0.036,
		max_ctrl_angle		= 1.15,
		delay				= 0.7,
		op_time				= 18.0,
		fins_discreet		= 0.08,
		no_ctrl_center_ang	= 0.00002,
	},
	

	fm  = {
		mass				= 31,  
		caliber    			= 0.3,
		L           		= 1.630,
		I           		= 1 / 12 * 31 * 1.63 * 1.63,
		Ma         			= 2,
		Mw         			= 5,		
		cx_coeff    		= {1,0.36,1.20,0.15,1.6},
		Sw					= 0.12,
		dCydA				= {0.024, 0.018},
		A					= 0.6,
		maxAoa				= 0.2,
		finsTau				= 0.05,
		freq				= 4.0,
	},
	
	eng_err = {
		y_error				= 0.0,
		z_error				= 0.1,
		min_time_interval	= 0.1,
		max_time_interval	= 1.0,
	},
	
	warhead		= warheads["AT_6"], 
	warhead_air = warheads["AT_6"], 
}, 
{ mass = 31 });


declare_missile("Vikhr_M", _("Vikhr M"), "vichr", "wAmmunitionVikhr", wsType_AS_Missile, "command_guided_spin_missile", 
{
	controller = {
		boost_start = 0.001,
		march_start = 0.500,
	},
	
	booster = {
		impulse								= 220,
		fuel_mass							= 5.1,
		work_time							= 0.5,
		nozzle_position						= {{-1.2, 0.0, 0.0}},
		nozzle_orientationXYZ				= {{0.0, 0.0, 0.0}},
		tail_width							= 0.2,
		smoke_color							= {1.0, 1.0, 1.0},
		smoke_transparency					= 0.8,
		custom_smoke_dissipation_factor		= 0.0,				
	},
	
	march = {
		impulse								= 240,
		fuel_mass							= 8.1,
		work_time							= 6.3,
		nozzle_position						= {{0.2, 0.08, 0.0},{0.2, -0.086, 0.0}},
		nozzle_orientationXYZ				= {{0.0, 0.0, 0.15},{0.0, 0.0, -0.15}},
		tail_width							= 0.05,
		smoke_color							= {1.0, 1.0, 1.0},
		smoke_transparency					= 0.1,
		custom_smoke_dissipation_factor		= 0.2,				
	},
	
	spiral_nav = {
		def_cone_max_dist		= 8500,
		t_cone_near_rad			= 1000,
		def_cone_near_rad		= 15,
		def_cone_near_rad_st	= 500,
		def_cone_time_stab_rad	= 5,
		gb_angle				= 0.0,
		gb_min_dist				= 0.0,
		gb_use_time				= 0.0,
		gb_max_retW				= 0.0,
		gb_ret_Kp				= 0.0,
	},
	
	autopilot = {
		Kp					= 0.078,
		Ki					= 0.058,
		Kd					= 0.038,
		max_ctrl_angle		= 1.35,
		delay				= 0.2,
		op_time				= 24.0,
		fins_discreet		= 0.04,
		no_ctrl_center_ang	= 0.00004,
	},

	fm  = {
		mass        = 45,  
		caliber     = 0.13,
		L           = 2.75,
		I           = 1 / 12 * 45 * 2.75 * 2.75,
		Ma          = 2,
		Mw          = 5,		
		cx_coeff    = {1,0.65,0.85,0.85,1.4},
		Sw			= 0.1,
		dCydA		= {0.024, 0.018},
		A			= 0.6,
		maxAoa		= 0.2,
		finsTau		= 0.1,
		freq		= 2.0,
	},
	
	eng_err = {
		y_error = 0.0,
		z_error = 0.0,
		min_time_interval = 0.1,
		max_time_interval = 1.0,
	},
	
	warhead = warheads["Vikhr_M"], 
	warhead_air = warheads["Vikhr_M"], 

}, 
{ mass = 45 });

--[[]]
declare_missile("Igla_1E", _("Igla-S"), "9M39", "wAmmunitionSelfHoming", wsType_SA_Missile, "self_homing_spin_missile2", 
{
	controller = {
		boost_start = 0.001,
		march_start = 0.40,
		march2_start = 2.0,
	},
	
	booster = {
		impulse								= 170,
		fuel_mass							= 0.22,
		work_time							= 0.048,
		boost_time							= 0,
		boost_factor						= 0,
		nozzle_position						= {{-0.8, 0.0, 0}},
		nozzle_orientationXYZ				= {{0.0, 0.0, 0.0}},
		tail_width							= 0.4,
		smoke_color							= {1.0, 1.0, 1.0},
		smoke_transparency					= 0.9,
		custom_smoke_dissipation_factor		= 0.3,				
	},
		
	march = {
		impulse								= 225,
		fuel_mass							= 2.25,
		work_time							= 1.9,
		boost_time							= 0,
		boost_factor						= 0,
		nozzle_position						= {{-0.8, 0.0, 0.0}},
		nozzle_orientationXYZ				= {{0.0, 0.0, 0.0}},
		tail_width							= 0.2,
		smoke_color							= {1.0, 1.0, 1.0},
		smoke_transparency					= 0.9,
		custom_smoke_dissipation_factor		= 0.3,	
	},
	
	march2 = {
		impulse								= 207,
		fuel_mass							= 2.23,
		work_time							= 6.6,
		boost_time							= 0,
		boost_factor						= 0,
		nozzle_position						= {{-0.8, 0.0, 0.0}},
		nozzle_orientationXYZ				= {{0.0, 0.0, 0.0}},
		tail_width							= 0.2,
		smoke_color							= {1.0, 1.0, 1.0},
		smoke_transparency					= 0.7,
		custom_smoke_dissipation_factor		= 0.3,	
	},

	fm = {
		mass        = 10.6,  
		caliber     = 0.072,  
		cx_coeff    = {1,1.15,0.6,0.4,1.5},
		L           = 1.68,
		I           = 1 / 12 * 10.6 * 1.68 * 1.68,
		Ma          = 0.6,
		Mw          = 1.2,
		Sw			= 0.2,
		dCydA		= {0.07, 0.036},
		A			= 0.6,
		maxAoa		= 0.3,
		finsTau		= 0.1,
		freq		= 20,
	},
	
	simple_IR_seeker = {
		sensitivity		= 10500,
		cooled			= true,
		delay			= 0.0,
		GimbLim			= math.rad(30),
		FOV				= math.rad(2);
		opTime			= 14.0,
		target_H_min	= 0.0,
		flag_dist		= 150.0,
		abs_err_val		= 4,
		ground_err_k	= 3,
	},
	
	simple_gyrostab_seeker = {
		omega_max	= math.rad(8)
	},
	
	fuze_proximity = {
		ignore_inp_armed	= 1,
		radius				= 3,
	},
	
	autopilot = {
		K				= 1.4,
		Kg				= 0.2,
		Ki				= 0.0,
		finsLimit		= 0.05,
		delay			= 0.5,
		fin2_coeff		= 0.5,
	},
	
	warhead = warheads["Igla_1E"],
	warhead_air = warheads["Igla_1E"]
}, 

{ mass = 10.6 }
);


declare_missile("FIM_92C", _("FIM-92B"), "fim-92", "wAmmunitionSelfHoming", wsType_SA_Missile, "self_homing_spin_missile2", 
{
	controller = {
		boost_start = 0.001,
		march_start = 0.40,
		march2_start = 1.8,
	},
	
	booster = {
		impulse								= 170,
		fuel_mass							= 0.22,
		work_time							= 0.048,
		boost_time							= 0,
		boost_factor						= 0,
		nozzle_position						= {{-0.8, 0.0, 0.0}},
		nozzle_orientationXYZ				= {{0.0, 0.0, 0.0}},
		tail_width							= 0.4,
		smoke_color							= {1.0, 1.0, 1.0},
		smoke_transparency					= 0.9,
		custom_smoke_dissipation_factor		= 0.3,				
	},
		
	march = {
		impulse								= 250,
		fuel_mass							= 2.52,
		work_time							= 1.4,
		boost_time							= 0,
		boost_factor						= 0,
		nozzle_position						= {{-0.8, 0.0, 0.0}},
		nozzle_orientationXYZ				= {{0.0, 0.0, 0.0}},
		tail_width							= 0.2,
		smoke_color							= {1.0, 1.0, 1.0},
		smoke_transparency					= 0.9,
		custom_smoke_dissipation_factor		= 0.3,	
	},
	
	march2 = {
		impulse								= 225,
		fuel_mass							= 2.04,
		work_time							= 5.1,
		boost_time							= 0,
		boost_factor						= 0,
		nozzle_position						= {{-0.8, 0.0, 0.0}},
		nozzle_orientationXYZ				= {{0.0, 0.0, 0.0}},
		tail_width							= 0.2,
		smoke_color							= {1.0, 1.0, 1.0},
		smoke_transparency					= 0.7,
		custom_smoke_dissipation_factor		= 0.3,	
	},

	fm = {
		mass        = 10.1,  
		caliber     = 0.072,  
		cx_coeff    = {1,1.15,0.8,0.4,1.5},
		L           = 1.52,
		I           = 1 / 12 * 10.6 * 1.52 * 1.52,
		Ma          = 0.6,
		Mw          = 1.2,
		Sw			= 0.2,
		dCydA		= {0.07, 0.036},
		A			= 0.6,
		maxAoa		= 0.3,
		finsTau		= 0.1,
		freq		= 20,
	},
	
	simple_IR_seeker = {
		sensitivity		= 9500,
		cooled			= true,
		delay			= 0.0,
		GimbLim			= math.rad(30),
		FOV				= math.rad(2);
		opTime			= 15.0,
		target_H_min	= 0.0,
		flag_dist		= 150.0,
		abs_err_val		= 4,
		ground_err_k	= 3,
	},
	
	simple_gyrostab_seeker = {
		omega_max	= math.rad(8)
	},
	
	fuze_proximity = {
		ignore_inp_armed	= 0,
		radius				= 0,
	},
	
	autopilot = {
		K				= 2.0,
		Kg				= 0.1,
		Ki				= 0.0,
		finsLimit		= 0.2,
		delay 			= 0.5,
		fin2_coeff		= 0.5,
	},
	
	warhead = warheads["FIM_92C"],
	warhead_air = warheads["FIM_92C"]
}, 

{ mass = 10.1 }
);


declare_missile("MIM_72G", _("MIM-72G"), "mim-72", "wAmmunitionSelfHoming", wsType_SA_Missile, "IR_seeker_stab_missile", 
{
	controller = {
		boost_start = 0.001,
		march_start = 0.501,
	},
	
	booster = {
		impulse								= 160,
		fuel_mass							= 2.0,
		work_time							= 0.5,
		boost_time							= 0,
		boost_factor						= 0,
		nozzle_position						= {{-1.6, 0.0, 0}},
		nozzle_orientationXYZ				= {{0.0, 0.0, 0.0}},
		tail_width							= 0.4,
		smoke_color							= {1.0, 1.0, 1.0},
		smoke_transparency					= 0.9,
		custom_smoke_dissipation_factor		= 0.3,				
	},
		
	march = {
		impulse								= 160,
		fuel_mass							= 27,
		work_time							= 9,
		boost_time							= 0,
		boost_factor						= 0,
		nozzle_position						= {{-1.6, 0.0, 0}},
		nozzle_orientationXYZ				= {{0.0, 0.0, 0.0}},
		tail_width							= 0.2,
		smoke_color							= {1.0, 1.0, 1.0},
		smoke_transparency					= 0.9,
		custom_smoke_dissipation_factor		= 0.3,	
	},

	fm = {
		mass        = 86,  
		caliber     = 0.127,  
		cx_coeff    = {1,0.45,1.0,0.65,1.3},
		L           = 2.9,
		I           = 1 / 12 * 86 * 2.9 * 2.9,
		Ma          = 0.6,
		Mw          = 1.2,
		Sw			= 0.2,
		dCydA		= {0.07, 0.036},
		A			= 0.6,
		maxAoa		= 0.25,
		finsTau		= 0.1,
		Ma_x		= 0.001,
		Kw_x		= 0.001,
		I_x			= 40,
	},
	
	simple_IR_seeker = {
		sensitivity		= 9500,
		cooled			= true,
		delay			= 0.0,
		GimbLim			= math.rad(30),
		FOV				= math.rad(7)*2;
		opTime			= 15.0,
		target_H_min	= 0.0,
		flag_dist		= 150.0,
		abs_err_val		= 4,
		ground_err_k	= 3,
	},
	
	simple_gyrostab_seeker = {
		omega_max	= math.rad(8)
	},
	
	fuze_proximity = {
		ignore_inp_armed	= 0,
		radius				= 0,
	},
	
	autopilot = {
		delay 			= 1.5,
		K				= 2.6,
		Kg				= 0.15,
		Ki				= 0.0001,
		finsLimit		= 0.44,
		fin2_coeff		= 0.5,
	},
	
	warhead = warheads["MIM_72G"],
	warhead_air = warheads["MIM_72G"]
}, 

{ mass = 86 }
);


declare_missile("AGM_122", _("AGM-122"), "agm-122", "wAmmunitionSelfHoming", wsType_AS_Missile, "anti_radiation_missile2", 
{
	controller = {
		march_start = 0.1,
	},

	march = {
		impulse								= 140,
		fuel_mass							= 27.2,
		work_time							= 5,
		boost_time							= 0,
		boost_factor						= 0,
		nozzle_position						= {{-1.45, 0.0, 0}},
		nozzle_orientationXYZ				= {{0.0, 0.0, 0.0}},
		tail_width							= 0.1,
		smoke_color							= {0.8, 0.8, 0.8},
		smoke_transparency					= 0.9,
		custom_smoke_dissipation_factor		= 0.2,	
	},
	
	
	fm = {
		mass        = 88,  
		caliber     = 0.127,  
		cx_coeff    = {1,1.5,0.56,0.28,1.8},
		L           = 3.02,
		I           = 1 / 12 * 127 * 3.02 * 3.02,
		Ma          = 0.3,
		Mw          = 1.2,
		wind_sigma	= 0.0,
		wind_time	= 0.0,
		Sw			= 0.36,
		dCydA		= {0.07, 0.036},
		A			= 0.36,
		maxAoa		= 0.24,
		finsTau		= 0.1,
		Kw_x		= 0.001,
		Ma_x		= 0.001,
	},
	
	radio_seeker = {
		FOV					= math.rad(15),
		op_time				= 200,
		keep_aim_time		= 4,
		pos_memory_time		= 60,
		sens_near_dist		= 200.0,
		sens_far_dist		= 20000.0,
		err_correct_time	= 1.2,		-- The time interval of calcuulation a random  aiming error, seconds
		err_val				= 0.0012,	-- Random  aiming error coefficient
		lock_err_val		= 0.06,
		calc_aim_dist		= 500000,
		blind_rad_val		= 0.2,
		blind_ctrl_dist		= 1000,
		aim_y_offset		= 2.2,
		min_sens_rad_val	= 0.00025,
	},
	
	simple_gyrostab_seeker = {
		omega_max	= math.rad(8)
	},
	
	fuze_proximity = {
		ignore_inp_armed	= 1,
		arm_delay			= 2,
		radius				= 4,
	},
	
	autopilot = {
		K				 = 90.0,
		Kg				 = 4.0,
		Ki				 = 0.0,
		finsLimit		 = 0.22,
		useJumpByDefault = 0,
		J_Power_K		 = 0.0,
		J_Diff_K		 = 0.0,
		J_Int_K			 = 0.0,
		J_Angle_K		 = 0.0,
		J_FinAngle_K	 = 0.0,
		J_Angle_W		 = 0.0,
		delay			 = 0.5,
	},
	
	start_helper = {
		delay				= 0.5,
		power				= 0.35,
		time				= 1,
		use_local_coord		= 0,
		max_vel				= 100,
		max_height			= 150,
		vh_logic_or			= 0,
	},
	
	warhead = warheads["AGM_122"],
	warhead_air = warheads["AGM_122"]
}, 

{ mass = 88 }
);


declare_missile("AGM_88", _('AGM-88C'), "AGM-88", "wAmmunitionSelfHoming", wsType_AS_Missile, "anti_rad_missile",
{
	controller = {
		boost_start = 0.5,
		march_start = 1.5,
	},
	
	boost = {
		impulse								= 235,
		fuel_mass							= 25.5,
		work_time							= 1.0,
		nozzle_position						= {{-2.1, 0, 0}},
		nozzle_orientationXYZ				= {{0.0, 0.0, 0.0}},
		tail_width							= 0.4,
		smoke_color							= {0.9, 0.9, 0.9},
		smoke_transparency					= 0.1,
		custom_smoke_dissipation_factor		= 0.3,
	},
	
	march = {
		impulse								= 226,
		fuel_mass							= 101.5,
		work_time							= 20.4,
		nozzle_position						= {{-2.1, 0, 0}},
		nozzle_orientationXYZ				= {{0.0, 0.0, 0.0}},
		tail_width							= 0.3,
		smoke_color							= {0.8, 0.8, 0.8},
		smoke_transparency					= 0.1,
		custom_smoke_dissipation_factor		= 0.3,	
	},
	
	fm = {
		mass				= 361,  
		caliber				= 0.254,  
		wind_sigma			= 0.0,
		wind_time			= 0.0,
		tail_first			= 1,
		fins_part_val		= 0,
		rotated_fins_inp	= 0,
		delta_max			= math.rad(20),
		L					= 0.25,
		S					= 0.051,
		Ix					= 3.5,
		Iy					= 320.5,
		Iz					= 320.5,
		
		Mxd					= 0.3 * 57.3,
		Mxw					= -44.5,

		table_scale	= 0.2,
		table_degree_values = 1,
	--	Mach	  | 0.0		0.2		0.4		0.6		0.8		1.0		1.2		1.4		1.6		1.8		2.0		2.2		2.4		2.6		2.8		3.0		3.2		3.4		3.6		3.8		4.0	 |
		Cx0 	= {	0.34,	0.34,	0.34,	0.34,	0.35,	1.10,	1.27,	1.23,	1.19,	1.12,	1.05,	1.0,	0.95,	0.91,	0.87,	0.84,	0.81,	0.78,	0.76,	0.74,	0.72 },
		CxB 	= {	0.11,	0.11,	0.11,	0.11,	0.11,	0.40,	0.19,	0.17,	0.16,	0.14,	0.13,	0.12,	0.12,	0.11,	0.11,	0.10,	0.09,	0.09,	0.08,	0.08,	0.07 },
		K1		= { 0.0056,	0.0056,	0.0056,	0.0056,	0.0056,	0.0056,	0.0056,	0.0056,	0.0052,	0.0048,	0.0045,	0.0041,	0.0037,	0.0036,	0.0034,	0.0032,	0.0031,	0.0030,	0.0029,	0.0027,	0.0026 },
		K2		= { 0.0055,	0.0055,	0.0055,	0.0055,	0.0055,	0.0055,	0.0055,	0.0055,	0.0051,	0.0047,	0.0043,	0.0037,	0.0031,	0.0032,	0.0033,	0.0035,	0.0036,	0.0037,	0.0038,	0.0039,	0.0040 },
		Cya		= { 1.14,	1.14,	1.14,	1.14,	1.14,	1.42,	1.46,	1.39,	1.32,	1.15,	1.06,	1.00,	0.94,	0.89,	0.83,	0.78,	0.73,	0.69,	0.65,	0.61,	0.57 },
		Cza		= { 1.14,	1.14,	1.14,	1.14,	1.14,	1.42,	1.46,	1.39,	1.32,	1.15,	1.06,	1.00,	0.94,	0.89,	0.83,	0.78,	0.73,	0.69,	0.65,	0.61,	0.57 },
		Mya		= { -0.5,	-0.5},
		Mza		= { -0.5,	-0.5},
		Myw		= { -2.0,	-2.0},
		Mzw		= { -2.0,	-2.0},
		A1trim	= { 10.0,	10.0},
		A2trim	= { 10.0,	10.0},
		
		model_roll = math.rad(45),
		fins_stall = 1,
	},
	
	proximity_fuze = {
		radius		= 8,
		arm_delay	= 1.6,
	},
	
	seeker = {
		delay					= 2.4,
		op_time					= 240,
		FOV						= math.rad(60),
		max_w_LOS				= math.rad(20),
		sens_near_dist			= 100,
		sens_far_dist			= 70000,
		
		keep_aim_time		= 5,
		pos_memory_time		= 150,
		err_correct_time	= 2.0,
		calc_aim_dist		= 500000,
		blind_rad_val		= 0.1,
		aim_y_offset		= 2.5,
		
		ang_err_val			= math.rad(0.008),
		abs_err_val			= 2,
	},

	autopilot = {
		delay				= 1.5,
		op_time				= 240,
		Kconv				= 6.0,
		Knv					= 0.0025,
		Kd					= 0.5,
		Ki					= 2.0,
		Kout				= 1.0,
		Kx					= 0.04,
		Krx					= 2.0,
		fins_limit			= math.rad(20),
		fins_limit_x		= math.rad(5),
		Areq_limit			= 25.0,
		bang_bang			= 0,
		max_side_N			= 10,
		max_signal_Fi		= math.rad(12),
		rotate_fins_output	= 0,
		alg					= 0,
		PN_dist_data 		= {	15000,	0,
								5000,	1	},
		draw_fins_conv		= {math.rad(90),1,1},
		null_roll			= math.rad(45),
		
		loft_active_by_default	= 0,
		loft_add_pitch			= math.rad(10),
		loft_trig_ang			= math.rad(26),
		loft_min_dist			= 15000,
		K_heading_hor			= 0.3,
		K_heading_ver			= 0.3,
		K_loft					= 0.3,
	},
	
	warhead		= warheads["AGM_88"],
	warhead_air = warheads["AGM_88"]
}, 

{ mass = 361 }
);



declare_missile("X_58", _("Kh-58U"), "X-58", "wAmmunitionSelfHoming", wsType_AS_Missile, "anti_radiation_missile", 
{
	controller = {
		boost_start = 0.301,
		march_start = 3.900,
	},
	
	booster = {
		impulse								= 195,
		fuel_mass							= 112.18,
		work_time							= 3.6,
		boost_time							= 0,
		boost_factor						= 0,
		nozzle_position						= {{-1.5, -0.19, 0}},
		nozzle_orientationXYZ				= {{0.0, 0.0, 0.0}},
		tail_width							= 0.4,
		smoke_color							= {1.0, 1.0, 1.0},
		smoke_transparency					= 0.9,
		custom_smoke_dissipation_factor		= 0.2,	
	},
	
	march = {
		impulse								= 210,
		fuel_mass							= 70.86,
		work_time							= 15,
		boost_time							= 0,
		boost_factor						= 0,
		nozzle_position						= {{-1.5, -0.19, 0}},
		nozzle_orientationXYZ				= {{0.0, 0.0, 0.0}},
		tail_width							= 0.4,
		smoke_color							= {1.0, 1.0, 1.0},
		smoke_transparency					= 0.9,
		custom_smoke_dissipation_factor		= 0.2,	
	},
	
	
	fm = {
		mass        = 640,  
		caliber     = 0.380,  
		cx_coeff    = {1,0.4,1.1,0.5,1.4},
		L           = 4.8,
		I           = 1 / 12 * 640 * 4.8 * 4.8,
		Ma          = 0.3,
		Mw          = 1.116,
		wind_sigma	= 0.0,
		wind_time	= 0.0,
		Sw			= 1.65,
		dCydA		= {0.07, 0.036},
		A			= 0.36,
		maxAoa		= 0.28,
		finsTau		= 0.1,
		
		Ma_x		= 0.001,
		Kw_x		= 0.001,
		I_x			= 50,
	},
	
	radio_seeker = {
		FOV					= math.rad(5),
		op_time				= 600,--200Kh58
		keep_aim_time		= 4,
		pos_memory_time		= 200,
		sens_near_dist		= 300.0,
		sens_far_dist		= 70000.0,
		err_correct_time	= 2.5,
		err_val				= 0.0036,
		calc_aim_dist		= 500000,
		blind_rad_val		= 0.1,
		blind_ctrl_dist		= 2800,
		aim_y_offset		= 4.5,
	},
	
	simple_gyrostab_seeker = {
		omega_max	= math.rad(8)
	},
	
	fuze_proximity = {
		ignore_inp_armed = 1,
	},
	
	autopilot = {
		delay			 = 1.0,
		K				 = 300.0,
		Kg				 = 5.0,
		Ki				 = 0.0,
		finsLimit		 = 0.1,
		useJumpByDefault = 1,
		J_Power_K		 = 1.2,
		J_Diff_K		 = 0.3,
		J_Int_K			 = 0.0,
		J_Angle_K		 =  math.rad(11),
		J_FinAngle_K	 =  math.rad(18),		
		J_Angle_W		 = 3.5,
	},
	
	start_helper = {
		delay				= 1.0,
		power				= 0.1,
		time				= 1,
		use_local_coord		= 0,
		max_vel				= 200,
		max_height			= 200,
		vh_logic_or			= 0,
	},
	
	warhead = warheads["X_58"],
	warhead_air = warheads["X_58"]
}, 

{ mass = 640 }
);

declare_missile("X_25MP", _("Kh-25MPU"), "X-25MP", "wAmmunitionSelfHoming", wsType_AS_Missile, "anti_radiation_missile", 
{
	controller = {
		boost_start = 0.001,
		march_start = 2.001,
	},
	
	booster = {
		impulse								= 208,
		fuel_mass							= 22.1,
		work_time							= 2,
		boost_time							= 0,
		boost_factor						= 0,
		nozzle_position						= {{-1.14, 0.0, 0.15},{-1.14, -0.0, -0.15}},
		nozzle_orientationXYZ				= {{0.0, -0.15, 0.0},{0.0, 0.15, 0.0}},
		tail_width							= 0.1,
		smoke_color							= {1.0, 1.0, 1.0},
		smoke_transparency					= 0.9,
		custom_smoke_dissipation_factor		= 0.2,				
	},
	
	
	march = {
		impulse								= 208,
		fuel_mass							= 60.4,
		work_time							= 7,
		boost_time							= 0,
		boost_factor						= 0,
		nozzle_position						= {{-1.14, 0.0, 0.15},{-1.14, -0.0, -0.15}},
		nozzle_orientationXYZ				= {{0.0, -0.15, 0.0},{0.0, 0.15, 0.0}},
		tail_width							= 0.1,
		smoke_color							= {1.0, 1.0, 1.0},
		smoke_transparency					= 0.9,
		custom_smoke_dissipation_factor		= 0.2,	
	},
	
	
	fm = {
		mass        = 315,  
		caliber     = 0.275,  
		cx_coeff    = {1,0.55,1.0,0.75,1.2},
		L           = 3.7,
		I           = 1 / 12 * 297 * 3.7 * 3.7,
		Ma          = 0.3,
		Mw          = 1.116,
		wind_sigma	= 20.0,
		wind_time	= 1000.000000,
		Sw			= 0.46,
		dCydA		= {0.07, 0.036},
		A			= 0.36,
		maxAoa		= 0.3,
		finsTau		= 0.1,
		Ma_x		= 0.001,
		Kw_x		= 0.001,
		I_x			= 30,
	},
	
	radio_seeker = {
		FOV					= math.rad(5),
		op_time				= 90,
		keep_aim_time		= 4,
		pos_memory_time		= 40,
		sens_near_dist		= 300.0,
		sens_far_dist		= 40000.0,
		err_correct_time	= 2.5,
		err_val				= 0.0044,
		calc_aim_dist		= 500000,
		blind_rad_val		= 0.2,
		blind_ctrl_dist		= 2000,
		aim_y_offset		= 2.0,
	},
	
	simple_gyrostab_seeker = {
		omega_max	= math.rad(8)
	},
	
	fuze_proximity = {
		ignore_inp_armed = 0,
	},
	
	autopilot = {
		K				 = 120.0,
		Kg				 = 6.0,
		Ki				 = 0.0,
		finsLimit		 = 0.22,
		useJumpByDefault = 1,
		J_Power_K		 = 1.2,
		J_Diff_K		 = 0.4,
		J_Int_K			 = 0.0,
		J_Angle_K		 = math.rad(12),
		J_FinAngle_K	 = math.rad(18),
		J_Angle_W		 = 3.5,
		delay			 = 1.0,
	},
	
	
	warhead = warheads["X_25MP"],
	warhead_air = warheads["X_25MP"]
}, 

{ mass = 315 }
);

declare_missile("X_25MR", _("Kh-25MR"), "X-25MR", "wAmmunitionVikhr", wsType_AS_Missile, "command_guided_missile", 
{
	controller = {
		boost_start = 0.001,
		march_start = 2.001,
	},
	
	booster = {
		impulse								= 208,
		fuel_mass							= 22.1,
		work_time							= 2,
		boost_time							= 0,
		boost_factor						= 0,
		nozzle_position						= {{-1.14, 0.0, 0.15},{-1.14, -0.0, -0.15}},
		nozzle_orientationXYZ				= {{0.0, -0.15, 0.0},{0.0, 0.15, 0.0}},
		tail_width							= 0.1,
		smoke_color							= {1.0, 1.0, 1.0},
		smoke_transparency					= 0.9,
		custom_smoke_dissipation_factor		= 0.2,				
	},
	
	
	march = {
		impulse								= 208,
		fuel_mass							= 60.4,
		work_time							= 7,
		boost_time							= 0,
		boost_factor						= 0,
		nozzle_position						= {{-1.14, 0.0, 0.15},{-1.14, -0.0, -0.15}},
		nozzle_orientationXYZ				= {{0.0, -0.15, 0.0},{0.0, 0.15, 0.0}},
		tail_width							= 0.1,
		smoke_color							= {1.0, 1.0, 1.0},
		smoke_transparency					= 0.9,
		custom_smoke_dissipation_factor		= 0.2,	
	},
	
	
	fm = {
		mass        = 300,  
		caliber     = 0.275,  
		cx_coeff    = {1,0.55,1.0,0.75,1.2},
		L           = 3.7,
		I           = 1 / 12 * 297 * 3.7 * 3.7,
		Ma          = 0.4,
		Mw          = 1.2,
		wind_sigma	= 0.0,--80.0,
		wind_time	= 1000.000000,
		Sw			= 0.5,
		dCydA		= {0.07, 0.036},
		A			= 0.6,
		maxAoa		= 0.24,
		finsTau		= 0.1,
		lockRoll	= 1,
	},
	
	spiral_nav = {
		t_cone_near_rad		= 1000,
		def_cone_max_dist	= 10000,
		def_cone_near_rad	= 1000,
		def_cone_near_rad_st	= 0,
		def_cone_time_stab_rad	= 0,
		gb_angle			= 0.06,
		gb_min_dist			= 1,
		gb_use_time			= 1.1,
		gb_max_retW			= 0.24,
		gb_ret_Kp			= 0.7,
	},
	
	autopilot = {
		Kp					= 0.008,
		Ki					= 0.004,
		Kd					= 0.006,
		max_ctrl_angle		= 1.0,
		delay				= 0.9,
		op_time				= 0.0,
		fins_discreet		= 0.001,
		no_ctrl_center_ang	= 0.00005,
	},
	
	warhead = warheads["X_25MR"],
	warhead_air = warheads["X_25MR"]
}, 

{ mass = 300 }
);


declare_missile("X_25ML", _("Kh-25ML"), "X-25ML", "wAmmunitionSelfHoming", wsType_AS_Missile, "self_homing_gyrost_missile2", 
{
	controller = {
		boost_start = 0.001,
		march_start = 2.001,
	},
	
	booster = {
		impulse								= 208,
		fuel_mass							= 22.1,
		work_time							= 2,
		boost_time							= 0,
		boost_factor						= 0,
		nozzle_position						= {{-1.14, 0.0, 0.15},{-1.14, -0.0, -0.15}},
		nozzle_orientationXYZ				= {{0.0, -0.15, 0.0},{0.0, 0.15, 0.0}},
		tail_width							= 0.1,
		smoke_color							= {1.0, 1.0, 1.0},
		smoke_transparency					= 0.9,
		custom_smoke_dissipation_factor		= 0.2,				
	},
	
	
	march = {
		impulse								= 208,
		fuel_mass							= 60.4,
		work_time							= 7,
		boost_time							= 0,
		boost_factor						= 0,
		nozzle_position						= {{-1.14, 0.0, 0.15},{-1.14, -0.0, -0.15}},
		nozzle_orientationXYZ				= {{0.0, -0.15, 0.0},{0.0, 0.15, 0.0}},
		tail_width							= 0.1,
		smoke_color							= {1.0, 1.0, 1.0},
		smoke_transparency					= 0.9,
		custom_smoke_dissipation_factor		= 0.2,	
	},
	
	
	fm = {
		mass        = 297,  
		caliber     = 0.275,  
		cx_coeff    = {1,0.55,1.0,0.75,1.2},
		L           = 3.7,
		I           = 1 / 12 * 297 * 3.7 * 3.7,
		Ma          = 0.4,
		Mw          = 1.1,
		wind_sigma	= 0.0,
		wind_time	= 0.0,
		Sw			= 0.5,
		dCydA		= {0.07, 0.036},
		A			= 0.6,
		maxAoa		= 0.3,
		finsTau		= 0.1,
		lockRoll	= 1,
	},
	
	simple_seeker = {
		sensitivity = 0,
		delay		= 0.0,
		FOV			= math.rad(60),
		DGF			= {0.00, 0.11, 0.22, 0.33, 0.44, 0.55, 0.66, 0.77, 0.88, 1.00},
		RWF			= {1.00, 1.00, 1.00, 1.00, 1.00, 1.00, 0.80, 0.70, 0.60, 0.10},
		maxW		= 0.18,
		stTime		= 0.5,
		opTime		= 40,
	},
	
	simple_gyrostab_seeker = {
		omega_max	= math.rad(8)
	},
	
	autopilot = {
		K				 = 20.0,
		Kg				 = 3.8,
		Ki				 = 0.0002,
		finsLimit		 = 0.22,
		useJumpByDefault = 1,
		J_Power_K		 = 1.2,
		J_Diff_K		 = 0.6,
		J_Int_K			 = 0.0,
		J_Angle_K		 = math.rad(12),
		J_FinAngle_K	 = math.rad(16),
		J_Angle_W		 = 3.5,
		delay			 = 1.0,
	},
	
	warhead = warheads["X_25ML"],
	warhead_air = warheads["X_25ML"]
}, 

{ mass = 297 }
);


declare_missile("X_29L", _("Kh-29L"), "X-29L", "wAmmunitionSelfHoming", wsType_AS_Missile, "self_homing_gyrost_missile", 
{
	controller = {
		march_start = 0.8,
	},
	
	march = {
		impulse								= 208,
		fuel_mass							= 132,
		work_time							= 5,
		boost_time							= 0,
		boost_factor						= 0,
		nozzle_position						= {{-1.3, -0.19, 0}},
		nozzle_orientationXYZ				= {{0.0, 0.0, 0.0}},
		tail_width							= 0.4,
		smoke_color							= {1.0, 1.0, 1.0},
		smoke_transparency					= 0.9,
		custom_smoke_dissipation_factor		= 0.2,	
	},
	
	fm = {
		mass        = 648,  
		caliber     = 0.380,  
		cx_coeff    = {1,0.55,1.0,0.75,1.25},
		L           = 3.875,
		I           = 1 / 12 * 648 * 3.875 * 3.875,
		Ma          = 0.3,
		Mw          = 1.2,
		wind_sigma	= 0.0,--80.0,
		wind_time	= 1000.000000,
		Sw			= 1.55,
		dCydA		= {0.07, 0.036},
		A			= 0.6,
		maxAoa		= 0.3,
		finsTau		= 0.1,
		lockRoll	= 1,
	},
	
	simple_seeker = {
		sensitivity = 0,
		delay		= 0.0,
		FOV			= math.rad(60),
		DGF			= {0.00, 0.11, 0.22, 0.33, 0.44, 0.55, 0.66, 0.77, 0.88, 1.00},
		RWF			= {1.00, 1.00, 1.00, 1.00, 1.00, 1.00, 0.80, 0.70, 0.60, 0.10},
		maxW		= 0.18,
		stTime		= 0.5,
		opTime		= 40,
	},
	
	simple_gyrostab_seeker = {
		omega_max	= math.rad(8)
	},
	
	autopilot = {
		K				 = 36.0,
		Kg				 = 6.0,
		Ki				 = 0.0005,
		finsLimit		 = 0.2,
		useJumpByDefault = 1,
		J_Power_K		 = 3.6,
		J_Diff_K		 = 1.0,
		J_Int_K			 = 0.0,
		J_Angle_K		 = math.rad(14),
		J_FinAngle_K	 = math.rad(18),
		J_Angle_W		 = 4.5,
		delay			 = 0.8,
	},
	
	warhead = warheads["X_29L"],
	warhead_air = warheads["X_29L"]
}, 

{ mass = 648 }
);

declare_missile("X_29T", _("Kh-29T"), "X-29T", "wAmmunitionSelfHoming", wsType_AS_Missile, "self_homing_gyrost_missile", 
{
	controller = {
		march_start = 0.8,
	},
	
	march = {
		impulse								= 148,
		fuel_mass							= 100,
		work_time							= 6.2,
		boost_time							= 0,
		boost_factor						= 0,
		nozzle_position						= {{-1.3, -0.19, 0}},
		nozzle_orientationXYZ				= {{0.0, 0.0, 0.0}},
		tail_width							= 0.4,
		smoke_color							= {1.0, 1.0, 1.0},
		smoke_transparency					= 0.9,
		custom_smoke_dissipation_factor		= 0.2,	
	},
	
	
	fm = {
		mass        = 668,  
		caliber     = 0.380,  
		cx_coeff    = {1,0.55,1.0,0.75,1.25},
		L           = 3.875,
		I           = 1 / 12 * 668 * 3.875 * 3.875,
		Ma          = 0.3,
		Mw          = 1.2,
		wind_sigma	= 0.0,--80.0,
		wind_time	= 1000.000000,
		Sw			= 1.55,
		dCydA		= {0.07, 0.036},
		A			= 0.6,
		maxAoa		= 0.3,
		finsTau		= 0.1,
		lockRoll	= 1,
	},
	
	simple_seeker = {
		sensitivity = 0,
		delay		= 0.0,
		FOV			= math.rad(60),
		DGF			= {0.00, 0.11, 0.22, 0.33, 0.44, 0.55, 0.66, 0.77, 0.88, 1.00},
		RWF			= {1.00, 1.00, 1.00, 1.00, 1.00, 1.00, 0.80, 0.70, 0.60, 0.10},
		maxW		= 0.016,
		stTime		= 0.5,
		opTime		= 40,
	},
	
	simple_gyrostab_seeker = {
		omega_max	= math.rad(8)
	},
	
	autopilot = {
		K				 = 36.0,
		Kg				 = 6.0,
		Ki				 = 0.0005,
		finsLimit		 = 0.2,
		useJumpByDefault = 1,
		J_Power_K		 = 3.6,
		J_Diff_K		 = 1.0,
		J_Int_K			 = 0.0,
		J_Angle_K		 = math.rad(14),
		J_FinAngle_K	 = math.rad(18),
		J_Angle_W		 = 4.5,
		delay			 = 0.8,
	},
	
	warhead = warheads["X_29T"],
	warhead_air = warheads["X_29T"]
}, 

{ mass = 668 }
);


declare_missile("X_29TE", _("Kh-29TE"), "X-29TE", "wAmmunitionSelfHoming", wsType_AS_Missile, "self_homing_gyrost_missile", 
{
	controller = {
		march_start = 1.0,
	},
	
	march = {
		impulse								= 148,
		fuel_mass							= 100,
		work_time							= 6.2,
		boost_time							= 0,
		boost_factor						= 0,
		nozzle_position						= {{-1.3, -0.19, 0}},
		nozzle_orientationXYZ				= {{0.0, 0.0, 0.0}},
		tail_width							= 0.4,
		smoke_color							= {1.0, 1.0, 1.0},
		smoke_transparency					= 0.1,
		custom_smoke_dissipation_factor		= 0.2,	
	},
		
	fm = {
		mass        = 678,  
		caliber     = 0.380,  
		cx_coeff    = {1,0.55,1.0,0.75,1.25},
		L           = 3.875,
		I           = 1 / 12 * 678 * 3.875 * 3.875,
		Ma          = 0.3,
		Mw          = 1.2,
		wind_sigma	= 0.0,--80.0,
		wind_time	= 1000.000000,
		Sw			= 1.55,
		dCydA		= {0.07, 0.036},
		A			= 0.6,
		maxAoa		= 0.3,
		finsTau		= 0.1,
		lockRoll	= 1,
	},
	
	simple_seeker = {
		sensitivity = 0,
		delay		= 0.0,
		FOV			= math.rad(60),
		DGF			= {0.00, 0.11, 0.22, 0.33, 0.44, 0.55, 0.66, 0.77, 0.88, 1.00},
		RWF			= {1.00, 1.00, 1.00, 1.00, 1.00, 1.00, 0.80, 0.70, 0.60, 0.10},
		maxW		= 0.018,
		stTime		= 0.5,
		opTime		= 140,
	},
	
	simple_gyrostab_seeker = {
		omega_max	= math.rad(8)
	},
	
	autopilot = {
		K				 = 36.0,
		Kg				 = 6.0,
		Ki				 = 0.0005,
		finsLimit		 = 0.2,
		useJumpByDefault = 1,
		J_Power_K		 = 3.6,
		J_Diff_K		 = 1.0,
		J_Int_K			 = 0.0,
		J_Angle_K		 = math.rad(14),
		J_FinAngle_K	 = math.rad(18),
		J_Angle_W		 = 4.5,
		delay			 = 0.8,
	},
	
	warhead = warheads["X_29TE"],
	warhead_air = warheads["X_29TE"]
}, 

{ mass = 678 }
);

declare_missile("AGM_154", _("AGM-154C"), "agm-154", "wAmmunitionCruise", wsType_AS_Missile, "AGM-154C", 
{
	fm =	{
		mass				= 485,  
		caliber				= 0.33,  
		cx_coeff			= {1, 0.3, 0.65, 0.010, 1.6},
		L					= 4.06,
		I					= 1000,
		Ma					= 2,
		Mw					= 7,
		wind_sigma			= 0.0,
		wind_time			= 0.0,
		Sw					= 1.1,
		dCydA				= {0.07, 0.036},
		A					= 0.06,
		maxAoa				= 0.22,
		finsTau				= 1.2,
		Ma_x				= 2,
		Ma_z				= 2,
		Kw_x				= 0.03,
		add_Sw				= 0.6,
		add_w_c0			= 0.008,
		start_no_w_A_mult	= 7,
	},
	
	simple_seeker =	{
		sensitivity = 0,
		delay		= 0.0,
		FOV			= 0.6,
		maxW		= 500,
		opTime		= 9999,
	},
	
	control_block =	{
		seeker_activation_dist		= 7000,
		default_cruise_height		= -1,
		obj_sensor					= 0,
		can_update_target_pos		= 0,
		turn_before_point_reach		= 1,
		turn_hor_N					= 2.2,
		turn_max_calc_angle_deg		= 90,
		turn_point_trigger_dist		= 100,
	},
	
	autopilot =	{
		delay						= 2,
		K							= 600,
		Ki							= 0.0,
		Kg							= 36,
		nw_K						= 180,
		nw_Ki						= 0.001,
		nw_Kg						= 52,
		finsLimit					= 0.8,
		useJumpByDefault			= 0,
		J_Power_K					= 7,
		J_Diff_K					= 1.2,
		J_Int_K						= 0.0,
		J_Angle_K					= 0.14,
		J_FinAngle_K				= 0.25,
		J_Angle_W					= 0.12,
		hKp_err						= 460,
		hKp_err_croll				= 0.012,
		hKd							= -0.008,
		max_roll					= 1.3,
		egm_Angle_K					= 0.18,
		egm_FinAngle_K				= 0.26,
		egm_add_power_K				= 0.2,
		wings_depl_fins_limit_K		= 0.28,
		err_new_wlos_k				= 0.84,
		err_aoaz_k					= 28,
		err_aoaz_sign_k				= 0.18,
	},
		
	warhead		= warheads["BLU-111B"],
	warhead_air = warheads["BLU-111B"],	
}, 

{ mass = 485 }
);

declare_missile("AGM_65D", _("AGM-65D"), "AGM-65D", "wAmmunitionSelfHoming", wsType_AS_Missile, "AGM-65", 
{
	fm = {
		mass        = 218,  
		caliber     = 0.305,  
		cx_coeff    = {1,0.39,0.38,0.236,1.31},
		L           = 2.49,
		I           = 1 / 12 * 209 * 2.49 * 2.49,
		Ma          = 0.68,
		Mw          = 1.116,
		wind_sigma	= 0.0,
		wind_time	= 0.0,
		Sw			= 0.55,
		dCydA		= {0.07, 0.036},
		A			= 0.36,
		maxAoa		= 0.23,
		finsTau		= 0.1,
		Ma_x		= 3,
		Kw_x		= 0.03,
		I_x			= 40,
	},
	
	seeker = {
		delay			= 0.0,
		op_time			= 105,
		FOV				= math.rad(60),
		max_w_LOS		= 0.06,
		max_w_LOS_surf	= 0.03,
	
		max_target_speed			= 33,
		max_target_speed_rnd_coeff	= 10,
	},
	
	PN_autopilot = {
		K			= 0.014,
		Ki			= 0,
		Kg			= 2.5,
		Kx			= 0.02,
		fins_limit	= 0.3,
		K_GBias		= 0.26,
	},
	
	march = {
		smoke_color			= {0.8, 0.8, 0.8},
		smoke_transparency	= 0.7,
	},
	
	warhead = warheads["AGM_65D"],
	warhead_air = warheads["AGM_65D"]
}, 
{ mass = 218 }
);

declare_missile("AGM_65E", _("AGM-65E"), "AGM-65E", "wAmmunitionLaserHoming", wsType_AS_Missile, "AGM-65E", 
{
	fm = {
		mass        = 292,  
		caliber     = 0.305,  
		cx_coeff    = {1,0.39,0.38,0.236,1.31},
		L           = 2.49,
		I           = 1 / 12 * 209 * 2.49 * 2.49,
		Ma          = 0.68,
		Mw          = 1.116,
		wind_sigma	= 0.0,
		wind_time	= 0.0,
		Sw			= 0.55,
		dCydA		= {0.07, 0.036},
		A			= 0.36,
		maxAoa		= 0.23,
		finsTau		= 0.1,
		Ma_x		= 3,
		Kw_x		= 0.03,
		I_x			= 40,
	},
	
	laser_spot_seeker = {
		FOV					= 0.558,
		max_seeker_range	= 18520,
	},

	PN_autopilot = {
		K			= 0.014,
		Ki			= 0,
		Kg			= 2.5,
		Kx			= 0.02,
		fins_limit	= 0.3,
		K_GBias		= 0.26,
	},
	
	march = {
		smoke_color			= {0.8, 0.8, 0.8},
		smoke_transparency	= 0.7,
	},
	
	warhead		= warheads["AGM_65E"],
	warhead_air = warheads["AGM_65E"]
}, 
{ mass = 292 }
);


declare_missile("AGM_65H", _("AGM-65H"), "AGM-65H", "wAmmunitionSelfHoming", wsType_AS_Missile, "AGM-65", 
{
	fm = {
		mass        = 208,  
		caliber     = 0.305,  
		cx_coeff    = {1,0.39,0.38,0.236,1.31},
		L           = 2.49,
		I           = 1 / 12 * 209 * 2.49 * 2.49,
		Ma          = 0.68,
		Mw          = 1.116,
		wind_sigma	= 0.0,
		wind_time	= 0.0,
		Sw			= 0.55,
		dCydA		= {0.07, 0.036},
		A			= 0.36,
		maxAoa		= 0.23,
		finsTau		= 0.1,
		Ma_x		= 3,
		Kw_x		= 0.03,
		I_x			= 40,
	},
	
	seeker = {
		delay			= 0.0,
		op_time			= 105,
		FOV				= math.rad(60),
		max_w_LOS		= 0.06,
		max_w_LOS_surf	= 0.03,
	
		max_target_speed			= 33,
		max_target_speed_rnd_coeff	= 10,
	},
	
	PN_autopilot = {
		K			= 0.014,
		Ki			= 0,
		Kg			= 2.5,
		Kx			= 0.02,
		fins_limit	= 0.3,
		K_GBias		= 0.26,
	},
	
	march = {
		smoke_color			= {0.9, 0.9, 0.9},
		smoke_transparency	= 0.9,
	},
	
	warhead = warheads["AGM_65H"],
	warhead_air = warheads["AGM_65H"]
}, 
{ mass = 208 }
);


declare_missile("AGM_65G", _("AGM-65G"), "AGM-65G", "wAmmunitionSelfHoming", wsType_AS_Missile, "AGM-65", 
{
	fm = {
		mass        = 301,  
		caliber     = 0.305,  
		cx_coeff    = {1,0.39,0.38,0.236,1.31},
		L           = 2.49,
		I           = 1 / 12 * 209 * 2.49 * 2.49,
		Ma          = 0.68,
		Mw          = 1.116,
		wind_sigma	= 0.0,
		wind_time	= 0.0,
		Sw			= 0.55,
		dCydA		= {0.07, 0.036},
		A			= 0.36,
		maxAoa		= 0.23,
		finsTau		= 0.1,
		Ma_x		= 3,
		Kw_x		= 0.03,
		I_x			= 40,
	},
	
	seeker = {
		delay			= 0.0,
		op_time			= 105,
		FOV				= math.rad(60),
		max_w_LOS		= 0.06,
		max_w_LOS_surf	= 0.03,
	
		max_target_speed			= 33,
		max_target_speed_rnd_coeff	= 10,

		ship_track_board_vis_angle	= math.rad(60),
	},
	
	PN_autopilot = {
		K			= 0.02,
		Ki			= 0,
		Kg			= 2.5,
		Kx			= 0.02,
		fins_limit	= 0.3,
		K_GBias		= 0.28,
	},
	
	march = {
		smoke_color			= {0.8, 0.8, 0.8},
		smoke_transparency	= 0.7,
	},
	
	warhead		= warheads["AGM_65G"],
	warhead_air	= warheads["AGM_65G"]
}, 
{ mass = 301 }
);


declare_missile("AGM_65K", _("AGM-65K"), "AGM-65K", "wAmmunitionSelfHoming", wsType_AS_Missile, "AGM-65", 
{
	fm = {
		mass        = 360,  
		caliber     = 0.305,  
		cx_coeff    = {1,0.39,0.38,0.236,1.31},
		L           = 2.49,
		I           = 1 / 12 * 209 * 2.49 * 2.49,
		Ma          = 0.68,
		Mw          = 1.116,
		wind_sigma	= 0.0,
		wind_time	= 0.0,
		Sw			= 0.55,
		dCydA		= {0.07, 0.036},
		A			= 0.36,
		maxAoa		= 0.23,
		finsTau		= 0.1,
		Ma_x		= 3,
		Kw_x		= 0.03,
		I_x			= 40,
	},
	
	seeker = {
		delay			= 0.0,
		op_time			= 105,
		FOV				= math.rad(60),
		max_w_LOS		= 0.06,
		max_w_LOS_surf	= 0.03,
	
		max_target_speed			= 33,
		max_target_speed_rnd_coeff	= 10,
	},
	
	PN_autopilot = {
		K			= 0.014,
		Ki			= 0,
		Kg			= 2.5,
		Kx			= 0.02,
		fins_limit	= 0.3,
		K_GBias		= 0.24,
	},
	
	march = {
		smoke_color			= {0.9, 0.9, 0.9},
		smoke_transparency	= 0.5,
	},
	
	warhead		= warheads["AGM_65K"],
	warhead_air	= warheads["AGM_65K"]
}, 
{ mass = 360 }
);

declare_missile("AGM_84A", _('AGM-84A Harpoon'), "agm-84", "wAmmunitionAntiShip", wsType_AS_Missile, "anti_ship_missile_stpos_ctrl", 
{
	fm = {
		mass        = 555.0,  
		caliber     = 0.343,  
		cx_coeff    = {1,0.39,0.38,0.236,1.31},
		L           = 3.85,
		I           = 1 / 12 * 555.0 * 3.85 * 3.85,
		Ma          = 0.68,
		Mw          = 1.116,
		wind_sigma	= 0.0,
		wind_time	= 1000.0,
		Sw			= 0.7,
		dCydA		= {0.07, 0.036},
		A			= 0.5,
		maxAoa		= 0.3,
		finsTau		= 0.05,
		Ma_x		= 3,
		Ma_z		= 3,
		Kw_x		= 0.05,
	},
	
	simple_seeker = {
		delay		= 1.0,
		FOV			= math.rad(45),
		stTime		= 0.5,
		opTime		= 9999,
	},
	
	autopilot =	--inertial autopilot + horiz correction when seeker is on
	{
		delay				= 1.0,		-- time delay
		Kpv					= 0.026,	-- vertical control PID
		Kdv					= 3.2,
		Kiv					= 0.00002,
		Kph					= 28.0,		-- horiz control PID
		Kdh					= 3.0,
		Kih					= 0.0,
		glide_height		= 35.0,		-- cruise glide height
		use_current_height	= 0,		-- keep launch height
		max_vert_speed 		= 25.0,		-- max vertical speed kept from launch height to glide height
		altim_vel_k			= 1.0,		-- proximity sensor correction by velocity coeff
		finsLimit			= 0.68,		-- max signal value from autopilot to fins 
		inertial_km_error	= 4.0,		-- inertial guidance error coeff to 1 km flight distance
	},
	
	final_autopilot =		-- final stage guidance with terminal maneuver
	{
		delay				= 0,
		K					= 60,		-- autopilot P
		Ki					= 0,		-- I
		Kg					= 16,		-- D
		finsLimit			= 0.9,		
		useJumpByDefault	= 1,		-- 
		J_Power_K			= 2.0,		-- terminal maneuver params: P
		J_Diff_K			= 0.8,		-- D
		J_Int_K				= 0,		-- I
		J_Angle_K			= 0.24,		-- jump maneuver angle
		J_FinAngle_K		= 0.32,		-- jump inactivation trigger angle to target
		J_Angle_W			= 2.4,		-- max maneuver angle speed
		bang_bang			= 0,		-- use only -1, 1 values to control fins
		J_Trigger_Vert		= 1,		-- use global y axis
	},
	
	triggers_control = {
		action_wait_timer				= 5,	-- wait for dist functions n sen, then set default values
		default_sensor_tg_dist			= 8000, -- turn on seeker and start horiz. correction if target is locked
		default_final_maneuver_tg_dist	= 4000,
		default_destruct_tg_dist		= 1000,	-- if seeker still can not find a target explode warhead after reaching pred. target point + n. km
		trigger_by_path					= 1,
	},
	
	controller = {
		boost_start	= 0.001,
		march_start = 0.01,
	},
	
	boost = {	--	air launch - no booster
		impulse								= 0,
		fuel_mass							= 0,
		work_time							= 0,
		boost_time							= 0,
		boost_factor						= 0,
		nozzle_position						= {{0, 0, 0}},
		nozzle_orientationXYZ				= {{0, 0, 0}},
		tail_width							= 0.0,
		smoke_color							= {0.0, 0.0, 0.0},
		smoke_transparency					= 0.0,
		custom_smoke_dissipation_factor		= 0.0,				
	},
	
	march = {
		impulse			= 550,
		fuel_mass		= 138.5,
		work_time		= 9999,
		min_fuel_rate	= 0.005,
		min_thrust		= -100,
		max_thrust		= 3000,
		thrust_Tau		= 0.0017,
		
		nozzle_position						= {{-1.70, -0.18, 0.0}},
		nozzle_orientationXYZ				= {{0.0, 0.0, 0.0}},
		tail_width							= 0.5,
		smoke_color							= {0.0, 0.0, 0.0},
		smoke_transparency					= 0.2,
		custom_smoke_dissipation_factor		= 0.2,	
		
		start_burn_effect			= 1,
		start_effect_delay			= {0.0,		0.3, 	0.8},
		start_effect_time			= {0.7,		1.0, 	0.1},
		start_effect_size			= {0.09,	0.104,	0.11},
		start_effect_smoke			= {0.01,	0.4, 	0.01},
		start_effect_x_pow			= {1.0,		1.0,	1.0},
		start_effect_x_dist			= {1.1,		0.9,	0.0},
		start_effect_x_shift		= {0.15,	0.15,	0.2},
	},
	
	engine_control = {
		default_speed	= 237,
		K				= 265,
		Kd				= 0.01,
		Ki				= 0.001,
		-- burst_signal = 9999, -- used in 'anti_ship_missile_tb' scheme
	},
	
	warhead = warheads["AGM_84A"],
	warhead_air = warheads["AGM_84A"]
}, 
{
 mass = 555.0,
 add_attributes = {"Cruise missiles"},
 }
);

declare_missile("AGM_84E", _('AGM-84E SLAM'), "agm-84e", "wAmmunitionAntiShip", wsType_AS_Missile, "AGM-84E", 
{
	fm = {
		mass        = 628.0,  
		caliber     = 0.343,  
		cx_coeff    = {1,0.39,0.38,0.236,1.31},
		L           = 4.49,
		I           = 1 / 12 * 628.0 * 4.49 * 4.49,
		Ma          = 0.68,
		Mw          = 1.116,
		wind_sigma	= 0.0,
		wind_time	= 1000.0,
		Sw			= 0.7,
		dCydA		= {0.07, 0.036},
		A			= 0.5,
		maxAoa		= 0.3,
		finsTau		= 0.05,
		Ma_x		= 3,
		Ma_z		= 3,
		Kw_x		= 0.05,
	},
	
	seeker = {
		delay			= 0.0,
		op_time			= 105,
		FOV				= math.rad(60),
		max_w_LOS		= 0.06,
		max_w_LOS_surf	= 0.03,
	
		max_target_speed			= 33,
		max_target_speed_rnd_coeff	= 10,
		
		check_launcher = 1,
	},
	
	autopilot =
	{
		delay				= 1.0,
		Kpv					= 0.026,
		Kdv					= 3.2,
		Kiv					= 0.00002,
		Kph					= 28.0,
		Kdh					= 3.0,
		Kih					= 0.0,
		glide_height		= 15.0,
		use_current_height	= 1,
		max_vert_speed 		= 25.0,
		altim_vel_k			= 1.0,
		finsLimit			= 0.68,
		inertial_km_error	= 3.0,
	},
	
	final_autopilot =		
		{
		delay				= 0,
		K					= 60,
		Ki					= 0,
		Kg					= 16,
		finsLimit			= 0.9,		
		useJumpByDefault	= 1,
		J_Power_K			= 2.0,
		J_Diff_K			= 0.8,
		J_Int_K				= 0,
		J_Angle_K			= 0.24,
		J_FinAngle_K		= 0.32,
		J_Angle_W			= 2.4,
		bang_bang			= 0,
		J_Trigger_Vert		= 1,
	},
	
	mode_switcher = {		-- use final maneuver if target is locked on
		delay = 1,
		trigger_dist		= 4000,
	},
	
	triggers_control = {
		use_horiz_dist				= 1,
		action_wait_timer			= 5,	-- wait for dist functions n sen, then set default values
		default_sensor_tg_dist		= 8000, -- turn on seeker and start horiz. correction if target is locked
		default_destruct_tg_dist	= 1000,	-- if seeker still can not find a target explode warhead after reaching pred. target point + n. km
	},
	
	controller = {
		boost_start	= 0.001,
		march_start = 0.01,
	},
	
	boost = {				--	air launch - no booster
		impulse								= 0,
		fuel_mass							= 0,
		work_time							= 0,
		boost_time							= 0,
		boost_factor						= 0,
		nozzle_position						= {{0, 0, 0}},
		nozzle_orientationXYZ				= {{0, 0, 0}},
		tail_width							= 0.0,
		smoke_color							= {0.0, 0.0, 0.0},
		smoke_transparency					= 0.0,
		custom_smoke_dissipation_factor		= 0.0,				
	},
	
	march = {
		impulse			= 690,
		fuel_mass		= 150.0,
		work_time		= 9999,
		min_fuel_rate	= 0.005,
		min_thrust		= -100,
		max_thrust		= 3000,
		thrust_Tau		= 0.0017,
		
		nozzle_position						= {{-2.3, -0.2, 0.0}},
		nozzle_orientationXYZ				= {{0.0, 0.0, 0.0}},
		tail_width							= 0.5,
		smoke_color							= {0.0, 0.0, 0.0},
		smoke_transparency					= 0.2,
		custom_smoke_dissipation_factor		= 0.2,	
		
		start_burn_effect			= 1,
		start_effect_delay			= {0.0,		0.3, 	0.8},
		start_effect_time			= {0.7,		1.0, 	0.1},
		start_effect_size			= {0.09,	0.104,	0.11},
		start_effect_smoke			= {0.01,	0.4, 	0.01},
		start_effect_x_pow			= {1.0,		1.0,	1.0},
		start_effect_x_dist			= {1.1,		0.9,	0.0},
		start_effect_x_shift		= {0.15,	0.15,	0.2},
	},
	
	engine_control = {
		default_speed	= 237,
		K				= 265,
		Kd				= 0.01,
		Ki				= 0.001,
	},
	
	warhead = warheads["AGM_84A"],
	warhead_air = warheads["AGM_84A"]
}, 
{
 mass = 628.0,
 add_attributes = {"Cruise missiles"},
 }
);

declare_missile("X_28", _("Kh-28"), "X-28", "wAmmunitionSelfHoming", wsType_AS_Missile, "anti_radiation_missile_diff_loft", 
{
	controller = {
		march_start = 2.001,
	},

	march = {
		impulse								= 150,
		fuel_mass							= 235,
		work_time							= 10,
		boost_time							= 0,
		boost_factor						= 0,
		nozzle_position						= {{-2.85, -0.21, 0}},
		nozzle_orientationXYZ				= {{0.0, 0.0, 0.0}},
		tail_width							= 0.4,
		smoke_color							= {1.0, 1.0, 1.0},
		smoke_transparency					= 0.2,
		custom_smoke_dissipation_factor		= 0.2,	
		effect_type							= 1,
	},
	
	
	fm = {
		mass        = 715,  
		caliber     = 0.430,  
		cx_coeff    = {1,0.5,1.1,0.3,1.3},
		L           = 6.04,
		I           = 1 / 12 * 715 *6.04 * 6.04,
		Ma          = 0.3,
		Mw          = 1.116,
		wind_sigma	= 0.0,
		wind_time	= 0.0,
		Sw			= 1.6,
		dCydA		= {0.07, 0.036},
		A			= 0.36,
		maxAoa		= 0.2,
		finsTau		= 0.1,
		lockRoll	= 1,
		
		Ma_x		= 0.001,
		Kw_x		= 0.001,
		I_x			= 60,
	},
	
	radio_seeker = {
		FOV					= math.rad(5),
		op_time				= 900,--200Kh58
		keep_aim_time		= 4,
		pos_memory_time		= 200,
		sens_near_dist		= 300.0,
		sens_far_dist		= 70000.0,
		err_correct_time	= 2,
		err_val				= 0.012,
		calc_aim_dist		= 500000,
		blind_rad_val		= 0.1,
		blind_ctrl_dist		= 2500,
		aim_y_offset		= 2.5,
	},
	
	simple_gyrostab_seeker = {
		omega_max	= math.rad(8)
	},
	
	fuze_proximity = {
		ignore_inp_armed	= 1,
		arm_delay			= 10,
		radius				= 10,
	},
	
	autopilot = {
		delay			 = 2.0,	
		K				 = 140.0,
		Kg				 = 5.0,
		Ki				 = 0.0,
		finsLimit		 = 0.1,
		useJumpByDefault = 1,
		J_Power_K		 = 1.2,
		J_Diff_K		 = 0.3,
		J_Int_K			 = 0.0,
		J_FinAngle_K	 =  math.rad(38),
		J_Angle_W		 = 3.5,
		
		J_Angle_K			=  math.rad(28.5),
		K1_ang		 		= 28.5,
		K2_ang		 		= -1.9,
		K3_ang		 		= 0.032,
	},
	
	start_helper = {
		delay				= 1.0,
		power				= 0.3,
		time				= 2,
		use_local_coord		= 0,
		max_vel				= 200,
		max_height			= 600,
		vh_logic_or			= 0,
	},
	
	warhead = warheads["X_28"],
	warhead_air = warheads["X_28"]
}, 

{ mass = 715 }
);


declare_missile("AGM_45", _("AGM-45"), "agm-45", "wAmmunitionSelfHoming", wsType_AS_Missile, "anti_radiation_missile2", 
{
	controller = {
		march_start = 0.021,
	},

	march = {
		impulse								= 190,
		fuel_mass							= 36,
		work_time							= 12,
		boost_time							= 0,
		boost_factor						= 0,
		nozzle_position						= {{-1.0, -0.1, 0}},
		nozzle_orientationXYZ				= {{0.0, 0.0, 0.0}},
		tail_width							= 0.3,
		smoke_color							= {0.6, 0.6, 0.6},
		smoke_transparency					= 0.8,
		custom_smoke_dissipation_factor		= 0.2,	
	},
	
	
	fm = {
		mass        = 177,  
		caliber     = 0.203,  
		cx_coeff    = {1,0.4,1.1,0.5,1.4},
		L           = 3.05,
		I           = 1 / 12 * 177 * 3.05 * 3.05,
		Ma          = 0.3,
		Mw          = 1.116,
		wind_sigma	= 0.0,
		wind_time	= 0.0,
		Sw			= 0.85,
		dCydA		= {0.07, 0.036},
		A			= 0.36,
		maxAoa		= 0.28,
		finsTau		= 0.1,
		
		Ma_x		= 0.001,
		Kw_x		= 0.001,
		I_x			= 50,
	},
	
	radio_seeker = {
		FOV					= math.rad(5),
		op_time				= 90,
		keep_aim_time		= 0.1,
		pos_memory_time		= 0.1,
		sens_near_dist		= 300.0,
		sens_far_dist		= 60000.0,
		err_correct_time	= 3.0,
		err_val				= 0.012,
		calc_aim_dist		= 500000,
		blind_rad_val		= 0.2,
		blind_ctrl_dist		= 2200,
		aim_y_offset		= 3.0,
		--min_sens_rad_val	= --0.00025,
	},
	
	simple_gyrostab_seeker = {
		omega_max	= math.rad(8)
	},
	
	fuze_proximity = {
		ignore_inp_armed	= 1,
		arm_delay			= 10,
		radius				= 10,
	},
	
	autopilot = {
		K				 = 220.0,
		Kg				 = 6.0,
		Ki				 = 0.0001,
		finsLimit		 = 0.1,
		useJumpByDefault = 1,
		J_Power_K		 = 1.2,
		J_Diff_K		 = 0.3,
		J_Int_K			 = 0.0,
		J_Angle_K		 = math.rad(10),
		J_FinAngle_K	 = math.rad(18),
		delay			 = 2.0,
		J_Angle_W		 = 2.5,
	},
	
	start_helper = {
		delay				= 0.2,
		power				= 0.02,
		time				= 2,
		use_local_coord		= 0,
		max_vel				= 200,
		max_height			= 400,
		vh_logic_or			= 1,
	},
	
	warhead = warheads["AGM_45"],
	warhead_air = warheads["AGM_45"]
}, 

{ mass = 177 }
);

declare_missile("X_31P", _("Kh-31P"), "X-31", "wAmmunitionSelfHoming", wsType_AS_Missile, "anti_radiation_missile", 
{
	controller = {
		boost_start = 0.001,
		march_start = 2.001,
	},
	
	booster = {
		impulse								= 160,
		fuel_mass							= 20,
		work_time							= 2,
		boost_time							= 0,
		boost_factor						= 0,
		nozzle_position						= {{-2.0, -0.21, 0}},
		nozzle_orientationXYZ				= {{0.0, 0.0, 0.0}},
		tail_width							= 0.2,
		smoke_color							= {0.8, 0.8, 0.8},
		smoke_transparency					= 0.8,
		custom_smoke_dissipation_factor		= 0.2,				
	},
	
	
	march = {
		impulse								= 280,
		fuel_mass							= 100,
		work_time							= 6,
		boost_time							= 0,
		boost_factor						= 0,
		nozzle_position						= {{-2.15, -0.21, 0}},
		nozzle_orientationXYZ				= {{0.0, 0.0, 0.0}},
		tail_width							= 0.1,
		smoke_color							= {0.9, 0.9, 0.9},
		smoke_transparency					= 0.05,
		custom_smoke_dissipation_factor		= 0.45,	
		effect_type 						= 1,
		min_start_speed						= 220,
	},
	
	
	fm = {
		mass        = 600,  
		caliber     = 0.360,  
		cx_coeff    = {1,0.5,1.1,0.5,1.4},
		L           = 4.7,
		I           = 1 / 12 * 600 * 4.7 * 4.7,
		Ma          = 0.3,
		Mw          = 1.116,
		wind_sigma	= 0.0,
		wind_time	= 0.0,
		Sw			= 0.6,
		dCydA		= {0.07, 0.036},
		A			= 0.36,
		maxAoa		= 0.28,
		finsTau		= 0.1,
		lockRoll	= 1,
		
		Ma_x		= 0.001,
		Kw_x		= 0.001,
		I_x			= 60,
	},
	
	radio_seeker = {
		FOV					= math.rad(30),
		op_time				= 900,
		keep_aim_time		= 4,
		pos_memory_time		= 40,
		sens_near_dist		= 300.0,
		sens_far_dist		= 40000.0,
		err_correct_time	= 2.5,
		err_val				= 0.0044,
		calc_aim_dist		= 500000,
		blind_rad_val		= 0.2,
		blind_ctrl_dist		= 2000,
		aim_y_offset		= 2.0,
	},
	
	simple_gyrostab_seeker = {
		omega_max	= math.rad(8)
	},
	
	fuze_proximity = {
		ignore_inp_armed = 1,
	},
	
	autopilot = {
		K				 = 240.0,
		Kg				 = 24.0,
		Ki				 = 0.0,
		finsLimit		 = 0.25,
		useJumpByDefault = 1,
		J_Power_K		 = 1.2,
		J_Diff_K		 = 0.4,
		J_Int_K			 = 0.0,
		J_Angle_K		 = math.rad(14),
		J_FinAngle_K	 = math.rad(24),
		J_Angle_W		 = 3.5,
		delay			 = 1.0,
	},
	
	
	warhead = warheads["X_31P"],
	warhead_air = warheads["X_31P"]
}, 

{ mass = 600 }
);

declare_missile("ALARM", _("ALARM"), "t-alarm", "wAmmunitionSelfHoming", wsType_AS_Missile, "anti_radiation_missile", 
{
	controller = {
		boost_start = 0.001,
		march_start = 1.001,
	},
	
	booster = {
		impulse								= 185,
		fuel_mass							= 15,
		work_time							= 1.0,
		boost_time							= 0,
		boost_factor						= 0,
		nozzle_position						= {{-2, -0.13, 0}},
		nozzle_orientationXYZ				= {{0.0, 0.0, 0.0}},
		tail_width							= 0.4,
		smoke_color							= {0.9, 0.9, 0.9},
		smoke_transparency					= 0.5,
		custom_smoke_dissipation_factor		= 0.3,				
	},
		
	march = {
		impulse								= 210,
		fuel_mass							= 55.0,
		work_time							= 9.0,
		boost_time							= 0,
		boost_factor						= 0,
		nozzle_position						= {{-2, -0.13, 0}},
		nozzle_orientationXYZ				= {{0.0, 0.0, 0.0}},
		tail_width							= 0.3,
		smoke_color							= {0.8, 0.8, 0.8},
		smoke_transparency					= 0.5,
		custom_smoke_dissipation_factor		= 0.3,	
	},
	
	fm = {
		mass        = 265,  
		caliber     = 0.254,  
		cx_coeff    = {1,0.65,1.0,0.6,1.3},
		L           = 4.3,
		I           = 1 / 12 * 265 * 4.3 * 4.3,
		Ma          = 0.3,
		Mw          = 1.2,
		wind_sigma	= 0.0,
		wind_time	= 0.0,
		Sw			= 1.16,
		dCydA		= {0.07, 0.036},
		A			= 0.36,
		maxAoa		= 0.28,
		finsTau		= 0.1,
		lockRoll	= 1,
		
		Ma_x		= 0.001,
		Kw_x		= 0.001,
		I_x			= 40,
	},
	
	radio_seeker = {
		FOV					= math.rad(6),	-- !	���� ������
		op_time				= 300,			-- !	����� ������ �������
		keep_aim_time		= 5,			-- !	����� �� ������ ������ ����� ���� ����� ������ �������
		pos_memory_time		= 150,			-- !	����� �� ������ ������ � ����� ������������
		sens_near_dist		= 150.0,		--		����������� ������� ���������	 
		sens_far_dist		= 70000.0,		--		������������ ������� ���������
		err_correct_time	= 1.2,			-- !!	������ ���������� ���� � ���� (��������������� ������)
		err_val				= 0.0026,		-- !!	������������ ������� ������
		calc_aim_dist		= 500000,		-- 		������������ ���������� ��� ���������� ����� ������������ (����)
		blind_rad_val		= 0.1,			--		������������ ������� �������� ������� �� ������� (����) (������ �-��� ����������)
		blind_ctrl_dist		= 2100,			--		���������� ��� ����� ������������ �� ����� ���������� (����)
		aim_y_offset		= 3.5,			-- !!	����� ������� �� ������ ������������ aimPoint ��������� ����
	},
	
	simple_gyrostab_seeker = {
		omega_max	= math.rad(8)
	},
	
	fuze_proximity = {
		ignore_inp_armed = 1,
	},
	
	autopilot = {
		K				 = 100.0,
		Kg				 = 2.4,
		Ki				 = 0.0,
		finsLimit		 = 0.08,
		useJumpByDefault = 1,
		J_Power_K		 = 1.9,
		J_Diff_K		 = 0.4,
		J_Int_K			 = 0.0,
		J_Angle_K		 = math.rad(8),
		J_FinAngle_K	 = math.rad(16),
		delay			 = 1.0,
		J_Angle_W		 = 3.5,
	},
	
	start_helper = {
		delay				= 1.0,
		power				= 0.1,
		time				= 1,
		use_local_coord		= 0,
		max_vel				= 200,
		max_height			= 200,
		vh_logic_or			= 0,
	},
	
	warhead = warheads["ALARM"],
	warhead_air = warheads["ALARM"]
}, 

{ mass = 265 }
);


declare_missile("BGM_109B", _("BGM-109B"), "tomahawk", "wAmmunitionCruise", wsType_SS_Missile, "cruise_missile_vert_start_booster_drop", 
{
	controller = {
		boost_start	= 0.01,
		march_start = 8.0,
	},
	
	fm = {
		mass			= 1440,  
		caliber			= 0.52,  
		cx_coeff		= {1, 0.3, 0.65, 0.018, 1.6},
		L				= 5.56,
		I				= 3000,
		Ma				= 2,
		Mw				= 4,
		wind_sigma		= 0.0,
		wind_time		= 0.0,
		Sw				= 1.2,
		dCydA			= {0.07, 0.036},
		A				= 0.08,
		maxAoa			= 0.3,
		finsTau			= 0.2,
		Ma_x			= 2,
		Kw_x			= 0.07,
		addDeplSw		= 1.0,
		wingsDeplDelay	= 20,
	},
	
	booster_animation = {
		start_val = 1,
	},
	
	play_booster_animation = {
		val = 0,
	},
	
	boost = {	--	air launch - no booster
		impulse								= 160,
		fuel_mass							= 247.5,
		work_time							= 7,
		boost_time							= 0,
		boost_factor						= 0,
		nozzle_position						= {{-3.64, 0, 0}},
		nozzle_orientationXYZ				= {{0, 0, 0}},
		tail_width							= 0.5,
		smoke_color							= {1.0, 1.0, 1.0},
		smoke_transparency					= 0.5,
		custom_smoke_dissipation_factor		= 0.5,
			
		wind_sigma					= 0.0,
		impulse_sigma				= 1,
		moment_sigma				= 1000000.0,
			
		mass						= 50.0,	
		caliber						= 0.5,	
		L							= 0.3,
		I							= 100,
		Ma							= 0.5,
		Mw							= 0.1,
		Cx							= 0.9,
			
		model_name					= "tomahawk_booster",
		start_impulse				= 0,
	},
	
	march = {
		impulse			= 500,
		fuel_mass		= 260.0,
		work_time		= 9999,
		min_fuel_rate	= 0.005,
		min_thrust		= -100,
		max_thrust		= 3800,
		thrust_Tau		= 0.0017,
	},
	
	control_block ={
		seeker_activation_dist		= 12000,
		default_cruise_height		= 50,
		obj_sensor					= 1,
		can_update_target_pos		= 0,
		turn_before_point_reach		= 1,
		turn_hor_N					= 0.8,
		turn_max_calc_angle_deg		= 90,
		turn_point_trigger_dist		= 100,
	},
	
	start_autopilot = {
		delay			= 0.5,
		rollCtrlDelay	= 15.0,
		opTime 			= 20.0,
		K				= 6.4,
		Kd				= 90.0,
		rK				= 0.02,
		rKd				= 0.0001,
		finsLimit		= 60.0,
	},
	
	final_autopilot =		{
		delay				= 0,
		K					= 320,
		Ki					= 0.001,
		Kg					= 4,
		finsLimit			= 0.4,
		useJumpByDefault	= 1,
		J_Power_K			= 1.2,
		J_Diff_K			= 0.1,
		J_Int_K				= 0.001,
		J_Angle_K			= 0.23,
		J_FinAngle_K		= 0.38,
		J_Angle_W			= 0.4,
		hKp_err				= 100,
		hKp_err_croll		= 0.04,
		hKd					= 0.005,
		max_roll			= 0.7,
	},
	
	cruise_autopilot = {
		delay				= 1,
		Kp_hor_err			= 240,
		Kp_hor_err_croll	= 0.08,
		Kd_hor				= 0,
		Kp_ver				= 20,
		Kii_ver				= 0.4,
		Kd_ver				= 0.0,
		Kp_eng				= 265,
		Ki_eng				= 0.003,
		Kd_eng				= 0,
		Kp_ver_st1			= 0.009,
		Kd_ver_st1			= 0.015,
		Kp_ver_st2			= 0.00018,
		Kd_ver_st2			= 0.00005,
		
		auto_terrain_following			= 1,
		auto_terrain_following_height	= 50,
		
		alg_points_num			= 7,
		alg_calc_time			= 1.5,
		alg_vel_k				= 6,
		alg_div_k				= 2,
		alg_max_sin_climb		= 0.7,
		alg_section_temp_points	= 3,
		alg_tmp_point_vel_k		= 1.5,
		no_alg_vel_k			= 10,
		
		max_roll			= 0.7,
		max_start_y_vel		= 15,
		stab_vel			= 237.5,
		finsLimit			= 0.8,
		estimated_N_max		= 2,
		eng_min_thrust		= -100,
		eng_max_thrust		= 3000,		
	},
	
			
	warhead		= warheads["BGM_109B"],
	warhead_air = warheads["BGM_109B"],	
}, 

{ mass = 1440 }
);


declare_missile("AIM_7", _('AIM-7M'), "aim-7", "wAmmunitionSelfHoming", wsType_AA_Missile, "aa_missile_semi_active",
{
	controller = {
		boost_start = 0.5,
		march_start = 4.2,
	},
	
	boost = {
		impulse								= 247,
		fuel_mass							= 38.48,
		work_time							= 3.7,
		nozzle_position						= {{-1.9, 0, 0}},
		nozzle_orientationXYZ				= {{0.0, 0.0, 0.0}},
		tail_width							= 0.4,
		smoke_color							= {1.0, 1.0, 1.0},
		smoke_transparency					= 0.9,
		custom_smoke_dissipation_factor		= 0.2,	
	},
	
	march = {
		impulse								= 209,
		fuel_mass							= 21.82,
		work_time							= 10.8,
		nozzle_position						= {{-1.9, 0, 0}},
		nozzle_orientationXYZ				= {{0.0, 0.0, 0.0}},
		tail_width							= 0.4,
		smoke_color							= {1.0, 1.0, 1.0},
		smoke_transparency					= 0.9,
		custom_smoke_dissipation_factor		= 0.2,
		
--		fuel_rate_data	=	{	--t		rate
--								0.0,	2.0,
--								4.0,	1.8,
--							},
	},
	
	fm = {
		mass				= 231,  
		caliber				= 0.2,  
		wind_sigma			= 0.0,
		wind_time			= 0.0,
		tail_first			= 1,
		fins_part_val		= 0,
		rotated_fins_inp	= 0,
		delta_max			= math.rad(20),
		L					= 0.2,
		S					= 0.0324,
		Ix					= 3.5,
		Iy					= 127.4,
		Iz					= 127.4,
		
		Mxd					= 0.3 * 57.3,
		Mxw					= -44.5,

		table_scale	= 0.2,
		table_degree_values = 1,
	--	Mach	  | 0.0		0.2		0.4		0.6		0.8		1.0		1.2		1.4		1.6		1.8		2.0		2.2		2.4		2.6		2.8		3.0		3.2		3.4		3.6		3.8		4.0	 |
		Cx0 	= {	0.34,	0.34,	0.34,	0.34,	0.35,	1.10,	1.27,	1.23,	1.19,	1.12,	1.05,	1.0,	0.95,	0.91,	0.87,	0.84,	0.81,	0.78,	0.76,	0.74,	0.72 },
		CxB 	= {	0.11,	0.11,	0.11,	0.11,	0.11,	0.40,	0.19,	0.17,	0.16,	0.14,	0.13,	0.12,	0.12,	0.11,	0.11,	0.10,	0.09,	0.09,	0.08,	0.08,	0.07 },
		K1		= { 0.0056,	0.0056,	0.0056,	0.0056,	0.0056,	0.0056,	0.0056,	0.0056,	0.0052,	0.0048,	0.0045,	0.0041,	0.0037,	0.0036,	0.0034,	0.0032,	0.0031,	0.0030,	0.0029,	0.0027,	0.0026 },
		K2		= { 0.0055,	0.0055,	0.0055,	0.0055,	0.0055,	0.0055,	0.0055,	0.0055,	0.0051,	0.0047,	0.0043,	0.0037,	0.0031,	0.0032,	0.0033,	0.0035,	0.0036,	0.0037,	0.0038,	0.0039,	0.0040 },
		Cya		= { 1.14,	1.14,	1.14,	1.14,	1.14,	1.42,	1.46,	1.39,	1.32,	1.15,	1.06,	1.00,	0.94,	0.89,	0.83,	0.78,	0.73,	0.69,	0.65,	0.61,	0.57 },
		Cza		= { 1.14,	1.14,	1.14,	1.14,	1.14,	1.42,	1.46,	1.39,	1.32,	1.15,	1.06,	1.00,	0.94,	0.89,	0.83,	0.78,	0.73,	0.69,	0.65,	0.61,	0.57 },
		Mya		= { -0.5,	-0.5},
		Mza		= { -0.5,	-0.5},
		Myw		= { -2.0,	-2.0},
		Mzw		= { -2.0,	-2.0},
		A1trim	= { 10.0,	10.0},
		A2trim	= { 10.0,	10.0},
		
		model_roll = math.rad(45),
		fins_stall = 1,
	},
	
	proximity_fuze = {
		radius		= 12,
		arm_delay	= 1.6,
	},
	
	seeker = {
		delay					= 2.0,
		op_time					= 75,
		FOV						= math.rad(120),
		max_w_LOS				= math.rad(20),
		sens_near_dist			= 100,
		sens_far_dist			= 30000,
		ccm_k0					= 1,
		aim_sigma				= 5.5,
		height_error_k			= 100;
		height_error_max_vel	= 138;
		height_error_max_h		= 300;
	},

	autopilot = {
		delay				= 1.5,
		op_time				= 75,
		Kconv				= 3.0,
		Knv					= 0.005,
		Kd					= 0.4,
		Ki					= 0.25,
		Kout				= 1.0,
		Kx					= 0.1,
		Krx					= 2.0,
		fins_limit			= math.rad(20),
		fins_limit_x		= math.rad(5),
		Areq_limit			= 25.0,
		bang_bang			= 0,
		max_side_N			= 10,
		max_signal_Fi		= math.rad(12),
		rotate_fins_output	= 0,
		alg					= 0,
		PN_dist_data 		= {	15000,	1,
								9000,	1	},
		draw_fins_conv		= {math.rad(90),1,1},
		null_roll			= math.rad(45),
		
		loft_active_by_default	= 1,
		loft_add_pitch			= math.rad(30),
		loft_time				= 4.0,
		loft_min_dist			= 6000,
	},
	
	warhead		= warheads["AIM_7"],
	warhead_air = warheads["AIM_7"]
}, 

{ mass = 230 }
);
