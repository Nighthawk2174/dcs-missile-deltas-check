dofile(LockOn_Options.script_path.."devices.lua")
dofile(LockOn_Options.script_path.."command_defs.lua")

local gettext = require("i_18n")
_ = gettext.translate

local dev = GetSelf()

local update_time_step = 0.1
make_default_activity(update_time_step)

local cmdfifo={}

-- listen for all the pertinent command IDs
for i=10000,device_commands.PROXY_last_command_sentinel do
  dev:listen_command(i)
end

function post_initialize()
    -- init device objects (names in devices.lua)
    weaponssystem = GetDevice(devices.WEAPONS)
    hotas = GetDevice(devices.HOTAS)
    hud = GetDevice(devices.HUD)
    vdi = GetDevice(devices.VDI)
    hsd = GetDevice(devices.HSD)
    radar = GetDevice(devices.RADAR)
    gearhook = GetDevice(devices.GEARHOOK)
    wingsweep = GetDevice(devices.WINGSWEEP)
    afcs = GetDevice(devices.AFCS)
    nav = GetDevice(devices.NAV_INTERFACE)
end

function ProcCommand(command)
    if command == device_commands.PROXY_Enable_ACM_guns_AA then -- 10000
        -- [lift ACM cover,] lift master arm, enable master arm switch, switch to guns, display mode air-to-air, TID repeat
        -- weaponssystem:performClickableAction(device_commands.WEAP_ACM_Cover, 1.0, false)
        weaponssystem:performClickableAction(device_commands.WEAP_Master_Arm_And_Cover, 1.0, false)
        hotas:performClickableAction(device_commands.STICK_Weapon_Selector_up, 1.0, false) -- select guns
        hud:performClickableAction(device_commands.DISP_mode_air2air, 1.0, false)
        hsd:performClickableAction(device_commands.HSD_Display_Mode_Cycle, 0.0, false) -- TID repeat
        --hotas:performClickableAction(device_commands.THROTTLE_CAGE_SEAM, 1.0, false) -- RTGS
        --hotas:performClickableAction(device_commands.THROTTLE_CAGE_SEAM, 0.0, false) -- release pushbutton
        afcs:performClickableAction(device_commands.AFCS_Stability_Roll, 0.0, false) -- disable roll SAS
    elseif command == device_commands.PROXY_carrier_landing then -- 10001
        gearhook:performClickableAction(device_commands.HOOK_Handle, 1.0, false)
        hud:performClickableAction(device_commands.DISP_mode_landing, 1.0, false)
        wingsweep:performClickableAction(device_commands.WINGSWEEP_FwdButton, 1.0, false)
    elseif command == device_commands.PROXY_ground_landing then -- 10002
        hud:performClickableAction(device_commands.DISP_mode_landing, 1.0, false)
        gearhook:performClickableAction(device_commands.HOOK_Handle, 0.0, false)
    elseif command == device_commands.PROXY_case1_recovery then --10003
        gearhook:performClickableAction(device_commands.HOOK_Handle, 1.0, false)
        hud:performClickableAction(device_commands.DISP_mode_landing, 1.0, false)
        wingsweep:performClickableAction(device_commands.WINGSWEEP_AftButton, 1.0, false)
        hud:performClickableAction(device_commands.DISP_HUD_AWL_mode, 1.0, false)
        vdi:performClickableAction(device_commands.DISP_VDI_landing_mode, 1.0, false)
        nav:performClickableAction(device_commands.NAV_Btn_Steer_AWL_PCD, 1.0, false)
    elseif command == device_commands.PROXY_pilot_VSL then --10004
        radar:performClickableAction(device_commands.WEAP_Target_designate_down, 1.0, false) -- pilot VSL
    elseif command == device_commands.PROXY_wings_swept then --10005
        wingsweep:performClickableAction(device_commands.WINGSWEEP_AftButton, 1.0, false)
        hud:performClickableAction(device_commands.DISP_mode_landing, 1.0, false)
    elseif command == device_commands.PROXY_a2g then --10006
        weaponssystem:performClickableAction(device_commands.WEAP_Master_Arm_And_Cover, 1.0, false)
        hud:performClickableAction(device_commands.DISP_mode_air2ground, 1.0, false)
        vdi:performClickableAction(device_commands.DISP_VDI_display_mode, 1.0, false)
    elseif command == device_commands.PROXY_parkingbrake then -- 10007
        gearhook:performClickableAction(device_commands.BRAKE_ParkingBrake, 0.0, false)
    elseif command == device_commands.PROXY_radarwow then -- 10008
        radar:performClickableAction(device_commands.RADAR_bypass_wow, 1.0, false)
    else
        print_message_to_user("Unhandled proxy command "..tostring(command)..", val="..tostring(value))
    end
end

function update()
    while (#cmdfifo > 0) do
        local command = table.remove(cmdfifo,1)
        ProcCommand(command)
    end
end

function SetCommand(command,value)
    table.insert(cmdfifo, command)
end

need_to_be_closed = false -- close lua state after initialization
