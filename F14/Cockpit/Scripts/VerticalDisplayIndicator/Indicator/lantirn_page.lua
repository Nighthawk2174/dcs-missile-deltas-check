dofile(LockOn_Options.script_path.."Scripts/VerticalDisplayIndicator/Indicator/definitions.lua")

dofile(LockOn_Options.script_path.."Scripts/Common/levels.lua")
INDICATOR_LEVEL = VDI_DEFAULT_LEVEL
dofile(LockOn_Options.script_path.."Scripts/Common/common_defs.lua")

SetScale(FOV)
half_width   = GetScale() -- size in meters
half_height  = GetAspect() * half_width
aspect       = GetAspect()

LANTIRN_FX_MATERIAL_NAME = "LANTIRNMaterialVDI"
LANTIRN_MATERIAL_NAME = "LANTIRN_MAT_VDI"
LANTIRN_FONT_MATERIAL = "font_LANTIRN_VDI"

TV_LEVEL = VDI_DEFAULT_LEVEL

dofile(general_page_path.."lantirn_page.lua")
