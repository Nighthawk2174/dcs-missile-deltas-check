--[[
local background = CreateElement "ceMeshPoly"
    background.name = create_guid_string()
    background.primitivetype = "triangles"
    background.vertices = { {-1,-aspect},
                      {-1,aspect},
                      { 1,aspect},
                      {1,-aspect}}
    background.indices = default_box_indices
    background.material = MakeMaterial(nil,{0,0,0,255})
AddElement(background)
--]]

local TID_fontsize=0.056 * half_height

function create_root(name)
	local root_origin = create_origin(name)
    root_origin.controllers = {{name}}
    return root_origin
end

local tactical_symbols = create_root("tactical_symbols")

local aircraft_origin = create_origin("TID_ownAC")
aircraft_origin.parent_element = tactical_symbols.name
aircraft_origin.controllers = {{"TID_origin"}}

-- aircraft reticle
local aircraft_reticle = my_create_textured_box_1k(200,400,200,200,nil,nil,1/4)
    aircraft_reticle.name = create_guid_string()
    aircraft_reticle.material = "TID_MAT2"
    aircraft_reticle.parent_element = aircraft_origin.name
    aircraft_reticle.isdraw = true
    aircraft_reticle.isvisible = true
AddElement(aircraft_reticle)

local steer_ref = create_origin()
steer_ref.controllers = {{"TID_steer_ref"}}
steer_ref.parent_element = tactical_symbols.name
local vertical_line = my_create_textured_box_1k(800,600,200,200,nil,nil,1/4)
    vertical_line.name = create_guid_string()
    vertical_line.material = "TID_MAT2"
    vertical_line.parent_element = steer_ref.name
    vertical_line.isdraw = true
    vertical_line.isvisible = true
AddElement(vertical_line)
local horizontal_line = my_create_textured_box_1k(0,800,200,200,nil,nil,1/4)
    horizontal_line.name = create_guid_string()
    horizontal_line.material = "TID_MAT2"
    horizontal_line.parent_element = steer_ref.name
    horizontal_line.isdraw = true
    horizontal_line.isvisible = true
AddElement(horizontal_line)

local ownshipvec = create_textured_line_2k(22, 10, 880, 12)
    ownshipvec.name 	= create_guid_string()
    ownshipvec.material = "TID_MAT"
    ownshipvec.parent_element = aircraft_origin.name
    ownshipvec.controllers = {{"TID_ownshipvec"}}
    ownshipvec.isvisible = true
AddElement(ownshipvec)

-- line from center to ownship in gnd stab when offscreen
local lineto_ownship = my_create_textured_box_2k(2033,23,8,2007,2040,2025)
    lineto_ownship.name = create_guid_string()
    lineto_ownship.parent_element = tactical_symbols.name
    lineto_ownship.material = "TID_MAT"
    lineto_ownship.isdraw = true
    lineto_ownship.isvisible = true
    lineto_ownship.controllers = {{"TID_lineto_ownship"}}
AddElement(lineto_ownship)

local center_plus = my_create_textured_box_2k(546,897,47,47)
    center_plus.name = create_guid_string()
    center_plus.parent_element = tactical_symbols.name
    center_plus.material = "TID_MAT"
    center_plus.isdraw = false
    center_plus.isvisible = true
    --center_plus.controllers = {{"TID_centerplus"}}
AddElement(center_plus)

function create_waypoint(name,text_x,text_y,xsize,ysize,xcenter,ycenter,show_alt,show_dot,num)
    local wp_origin = create_origin("TID_wp_"..tostring(num))
    wp_origin.controllers = {{"TID_waypoint",num}, {"TID_wp_bright",num}}
    wp_origin.parent_element = tactical_symbols.name
    local waypoint = my_create_textured_box_1k(text_x,text_y,xsize,ysize,xcenter,ycenter,1/4)
        waypoint.name = create_guid_string()
        waypoint.parent_element = wp_origin.name
        waypoint.controllers = {{"TID_wp_bright",num},{"TID_wp_sym",num}}
        waypoint.material = "TID_MAT2"
        waypoint.isdraw = true
        waypoint.isvisible = true
    AddElement(waypoint)
    if (show_alt) then
        local wp_alt_str           = CreateElement "ceStringPoly"
            wp_alt_str.name            = create_guid_string()
            wp_alt_str.material        = "font_TID"
            wp_alt_str.parent_element = wp_origin.name
            wp_alt_str.stringdefs    = {TID_fontsize, TID_fontsize, 0, 0}
            wp_alt_str.init_pos = {-0.03,0}
            wp_alt_str.alignment     = "RightCenter"
            wp_alt_str.controllers = {{"TID_wp_alt",num},{"TID_wp_bright",num}}
            wp_alt_str.isdraw = true
        AddElement(wp_alt_str)
    end
    local waypoint_dot = my_create_textured_box_1k(0,0,200,200,xcenter,ycenter,1/4)
        waypoint_dot.name = create_guid_string()
        waypoint_dot.parent_element = wp_origin.name
        waypoint_dot.material = "TID_MAT2"
        waypoint_dot.isdraw = true
        waypoint_dot.isvisible = true
        local b_show_dot = show_dot and 1 or 0
        waypoint_dot.controllers = {{"TID_wp_bright",num},{"TID_wp_dot",num,b_show_dot}}
    AddElement(waypoint_dot)
    if (name == "DP") then
        -- special case
        local diagonal_line = my_create_textured_box_1k(800,200,200,200,xcenter,ycenter,1/4)
            diagonal_line.name = create_guid_string()
            diagonal_line.parent_element = wp_origin.name
            diagonal_line.init_rot = {90,0,0}
            diagonal_line.controllers = {{"TID_wp_bright",num},{"TID_wp_sym",num}}
            diagonal_line.material = "TID_MAT2"
            diagonal_line.isdraw = true
            diagonal_line.isvisible = true
            --diagonal_line.controllers = {{"TID_diagonal_line",num}}
        AddElement(diagonal_line)
    end
    if (name == "DLHB" or name == "DLST") then
        -- special case
        local diagonal_line = my_create_textured_box_1k(800,200,200,200,xcenter,ycenter,1/4)
            diagonal_line.name = create_guid_string()
            diagonal_line.parent_element = wp_origin.name
            diagonal_line.controllers = {{"TID_wp_bright",num},{"TID_wp_sym",num}}
            diagonal_line.material = "TID_MAT2"
            diagonal_line.isdraw = true
            diagonal_line.isvisible = true
            --diagonal_line.controllers = {{"TID_diagonal_line",num}}
        AddElement(diagonal_line)
    end
    if (name == "DLFP") then
        -- special case
        local vertical_line = my_create_textured_box_1k(800,600,200,200,xcenter,ycenter,1/4)
            vertical_line.name = create_guid_string()
            vertical_line.parent_element = wp_origin.name
            vertical_line.controllers = {{"TID_wp_bright",num},{"TID_wp_sym",num}}
            vertical_line.material = "TID_MAT2"
            vertical_line.isdraw = true
            vertical_line.isvisible = true
            --vertical_line.controllers = {{"TID_vertical_line",num}}
        AddElement(vertical_line)
    end
    if (name:sub(1, #"WP") == "WP") then
        local wp_num_str           = CreateElement "ceStringPoly"
            wp_num_str.name            = create_guid_string()
            wp_num_str.material        = "font_TID"
            wp_num_str.parent_element = wp_origin.name
            wp_num_str.stringdefs    = {TID_fontsize, TID_fontsize, 0, 0}
            wp_num_str.init_pos = {-0.03,0}
            wp_num_str.alignment     = "RightCenter"
            wp_num_str.controllers = {{"TID_wp_bright",num},{"TID_wp_sym",num}}
            wp_num_str.value = name:sub(3, -1)
            wp_num_str.isdraw = true
        AddElement(wp_num_str)
    end
    if (name:sub(1, #"DLWP") == "DLWP") then
        local wp_num_str           = CreateElement "ceStringPoly"
            wp_num_str.name            = create_guid_string()
            wp_num_str.material        = "font_TID"
            wp_num_str.parent_element = wp_origin.name
            wp_num_str.stringdefs    = {TID_fontsize, TID_fontsize, 0, 0}
            wp_num_str.init_pos = {-0.03,0}
            wp_num_str.alignment     = "RightCenter"
            wp_num_str.controllers = {{"TID_wp_bright",num},{"TID_wp_sym",num}}
            wp_num_str.value = name:sub(5, -1)
            wp_num_str.isdraw = true
        AddElement(wp_num_str)
    end
end

create_waypoint("WP1",0,400,200,200,nil,nil,false,true,1) -- WP1
create_waypoint("WP2",0,400,200,200,nil,nil,false,true,2) -- WP2
create_waypoint("WP3",0,400,200,200,nil,nil,false,true,3) -- WP3
create_waypoint("FP",0,600,200,200,nil,nil,false,false,4) -- FP (fixed point)
create_waypoint("IP",800,0,200,200,nil,nil,true,false,5) -- IP (initial/indication point)
create_waypoint("ST",600,200,200,200,nil,nil,true,false,6) -- ST (surface target)
create_waypoint("HB",200,200,200,200,nil,nil,false,true,7) -- HB (home base)
create_waypoint("DP",200,400,200,200,nil,nil,true,true,8) -- DP (defended point)
create_waypoint("HA",600,0,200,200,nil,nil,true,true,9) -- HA (hostile area)
create_waypoint("DLHB",200,200,200,200,nil,nil,false,true,10) -- TDS datalink HB (home base)
create_waypoint("DLWP1",200,600,200,200,nil,nil,false,true,11) -- TDS datalink WP1
create_waypoint("DLWP2",200,600,200,200,nil,nil,false,true,12) -- TDS datalink WP2
create_waypoint("DLWP3",200,600,200,200,nil,nil,false,true,13) -- TDS datalink WP3
create_waypoint("DLST",600,200,200,200,nil,nil,true,false,14) -- TDS datalink ST (surface target)
create_waypoint("DLFP",0,600,200,200,nil,nil,true,false,15) -- TDS datalink FP (fixed point)

-- left radar azimuth
local left_radar_az = my_create_textured_box_2k(2012,23,8,2007,2019,2025)
    left_radar_az.name = create_guid_string()
    left_radar_az.material = "TID_MAT"
    left_radar_az.parent_element = aircraft_origin.name
    left_radar_az.isdraw = true
    left_radar_az.isvisible = true
    left_radar_az.controllers = {{"TID_left_az"}}
    left_radar_az.state_tex_coords = {
        --get_tex_coord(2012,23,8,2007,2048,2048), -- 25nm
        get_tex_coord(1991,23,8,2007,2048,2048), -- 50nm
        get_tex_coord(1971,23,8,2007,2048,2048), -- 100nm
        get_tex_coord(1953,23,8,2007,2048,2048), -- 200nm
        get_tex_coord(1934,23,8,2007,2048,2048), -- 400nm
    }
AddElement(left_radar_az)

-- right radar azimuth
local right_radar_az = my_create_textured_box_2k(2012,23,8,2007,2019,2025)
    right_radar_az.name = create_guid_string()
    right_radar_az.material = "TID_MAT"
    right_radar_az.parent_element = aircraft_origin.name
    right_radar_az.isdraw = true
    right_radar_az.isvisible = true
    right_radar_az.controllers = {{"TID_right_az"}}
    right_radar_az.state_tex_coords = {
        --get_tex_coord(2012,23,8,2007,2048,2048), -- 25nm
        get_tex_coord(1991,23,8,2007,2048,2048), -- 50nm
        get_tex_coord(1971,23,8,2007,2048,2048), -- 100nm
        get_tex_coord(1953,23,8,2007,2048,2048), -- 200nm
        get_tex_coord(1934,23,8,2007,2048,2048), -- 400nm
    }
AddElement(right_radar_az)

-- artifical horizon
local artif_horiz = my_create_textured_box_2k(19,5,2003,10)
    artif_horiz.name = create_guid_string()
    artif_horiz.material = "TID_MAT"
    artif_horiz.parent_element = tactical_symbols.name
    artif_horiz.isdraw = true
    artif_horiz.isvisible = true
    artif_horiz.controllers = {{"TID_horizon"}}
AddElement(artif_horiz)

local steering_cue = my_create_textured_box_1k(600,600,200,200,nil,nil,1/4)
    steering_cue.name = create_guid_string()
    steering_cue.material = "TID_MAT2"
    steering_cue.isdraw = true
    steering_cue.isvisible = true
    steering_cue.parent_element = tactical_symbols.name
    steering_cue.controllers = {{"TID_steering_cue"}}
AddElement(steering_cue)

--local testcircle  = create_textured_circle_2k(22, 10, 880, 12)
--[[local ASE = CreateElement "ceCircleHB"
    ASE.width = 0.003 --3mm
    ASE.name 	= create_guid_string()
    --ASE.material = "TID_MAT" -- for textured circle
    ASE.material = "TID_COLOR" -- for untextured circle
    ASE.controllers = {{"TID_ase"}}
    ASE.isvisible = true
    ASE.isdraw = true
AddElement(ASE)--]]

local ASE_circle = add_stroke_circle(create_guid_string(), 0.2, nil, tactical_symbols.name, {{"TID_ase"}}, nil, nil, nil, nil, "TID_ASE_COLOR")

-- radar sweep
local radar_sweep = my_create_textured_box_2k(2033,23,8,2007,2040,2025)
    radar_sweep.name = create_guid_string()
    radar_sweep.material = "TID_MAT"
    radar_sweep.parent_element = aircraft_origin.name
    radar_sweep.isdraw = true
    radar_sweep.isvisible = true
    radar_sweep.controllers = {{"TID_sweep"}}
AddElement(radar_sweep)

-- TCS track symbol
local tcs_track = create_textured_line_2k(22, 10, 880, 12)
    tcs_track.name = create_guid_string()
    tcs_track.material = "TID_MAT"
    tcs_track.parent_element = aircraft_origin.name
    tcs_track.isdraw = true
    tcs_track.isvisible = true
    tcs_track.controllers = {{"TID_tcstrack"}}
AddElement(tcs_track)
local tcs_track_circle = my_create_textured_box_1k(200,400,200,200,nil,nil,1/4)
    tcs_track_circle.name = create_guid_string()
    tcs_track_circle.material = "TID_MAT2"
    tcs_track_circle.isdraw = true
    tcs_track_circle.isvisible = true
    tcs_track_circle.init_pos = {0.333,0}  -- 1.5"
    tcs_track_circle.parent_element = tcs_track.name
AddElement(tcs_track_circle)

function make_target(targetnum)
    local target_origin = create_origin("TID_tgt_"..tostring(targetnum))
    target_origin.controllers = {{"TID_target",targetnum}}
    target_origin.parent_element = tactical_symbols.name
    local target= my_create_textured_box_1k(400,200,200,200,500,275,1/4)
        target.name 	= create_guid_string()
        target.material = "TID_MAT2"
        target.parent_element = target_origin.name
        target.init_rot = {180,0,0}
        target.controllers = {{"TID_targettype",targetnum,0}, {"TID_targetbright",targetnum}}
        target.state_tex_coords = {
            --get_tex_coord(400,200,200,200,1024,1024), -- unknown target symbol
            get_tex_coord(600,400,200,200,1024,1024), -- enemy target symbol
            get_tex_coord(400,400,200,200,1024,1024), -- friendly target symbol
        }
        target.isvisible = true
    AddElement(target)
    local datalinktarget= my_create_textured_box_1k(400,200,200,200,500,275,1/4)
        datalinktarget.name 	= create_guid_string()
        datalinktarget.material = "TID_MAT2"
        datalinktarget.parent_element = target_origin.name
        datalinktarget.init_rot = {0,0,0}
        datalinktarget.controllers = {{"TID_targettype",targetnum,1}, {"TID_targetbright",targetnum}}
        datalinktarget.state_tex_coords = {
            --get_tex_coord(400,200,200,200,1024,1024), -- unknown datalinktarget symbol
            get_tex_coord(600,400,200,200,1024,1024), -- enemy datalinktarget symbol
            get_tex_coord(400,400,200,200,1024,1024), -- friendly datalinktarget symbol
        }
        datalinktarget.isvisible = true
    AddElement(datalinktarget)
    local target_dot = my_create_textured_box_1k(0,0,200,200,nil,nil,1/4)
        target_dot.name = create_guid_string()
        target_dot.parent_element = target_origin.name
        target_dot.controllers = {{"TID_targetbright",targetnum}}
        target_dot.material = "TID_MAT2"
        target_dot.isdraw = true
        target_dot.isvisible = true
    AddElement(target_dot)
    local target_multi = my_create_textured_box_1k(0,200,200,200,150,325,1/4)
        target_multi.name = create_guid_string()
        target_multi.parent_element = target_origin.name
        target_multi.controllers = {{"TID_tgt_multi",targetnum},{"TID_targetbright",targetnum}}
        target_multi.material = "TID_MAT2"
        target_multi.isdraw = false
        target_multi.isvisible = true
    AddElement(target_multi)
    local target_dont_attk = my_create_textured_box_1k(800,600,200,200,nil,nil,1/4)
        target_dont_attk.name = create_guid_string()
        target_dont_attk.parent_element = target_origin.name
        target_dont_attk.controllers = {{"TID_tgt_do_not_attk",targetnum},{"TID_targetbright",targetnum}}
        target_dont_attk.material = "TID_MAT2"
        target_dont_attk.isdraw = false
        target_dont_attk.isvisible = true
    AddElement(target_dont_attk)
    local targetvec = create_textured_line_2k(22, 10, 880, 12)
        targetvec.name 	= create_guid_string()
        targetvec.material = "TID_MAT"
        targetvec.parent_element = target_origin.name
        targetvec.controllers = {{"TID_targetvec",targetnum}, {"TID_targetbright",targetnum}}
        targetvec.isvisible = true
    AddElement(targetvec)
    local launchzone= create_textured_line_2k(22, 34, 880, 24)
        launchzone.name 	= create_guid_string()
        launchzone.material = "TID_MAT"
        launchzone.parent_element = target_origin.name
        launchzone.controllers = {{"TID_LZ",targetnum}, {"TID_targetbright",targetnum}}
        launchzone.isvisible = true
    AddElement(launchzone)
    local Rmax= my_create_textured_box_2k(145,841,17,17)
        Rmax.name 	= create_guid_string()
        Rmax.material = "TID_MAT"
        Rmax.parent_element = target_origin.name
        Rmax.controllers = {{"TID_Rmax",targetnum}, {"TID_targetbright",targetnum}}
        Rmax.isvisible = true
    AddElement(Rmax)
    local lostX= my_create_textured_box_2k(266,901,47,54)
        lostX.name 	= create_guid_string()
        lostX.material = "TID_MAT"
        lostX.parent_element = target_origin.name
        lostX.controllers = {{"TID_lostX",targetnum}, {"TID_targetbright",targetnum}}
        lostX.isvisible = true
    AddElement(lostX)
    local tgt_alt_str           = CreateElement "ceStringPoly"
        tgt_alt_str.name            = create_guid_string()
        tgt_alt_str.material        = "font_TID"
        tgt_alt_str.parent_element = target_origin.name
        tgt_alt_str.stringdefs    = {TID_fontsize, TID_fontsize, 0, 0}
        tgt_alt_str.init_pos = {-0.03,0}
        tgt_alt_str.alignment     = "RightCenter"
        tgt_alt_str.controllers 		= {{"TID_tgt_alt",targetnum}, {"TID_targetbright",targetnum}}
        tgt_alt_str.isdraw = false
    AddElement(tgt_alt_str)
    local tgt_prio_str           = CreateElement "ceStringPoly"
        tgt_prio_str.name            = create_guid_string()
        tgt_prio_str.material        = "font_TID"
        tgt_prio_str.parent_element = target_origin.name
        tgt_prio_str.stringdefs    = {TID_fontsize, TID_fontsize, 0, 0}
        tgt_prio_str.init_pos = {0.03,0}
        tgt_prio_str.alignment     = "LeftCenter"
        tgt_prio_str.controllers 		= {{"TID_tgt_prio",targetnum}, {"TID_targetbright",targetnum}}
        tgt_prio_str.isdraw = false
    AddElement(tgt_prio_str)

----    local targetvec= create_textured_box(8161,31,8161+5,31+460,8192,512,nil,nil,31)
--    local targetvec= create_textured_box(8138,1,8191,1+460,8192,512,nil,nil,1)
--        targetvec.element_params={28/8192,5/8192,(8163-28)/143} -- middle X texcoord of shortest line, width, distance to next
--        targetvec.name 	= create_guid_string()
--        targetvec.material = TID_LINE_FLIP
--        targetvec.parent_element = target_origin.name
--        targetvec.controllers = {{"TID_targetvec",targetnum}, {"TID_targetbright",targetnum}}
--        targetvec.isvisible = true
--    AddElement(targetvec)
end

for targetnum = 1,32 do
    make_target(targetnum)
end

-- cursor
local tid_cursor = my_create_textured_box_1k(200,400,200,200,nil,nil,1/4)
    tid_cursor.name = create_guid_string()
    tid_cursor.material = "TID_MAT2"
    tid_cursor.isdraw = true
    tid_cursor.isvisible = true
    tid_cursor.controllers = {{"TID_cursor"}}
    tid_cursor.parent_element = tactical_symbols.name
AddElement(tid_cursor)

local radar_elev_str           = CreateElement "ceStringPoly"
    radar_elev_str.name            = create_guid_string()
    radar_elev_str.material        = "font_TID"
    radar_elev_str.stringdefs    = {TID_fontsize, TID_fontsize, 0, 0}
    radar_elev_str.init_pos = {-0.75,0.04}
    --radar_elev_str.init_rot         = {0, 0, 0}
    radar_elev_str.alignment     = "CenterCenter"
    radar_elev_str.parent_element = tactical_symbols.name
    --radar_elev_str.formats		= {"%02.1f"}
    radar_elev_str.controllers 		= {{"TID_radar_el"}}
    --radar_elev_str.value         = "028.0"
    radar_elev_str.isdraw = false
AddElement(radar_elev_str)

local radar_upper_str           = CreateElement "ceStringPoly"
    radar_upper_str.name            = create_guid_string()
    radar_upper_str.parent_element = tactical_symbols.name
    radar_upper_str.material        = "font_TID"
    radar_upper_str.stringdefs    = {TID_fontsize, TID_fontsize, 0, 0}
    radar_upper_str.init_pos = {-0.75,0.12}
    radar_upper_str.alignment     = "CenterCenter"
    radar_upper_str.controllers 		= {{"TID_radar_ext",0}}
    radar_upper_str.isdraw = false
AddElement(radar_upper_str)

local radar_lower_str           = CreateElement "ceStringPoly"
    radar_lower_str.name            = create_guid_string()
    radar_lower_str.parent_element = tactical_symbols.name
    radar_lower_str.material        = "font_TID"
    radar_lower_str.stringdefs    = {TID_fontsize, TID_fontsize, 0, 0}
    radar_lower_str.init_pos = {-0.75,-0.04}
    radar_lower_str.alignment     = "CenterCenter"
    radar_lower_str.controllers 		= {{"TID_radar_ext",1}}
    radar_lower_str.isdraw = false
AddElement(radar_lower_str)

local nav_status_str           = CreateElement "ceStringPoly"
    nav_status_str.name            = create_guid_string()
    nav_status_str.parent_element = tactical_symbols.name
    nav_status_str.material        = "font_TID"
    nav_status_str.stringdefs    = {TID_fontsize, TID_fontsize, 0, 0}
    nav_status_str.init_pos = {0.5,-0.36}
    nav_status_str.alignment     = "CenterCenter"
    nav_status_str.controllers 		= {{"TID_nav_status"}}
    nav_status_str.isdraw = false
AddElement(nav_status_str)

local ownspeed_str           = CreateElement "ceStringPoly"
    ownspeed_str.name            = create_guid_string()
    ownspeed_str.parent_element = tactical_symbols.name
    ownspeed_str.material        = "font_TID"
    ownspeed_str.stringdefs    = {TID_fontsize, TID_fontsize, 0, 0}
    ownspeed_str.init_pos = {-0.6,-0.28}
    ownspeed_str.alignment     = "CenterCenter"
    ownspeed_str.controllers 		= {{"TID_ownspeed"}}
    ownspeed_str.isdraw = false
AddElement(ownspeed_str)

local wpn_status_str           = CreateElement "ceStringPoly"
    wpn_status_str.name            = create_guid_string()
    wpn_status_str.parent_element = tactical_symbols.name
    wpn_status_str.material        = "font_TID"
    wpn_status_str.stringdefs    = {TID_fontsize, TID_fontsize, 0, 0}
    wpn_status_str.init_pos = {0.46,-0.44}
    wpn_status_str.alignment     = "CenterCenter"
    wpn_status_str.controllers 		= {{"TID_wpn_status"}}
    wpn_status_str.isdraw = false
AddElement(wpn_status_str)

local closure_str           = CreateElement "ceStringPoly"
    closure_str.name            = create_guid_string()
    closure_str.parent_element = tactical_symbols.name
    closure_str.material        = "font_TID"
    closure_str.stringdefs    = {TID_fontsize, TID_fontsize, 0, 0}
    closure_str.init_pos = {0.75,0.04}
    closure_str.alignment     = "CenterCenter"
    closure_str.controllers 		= {{"TID_closure"}}
    closure_str.isdraw = false
AddElement(closure_str)

local run_indicator           = CreateElement "ceStringPoly"
    run_indicator.name            = create_guid_string()
    run_indicator.parent_element = tactical_symbols.name
    run_indicator.material        = "font_TID"
    run_indicator.stringdefs    = {TID_fontsize, TID_fontsize, 0, 0}
    run_indicator.init_pos = {0.75,-0.08}
    run_indicator.alignment     = "CenterCenter"
    run_indicator.controllers 		= {{"TID_run_indicator"}}
    run_indicator.isdraw = false
AddElement(run_indicator)

local readout_symbols = create_root("readout_symbols")

local buffer_register           = CreateElement "ceStringPoly"
    buffer_register.name            = create_guid_string()
    buffer_register.parent_element = readout_symbols.name
    buffer_register.material        = "font_TID"
    buffer_register.stringdefs    = {TID_fontsize, TID_fontsize, 0, 0}
    buffer_register.init_pos = {0,0.8}
    buffer_register.alignment     = "CenterCenter"
    buffer_register.controllers 		= {{"TID_buffer_register"}}
    buffer_register.isdraw = false
AddElement(buffer_register)
local left_readout           = CreateElement "ceStringPoly"
    left_readout.name            = create_guid_string()
    left_readout.parent_element = readout_symbols.name
    left_readout.material        = "font_TID"
    left_readout.stringdefs    = {TID_fontsize, TID_fontsize, 0, 0}
    left_readout.init_pos = {-0.5,0.74}
    left_readout.alignment     = "LeftCenter"
    left_readout.controllers 		= {{"TID_readout",0}}
    left_readout.isdraw = false
AddElement(left_readout)
local right_readout           = CreateElement "ceStringPoly"
    right_readout.name            = create_guid_string()
    right_readout.parent_element = readout_symbols.name
    right_readout.material        = "font_TID"
    right_readout.stringdefs    = {TID_fontsize, TID_fontsize, 0, 0}
    right_readout.init_pos = {0.5,0.74}
    right_readout.alignment     = "RightCenter"
    right_readout.controllers 		= {{"TID_readout",1}}
    right_readout.isdraw = false
AddElement(right_readout)
local flycatcher           = CreateElement "ceStringPoly"
    flycatcher.name            = create_guid_string()
    flycatcher.parent_element = readout_symbols.name
    flycatcher.material        = "font_TID"
    flycatcher.stringdefs    = {TID_fontsize, TID_fontsize, 0, 0}
    flycatcher.init_pos = {0,0.64}
    flycatcher.alignment     = "CenterCenter"
    flycatcher.controllers 		= {{"TID_flycatcher"}}
    flycatcher.isdraw = false
AddElement(flycatcher)

local align_symbols = create_root("align_symbols")
local align_time           = CreateElement "ceStringPoly"
    align_time.name            = create_guid_string()
    align_time.parent_element = align_symbols.name
    align_time.material        = "font_TID"
    align_time.stringdefs    = {TID_fontsize, TID_fontsize, 0, 0}
    align_time.init_pos = {-0.72,0.56}
    align_time.alignment     = "CenterCenter"
    align_time.controllers 		= {{"TID_aligntime"}}
    align_time.isdraw = false
AddElement(align_time)

local align_status           = CreateElement "ceStringPoly"
    align_status.name            = create_guid_string()
    align_status.parent_element = align_symbols.name
    align_status.material        = "font_TID"
    align_status.stringdefs    = {TID_fontsize, TID_fontsize, 0, 0}
    align_status.init_pos = {0,0.56}
    align_status.alignment     = "CenterCenter"
    align_status.controllers 		= {{"TID_alignstatus"}}
    align_status.isdraw = false
AddElement(align_status)

local align_ash           = CreateElement "ceStringPoly"
    align_ash.name            = create_guid_string()
    align_ash.parent_element = align_symbols.name
    align_ash.material        = "font_TID"
    align_ash.stringdefs    = {TID_fontsize, TID_fontsize, 0, 0}
    align_ash.init_pos = {0.36,0.56}
    align_ash.alignment     = "CenterCenter"
    align_ash.controllers 		= {{"TID_alignASH"}}
    align_ash.isdraw = false
AddElement(align_ash)

local coarse_align_marker = my_create_textured_box_1k(75,294,51,12,nil,nil,1/4)
    coarse_align_marker.name = create_guid_string()
    coarse_align_marker.material = "TID_MAT2"
    coarse_align_marker.parent_element = align_symbols.name
    coarse_align_marker.isdraw = true
    coarse_align_marker.isvisible = true
    coarse_align_marker.init_pos = {-0.3,0.56}
    coarse_align_marker.init_rot = {90,0,0}
AddElement(coarse_align_marker)
local alert_align_marker = my_create_textured_box_1k(75,294,51,12,nil,nil,1/4)
    alert_align_marker.name = create_guid_string()
    alert_align_marker.material = "TID_MAT2"
    alert_align_marker.parent_element = align_symbols.name
    alert_align_marker.isdraw = true
    alert_align_marker.isvisible = true
    alert_align_marker.init_pos = {0.17,0.56}
    alert_align_marker.init_rot = {90,0,0}
AddElement(alert_align_marker)
local fine_align_marker = my_create_textured_box_1k(75,294,51,12,nil,nil,1/4)
    fine_align_marker.name = create_guid_string()
    fine_align_marker.material = "TID_MAT2"
    fine_align_marker.parent_element = align_symbols.name
    fine_align_marker.isdraw = true
    fine_align_marker.isvisible = true
    fine_align_marker.init_pos = {0.57,0.56}
    fine_align_marker.init_rot = {90,0,0}
AddElement(fine_align_marker)
local align_caret = my_create_textured_box_1k(837,466,120,68,898,474,1/4)
    align_caret.name = create_guid_string()
    align_caret.material = "TID_MAT2"
    align_caret.parent_element = align_symbols.name
    align_caret.isdraw = true
    align_caret.isvisible = true
    align_caret.init_pos = {-0.57,0.6}
    align_caret.controllers = {{"TID_aligncaret", 0.57-(-0.57)}} -- range from -0.57 to 0.57
AddElement(align_caret)
local align_caret_top = my_create_textured_box_1k(837,466,120,68,898,474,1/4) -- complete the diamond with upside down caret
    align_caret_top.name = create_guid_string()
    align_caret_top.material = "TID_MAT2"
    align_caret_top.parent_element = align_caret.name
    align_caret_top.isdraw = true
    align_caret_top.isvisible = true
    align_caret_top.init_pos = {0,0} -- relative to caret
    align_caret_top.init_rot = {180,0,0}
    align_caret_top.controllers = {{"TID_aligndiamond"}}
AddElement(align_caret_top)
local align_dot = my_create_textured_box_1k(0,0,200,200,nil,nil,1/4)
    align_dot.name = create_guid_string()
    align_dot.material = "TID_MAT2"
    align_dot.parent_element = align_caret.name
    align_dot.isdraw = true
    align_dot.isvisible = true
    align_dot.init_pos = {0,0} -- relative to caret
    align_dot.controllers = {{"TID_aligndot"}}
AddElement(align_dot)

local mtm_symbols = create_root("mtm_symbols")
local mtm_status           = CreateElement "ceStringPoly"
    mtm_status.name            = create_guid_string()
    mtm_status.parent_element = mtm_symbols.name
    mtm_status.material        = "font_TID"
    mtm_status.stringdefs    = {TID_fontsize, TID_fontsize, 0, 0}
    mtm_status.init_pos = {-0.5,-0.5}   -- "lower left quadrant"
    mtm_status.alignment     = "CenterCenter"
    mtm_status.controllers 		= {{"TID_mtmstatus"}}
    mtm_status.isdraw = true
AddElement(mtm_status)


local spinwarn_symbols = create_root("spinwarn_symbols")
local yaw_diamond = create_origin()
local rates_half_size = 0.7
yaw_diamond.controllers = {{"TID_yawdiamond",2*rates_half_size}}
yaw_diamond.parent_element = spinwarn_symbols.name
yaw_diamond.init_pos = {-rates_half_size,-0.22}
    local yawdiamondbot = my_create_textured_box_1k(837,466,120,68,898,474,1/4)
        yawdiamondbot.name = create_guid_string()
        yawdiamondbot.material = "TID_MAT2"
        yawdiamondbot.parent_element = yaw_diamond.name
        yawdiamondbot.isdraw = true
        yawdiamondbot.isvisible = true
    AddElement(yawdiamondbot)
    local yawdiamondtop = my_create_textured_box_1k(837,466,120,68,898,474,1/4) -- complete the diamond with upside down caret
        yawdiamondtop.name = create_guid_string()
        yawdiamondtop.material = "TID_MAT2"
        yawdiamondtop.parent_element = yawdiamondbot.name
        yawdiamondtop.isdraw = true
        yawdiamondtop.isvisible = true
        yawdiamondtop.init_pos = {0,0} -- relative to bottom caret
        yawdiamondtop.init_rot = {180,0,0}
    AddElement(yawdiamondtop)
local rates={"30", "50", "70", "90", "110", "+"}
for num, rate in ipairs(rates) do
    local yawrate           = CreateElement "ceStringPoly"
    yawrate.name            = create_guid_string()
    yawrate.parent_element = spinwarn_symbols.name
    yawrate.material        = "font_TID"
    yawrate.stringdefs    = {TID_fontsize, TID_fontsize, 0, 0}
    yawrate.init_pos = {-rates_half_size+(num-1)*(2*rates_half_size/5),-0.3}
    yawrate.alignment     = "CenterCenter"
    yawrate.controllers = {{"TID_yawratenums"}}
    yawrate.isdraw = true
    yawrate.value = rate
    AddElement(yawrate)
end

local spinarrow = create_textured_box(20,75,20+1540,75+120,2048,2048,nil,1020,nil)
spinarrow.name = create_guid_string()
spinarrow.material = "TID_MAT"
spinarrow.parent_element = spinwarn_symbols.name
spinarrow.isdraw = true
spinarrow.isvisible = true
spinarrow.init_pos = {0,0.4}
spinarrow.controllers = {{"TID_spinarrow"}}
AddElement(spinarrow)



--[[
local testhwline  = create_textured_line_2k(22, 10, 880, 12)
    testhwline.name 	= create_guid_string()
    testhwline.material = "TID_MAT"
    testhwline.controllers = {{"TID_testhwline"}}
    testhwline.isvisible = true
    testhwline.isdraw = false
AddElement(testhwline)
--]]

--make_ruler_grid()