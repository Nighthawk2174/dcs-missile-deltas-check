cdata =
{
	modName               			= _('F-14B'),
	F14B                  			= _('F-14B'),
    CPDICT                			= _('Customized Cockpit'),
    RADIO_MENU_PTT_OPTIONS 			= _('Radio Menu And PTT Behaviour'),
    
    JESTER_LABEL                    = _('Jester AI Contract'),    
    JESTER_HM                       = _('Use head movement in order to select items in Jester menu'),
    JESTER_HM_TIP                   = _('Allows using your head movement for selection of menu items.'),
    JESTER_LC                       = _('Jester Landing Callouts'),
    JESTER_LC_TIP                   = _('Jester will assist during landings.'),    
    JESTER_STPSTT                   = _('Switch PD-STT to P-STT lock when going WVR'),
    JESTER_STPSTT_TIP               = _('P-STT is better suited in when close to target but does not allow AIM-54 data link updates.\nIf option is checked Jester will automatically try to switch PD-STT lock to P-STT when close to enemy.'),    
    JESTER_CAM                      = _('Jester menu camera'),
    JESTER_CAM_TIP                  = _('Show Jester camera in center of Jester menu.'),
    TID_A2G                         = _("Repeat RIO weapon type wheel on TID"),
    TID_A2G_TIP                     = _("For easier visibility, since RIO weapon type window is as difficult to read\nin-game as it was in real life. Shown in A/G mode with pilot weapon selector off.\n(Real F-14 could not repeat RIO weapon wheel on TID)"),
    WEAP_OFF_GUN                    = _("Require weapon selector press between OFF and GUN"),
    WEAP_OFF_GUN_TIP                = _("For enhanced realism, pilot weapon selector needs to be pressed while moving\nbetween OFF and GUN positions. This may be impractical with many consumer\njoysticks, hence it is an optional feature."),

    --[[
    --JESTER_Boring                   = _('Jester doesn\'t joke around'),
    JESTER_LandingCallouts          = _('Jester Landing Callouts'),
    JESTER_HeadMenu                 = _('Use head movement to select in menu.'),
    JESTER_Camera                   = _('Jester menu camera'),
    ]]
    
    default               			= _('Default_liv'),
    --english                 		= _('English_liv'),
}
