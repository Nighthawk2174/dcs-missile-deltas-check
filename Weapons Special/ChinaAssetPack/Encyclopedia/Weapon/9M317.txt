9M317 Surface-to-Air Missile

9m317


Name: 9M317
Type: Surface-to-Air Missile
Developed: Russia
Warhead weight, kg: 70
Guidance: Semi-active radar
Weight, kg: 710
G limit: 24
Length, m: 5.55
Body diameter, m: 0.4
Range, km: 3-50
Maximum Mach number: 4