052C DDG-171 Haikou

052c


Name: 052C DDG-171 Haikou (Luyang II class)
Type: Destroyer
Length, m: 155.5
Beam, m: 17.2
Draught, m: 6.1
Displacement, tons: 5,950 (standard), 6,288 (full)
Speed, knots: 32
Range, miles: 4,500 at 15 kts
Complement, men: 280

Armament: 
48 x HHQ-9 surface-to-air missiles
8 x YJ-62 anti-ship missiles
1 x Type 210 100 mm dual purpose gun
2 x Type 730 CIWS
6 x torpedo tubes

The Type 052C destroyer (NATO code name Luyang II class, or Lanzhou class after the lead ship) is a class of destroyer built by China. It features a four array phased array radar for 360-degree coverage. The radar is used in conjunction with vertically launched HHQ-9 long-range air defence missiles. The Type 052C was the first warship in the People's Liberation Army Navy Surface Force to have true long-range fleet air defence capability.

The 052C uses the same hull and propulsion as the preceding Type 052B destroyer. While the Type 052B uses a mixture of Russian and Chinese systems, the Type 052C uses predominantly Chinese systems, with a few sensors being notable exceptions.