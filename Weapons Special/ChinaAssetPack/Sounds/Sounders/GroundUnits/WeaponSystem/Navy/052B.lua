dofile("Tools.lua")
dofile("GroundUnits/WeaponSystem/Tools/weapons.lua")

dofile("GroundUnits/WeaponSystem/Tools/_cap_common_sounder.lua")


_052b_weapons = weapons:new()

-- main gun
_052b_weapons:addTurret(1)
_052b_weapons:addLauncher(1, 1, MainGun76)

-- YJ83
for i=2,17 do
    _052b_weapons:addLauncher(i, 1, AntiShipMissile)
end

-- 9M317
_052b_weapons:addTurret(22)
_052b_weapons:addLauncher(22, 1, SAM_Hot_Reload)

_052b_weapons:addTurret(23)
_052b_weapons:addLauncher(23, 1, SAM_Hot_Reload)


-- CIWS
_052b_weapons:addTurret(24)
_052b_weapons:addLauncher(24, 1, CIWS)

_052b_weapons:addTurret(25)
_052b_weapons:addLauncher(25, 1, CIWS)
