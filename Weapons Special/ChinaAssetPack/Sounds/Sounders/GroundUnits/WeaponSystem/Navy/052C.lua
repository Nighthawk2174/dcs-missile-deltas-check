dofile("Tools.lua")
dofile("GroundUnits/WeaponSystem/Tools/weapons.lua")

dofile("GroundUnits/WeaponSystem/Tools/_cap_common_sounder.lua")


_052c_weapons = weapons:new()

-- main gun
_052c_weapons:addTurret(1)
_052c_weapons:addLauncher(1, 1, MainGun100)

-- HHQ9 starts from 6
for i=6,53 do
    _052c_weapons:addLauncher(i, 1, SAM_Cold_Single)
end

-- CIWS
_052c_weapons:addTurret(54)
_052c_weapons:addLauncher(54, 1, CIWS)

_052c_weapons:addTurret(55)
_052c_weapons:addLauncher(55, 1, CIWS)

-- YJ62
for i=56,63 do
    _052c_weapons:addLauncher(i, 1, AntiShipMissile)
end
