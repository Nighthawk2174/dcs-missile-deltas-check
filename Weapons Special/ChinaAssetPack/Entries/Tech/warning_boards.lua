-- Warning Board 02

GT1 = {};
GT_t.ws = 0;
set_recursive_metatable(GT1, GT_t.generic_stationary)
set_recursive_metatable(GT1.chassis, GT_t.CH_t.STATIC);
GT1.chassis.mass = 10

GT1.visual.shape = "biaoyu"
GT1.visual.shape_dstr = ""
GT1.CustomAimPoint = {0.0,1.0,0.0};
--Burning after hit
GT1.visual.fire_size = 0.3 --relative burning size
GT1.visual.fire_pos = {-0.7,0.0,-2.2};
GT1.visual.fire_time = 400 --burning time (seconds)
GT1.visual.min_time_agony = 0;
GT1.visual.max_time_agony = 0;

GT1.Name = "warning_board_a"
GT1.DisplayName = _("Warning Board A")
GT1.Rate = 1

GT1.DetectionRange  = 0;
GT1.ThreatRange = 0;
GT1.mapclasskey = "P0091000076";
GT1.attribute = {
    wsType_Ground, wsType_Tank, wsType_Gun, wsType_GenericFort,
    --"Fortifications",
};
GT1.category = "Unarmed";

GT1.Countries = {'China'}

add_surface_unit(GT1)
