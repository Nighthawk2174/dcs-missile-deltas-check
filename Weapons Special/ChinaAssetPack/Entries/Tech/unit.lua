-- all in '/Entries/Navy/'
local tech_path = '/Entries/Tech/'

dofile(current_mod_path .. tech_path .. 'weapons.lua')

-- navy
dofile(current_mod_path .. tech_path .. '052B.lua')
dofile(current_mod_path .. tech_path .. '054A.lua')
dofile(current_mod_path .. tech_path .. '052C.lua')

-- vehicle
dofile(current_mod_path .. tech_path .. 'zbd04.lua')
dofile(current_mod_path .. tech_path .. 'hq7_ln.lua')


-- static
dofile(current_mod_path .. tech_path .. 'warning_boards.lua')
