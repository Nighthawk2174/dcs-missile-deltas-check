
local self_ID = 'China Asset Pack by Deka Ironwork Simulations and Eagle Dynamics'
local pack_name = 'China Asset Pack'

declare_plugin(self_ID, {
    encyclopedia_path = current_mod_path..'/Encyclopedia',
    installed     = true, -- if false that will be place holder , or advertising
    state         = 'installed',
    dirName       = current_mod_path,
    shortName     = pack_name,
    fileMenuName  = _(pack_name),
    displayName   = _(pack_name),
    developerName = _("Deka Ironwork Simulations and Eagle Dynamics"),
    version       = 'WIP',
    info = _('China Asset Pack is a free add-on for DCS World developed by Deka Ironwork Simulations. It contains several AI aircrafts, ground and navy units, as well as weapons that have served or are currently serving in China.'),

    --[[
    Missions =
    {
        {
            name = _("J-11A"),
            dir  = "Missions",
        },
    },    
    LogBook =
    {
        {
            name = _("J-11A"),
            type = "J-11A",
        },
    },
    ]]
})

-- models
mount_vfs_model_path (current_mod_path .. '/Shapes/Weapons')
mount_vfs_model_path (current_mod_path .. '/Shapes/J-11A')
mount_vfs_model_path (current_mod_path .. '/Shapes/KJ-2000')
mount_vfs_model_path (current_mod_path .. '/Shapes/Navy')
mount_vfs_model_path (current_mod_path .. '/Shapes/Vehicles')
mount_vfs_model_path (current_mod_path .. '/Shapes/Static')

-- textures
mount_vfs_texture_path (current_mod_path .. '/Textures/Weapons')
mount_vfs_texture_path (current_mod_path .. '/Textures/J-11A')
mount_vfs_texture_path (current_mod_path .. '/Textures/KJ-2000')
mount_vfs_texture_path (current_mod_path .. '/Textures/Navy/052b')
mount_vfs_texture_path (current_mod_path .. '/Textures/Navy/052c')
mount_vfs_texture_path (current_mod_path .. '/Textures/Navy/054a')
mount_vfs_texture_path (current_mod_path .. '/Textures/Vehicles/zbd04a')
mount_vfs_texture_path (current_mod_path .. '/Textures/Vehicles/hq7')
mount_vfs_texture_path (current_mod_path .. '/Textures/Static/')

-- liveries
mount_vfs_liveries_path (current_mod_path .. '/Liveries')
mount_vfs_texture_path(current_mod_path .. "/Skins/1/ME")

--[[
    Below sequence must NOT be changed!!!
]]

-- loadouts
dofile(current_mod_path .. '/Entries/Payload.lua')

-- load aircraft entries
dofile(current_mod_path .. '/Entries/Aircrafts.lua')
dofile(current_mod_path .. '/Entries/Tech.lua')

----------------------------------------------------------------------------------------
plugin_done()
