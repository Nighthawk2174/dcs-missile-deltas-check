dofile("Tools.lua")
dofile("GroundUnits/WeaponSystem/Tools/weapons.lua")

rapier_weapons = weapons:new()

rapier_weapons:addTurret(1,"GndTech/TurretRotation_Electric")
rapier_weapons:addLauncher(1, 1, {single_shot = "Weapons/MissileLaunch2"})
