declare_plugin("TechWeaponPack",
{
installed 	 	  = true,
state		 	  = "installed",
developerName	  = "Eagle Dynamics",
version		 	  = __DCS_VERSION__,		 
})


mount_vfs_liveries_path (current_mod_path ..  "/Liveries")
mount_vfs_model_path    (current_mod_path ..  "/Shapes")
-------------------------------------------------------------
mount_vfs_texture_path	(current_mod_path ..  "/Textures/S_75_Launcher")
mount_vfs_texture_path	(current_mod_path ..  "/Textures/S_75_Rocket")
mount_vfs_texture_path	(current_mod_path ..  "/Textures/SNR_75")
mount_vfs_texture_path	(current_mod_path ..  "/Textures/hy_launcher")
mount_vfs_texture_path	(current_mod_path ..  "/Textures/mys-m1")
mount_vfs_texture_path	(current_mod_path ..  "/Textures/rapier_fsa_launcher_radar")
mount_vfs_texture_path	(current_mod_path ..  "/Textures/rapier_fsa_optical_tracker_unit")
mount_vfs_texture_path	(current_mod_path ..  "/Textures/rapier_fsa_missile")
mount_vfs_texture_path	(current_mod_path ..  "/Textures/land_rover_101_fc")
mount_vfs_texture_path	(current_mod_path ..  "/Textures/land_rover_109_s3")

mount_vfs_texture_path	(current_mod_path ..  "/Textures/oil_platform")

mount_vfs_texture_path	(current_mod_path ..  "/Textures/boxcartrinity")
mount_vfs_texture_path	(current_mod_path ..  "/Textures/container_20ft")
mount_vfs_texture_path	(current_mod_path ..  "/Textures/container_40ft")
mount_vfs_texture_path	(current_mod_path ..  "/Textures/es44ah")
mount_vfs_texture_path	(current_mod_path ..  "/Textures/tankcartrinity")
mount_vfs_texture_path	(current_mod_path ..  "/Textures/wellcarnsc")
-------------------------------------------------------------

dofile(current_mod_path .."/Database/db_sensors.lua")
dofile(current_mod_path .."/Database/db_units_cars.lua")
dofile(current_mod_path .."/Database/db_ground_objects.lua")


dofile(current_mod_path..'/Database/SA_missiles.lua')
---------------------------------------------------------
plugin_done()