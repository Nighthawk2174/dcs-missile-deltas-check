RIM_116A =
{
	category		= CAT_MISSILES,
	name			= "RIM_116A",
	user_name		= _("RIM-116A"),
	scheme			= "self_homing_spin_missile",
	class_name		= "wAmmunitionSelfHoming",
	model			= "mim-72",--!to change
	mass			= 73.6,
	
	wsTypeOfWeapon 	= {wsType_Weapon,wsType_Missile,wsType_SA_Missile,WSTYPE_PLACEHOLDER},

	Escort			= 0,
	Head_Type		= 1,
	sigma			= {10, 10, 10},
	M				= 73.6,
	H_max			= 4000.0,
	H_min			= -1,
	Diam			= 127.0,
	Cx_pil			= 1,
	D_max			= 10000.0,
	D_min			= 500.0,
	Head_Form		= 0,
	Life_Time		= 24,
	Nr_max			= 20,
	v_min			= 140.0,
	v_mid			= 350.0,
	Mach_max		= 2.5,
	t_b				= 0.0,
	t_acc			= 5.0,
	t_marsh			= 0.0,
	Range_max		= 10000.0,
	H_min_t			= 5.0,
	Fi_start		= math.rad(1),
	Fi_rak			= 3.14152,
	Fi_excort		= 0.79,
	Fi_search		= 99.9,
	OmViz_max		= 99.9,
	X_back			= 0,
	Y_back			= 0,
	Z_back			= 0,
	Reflection		= 0.0182,
	KillDistance	= 5.0,

	shape_table_data =
	{
		{
			name		= "RIM_116A",
			file		= "mim-72",
			life		= 1,
			fire		= { 0, 1},
			username	= _("RIM-116A"),
			index		= WSTYPE_PLACEHOLDER,
		},
	},
	
	controller = {
		boost_start = 0.001,
		march_start = 0.501,
	},
	
	booster = {
		impulse								= 160,
		fuel_mass							= 2.0,
		work_time							= 0.5,
		boost_time							= 0,
		boost_factor						= 0,
		nozzle_position						= {{-1.6, 0.0, 0}},
		nozzle_orientationXYZ				= {{0.0, 0.0, 0.0}},
		tail_width							= 0.4,
		smoke_color							= {1.0, 1.0, 1.0},
		smoke_transparency					= 0.9,
		custom_smoke_dissipation_factor		= 0.3,				
	},
		
	march = {
		impulse								= 160,
		fuel_mass							= 27,
		work_time							= 9,
		boost_time							= 0,
		boost_factor						= 0,
		nozzle_position						= {{-1.6, 0.0, 0}},
		nozzle_orientationXYZ				= {{0.0, 0.0, 0.0}},
		tail_width							= 0.2,
		smoke_color							= {1.0, 1.0, 1.0},
		smoke_transparency					= 0.9,
		custom_smoke_dissipation_factor		= 0.3,	
	},

	fm = {
		mass        = 73.6,  
		caliber     = 0.127,  
		cx_coeff    = {1,0.45,1.0,0.65,1.3},
		L           = 2.82,
		I           = 1 / 12 * 86 * 2.82 * 2.82,
		Ma          = 0.6,
		Mw          = 1.2,
		Sw			= 0.2,
		dCydA		= {0.07, 0.036},
		A			= 0.6,
		maxAoa		= 0.3,
		finsTau		= 0.02,
		freq		= 10,
	},
	
	simple_IR_seeker = {
		sensitivity		= 9500,
		cooled			= true,
		delay			= 0.0,
		GimbLim			= math.rad(130),
		FOV				= math.rad(7)*2,
		opTime			= 15.0,
		target_H_min	= 0.0,
		flag_dist		= 150.0,
	},
	
	simple_gyrostab_seeker = {
		omega_max	= math.rad(8)
	},
	
	fuze_proximity = {
		ignore_inp_armed	= 0,
		radius				= 0,
	},
	
	autopilot = {
		K				= 56.0,
		Kg				= 2.0,
		Ki				= 0.0,
		finsLimit		= 3.0,
		delay 			= 0.5,
		fin2_coeff		= 0.5,
	},
	
	
	warhead = predefined_warhead("MIM_72G"),
	warhead_air = predefined_warhead("MIM_72G")
}

declare_weapon(RIM_116A)
