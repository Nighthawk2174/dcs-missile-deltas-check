-- Land Rover 101 fc

GT = {};
set_recursive_metatable(GT, GT_t.generic_wheel_vehicle)
GT.chassis = {
	life = 2,
	mass = 1500,
	length = 4.29,
	width = 1.83,
	max_road_velocity = 34.7,
	fordingDepth = 0.6,
	max_slope = 0.47,
	engine_power = 90,
	max_vert_obstacle = 0.5,
	max_acceleration = 1.1,
	min_turn_radius = 7.15,
	X_gear_1 = 1.3,
	Y_gear_1 = 0,
	Z_gear_1 = 0.76,
	X_gear_2 = -1.27,
	Y_gear_2 = 0,
	Z_gear_2 = 0.76,
	gear_type = GT_t.GEAR_TYPES.WHEELS,
	r_max = 0.46,
	armour_thickness = 0.001,
}

GT.visual.shape = "land_rover_101_fc"
GT.visual.shape_dstr = "land_rover_101_fc_p_1"

GT.AddPropVehicle = {
			{ id = "Variant" , control = 'comboList', wCtrl = 150, label = _('Variant'), defValue = 4, arg = 60,
				values = {
				{ id = 1, dispName = _("Covered/Empty"), value = 0.0},
				{ id = 2, dispName = _("Covered/Loaded"), value = 0.1},
				{ id = 3, dispName = _("Open/Loaded"), value = 0.2},
				{ id = 4, dispName = _("Open/Empty"), value = 0.3},
				}
			}
		}


--chassis
GT.swing_on_run = false


--Burning after hit
GT.visual.fire_size = 0.7 --relative burning size
GT.visual.fire_pos[1] = 0 -- center of burn at long axis shift(meters)
GT.visual.fire_pos[2] = 0 -- center of burn shift at vertical shift(meters)
GT.visual.fire_pos[3] = 0 -- center of burn at transverse axis shift(meters)
GT.visual.fire_time = 1000 --burning time (seconds)

GT.driverViewConnectorName = {"DRIVER_POINT", offset = {0.3, 0.0, 0.0}}
GT.driverCockpit = "DriverCockpit/DriverCockpit"

GT.Name = "Land_Rover_101_FC"
GT.DisplayName = _("Land Rover 101 FC")
GT.Rate = 3

GT.DetectionRange  = 0;
GT.ThreatRange = 0;
GT.mapclasskey = "P0091000212";
GT.attribute = {wsType_Ground,wsType_Tank,wsType_NoWeapon,wsType_GenericVehicle,
				"Trucks",
				};
GT.category = "Unarmed";
GT.Countries = {"UK", "Iran", "Malaysia", "Oman", "Singapore", "Switzerland", "Turkey", "United Arab Emirates", "Zambia", "Australia", "Libya", "Netherlands", "Iraq"}