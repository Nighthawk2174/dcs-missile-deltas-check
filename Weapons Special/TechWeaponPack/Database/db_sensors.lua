declare_sensor({
		Name = "Mys-M1_SR",
		category = SENSOR_RADAR,
		type = RADAR_SS,
		vehicles_detection = true,
		airborne_radar = false,
		scan_volume = 
		{
			azimuth = {-180, 180},
			elevation = {-10, 10}
		},
		max_measuring_distance = 200000,
		detection_distance = 
		{
			[HEMISPHERE_UPPER] =
			{
				[ASPECT_HEAD_ON] = 200000.0,
				[ASPECT_TAIL_ON] = 200000.0
			},
			[HEMISPHERE_LOWER] =
			{
				[ASPECT_HEAD_ON] = 200000.0,
				[ASPECT_TAIL_ON] = 200000.0
			}
		},
		lock_on_distance_coeff = 1.0,
		velocity_limits = 
		{
			radial_velocity_min = 0,
		},
		scan_period = 10.0,
		RCS = 5.0,
		RBM_detection_distance = 200000.0
	})