--db.Units.Cars = {};
local plugin_db_path = current_mod_path..'/Database/'

--------------------------------------------------------------------------
-- Cars
--------------------------------------------------------------------------

local function chassis_file(f)
    if dofile(plugin_db_path..f) then
		error("can't load file "..f)
		return
	end
end

local function vehicle_file(f)
	if dofile(plugin_db_path..f) then
		error("can't load file "..f)
		return
	end
	if(GT) then
		GT.shape_table_data = 
		{
			{
				file  	    = GT.visual.shape;
				username    = GT.Name;
				desrt       = GT.visual.shape_dstr;
			    classname 	= "lLandVehicle";
				positioning = "BYNORMAL";
				life		= GT.life or 5;
			},
			{
				name  = GT.visual.shape_dstr;
				file  = GT.visual.shape_dstr;
			},
		}
		add_surface_unit(GT)
		GT = nil;
	else
		error("GT empty in file "..f)
	end;
end

--- BEGIN Vehicles
GT = nil;

--Russia
vehicle_file("vehicles/SAM/S75_Launcher.lua")
vehicle_file("vehicles/SAM/Radar/S75_TR.lua")

-- UK
vehicle_file("vehicles/SAM/Rapier_FSA_Launcher.lua")
vehicle_file("vehicles/SAM/Rapier_FSA_OpticalTracker.lua")
vehicle_file("vehicles/SAM/Rapier_FSA_BlindfireTracker.lua")
vehicle_file("vehicles/Unarmed/Land_Rover_101_FC.lua")
vehicle_file("vehicles/Unarmed/Land_Rover_109_S3.lua")

-- China
vehicle_file("vehicles/MissilesSS/HY-2_Launcher.lua")
vehicle_file("vehicles/MissilesSS/Mys-m1.lua")
--- END Vehicles

-- Railway Objects
vehicle_file("vehicles/RailwayObjects/ES44AH.lua")
vehicle_file("vehicles/RailwayObjects/boxcartrinity.lua")
vehicle_file("vehicles/RailwayObjects/tankcartrinity.lua")
vehicle_file("vehicles/RailwayObjects/wellcarnsc.lua")