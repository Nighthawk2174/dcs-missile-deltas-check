local function calcPiercingMass(warhead)
	warhead.piercing_mass  = warhead.mass;
	if (warhead.expl_mass/warhead.mass > 0.1) then
		warhead.piercing_mass  = warhead.mass/5.0;
	end
end

function HE_penetrating_warhead(power,caliber)
	local res = {};
	
	res.caliber = caliber;
    res.expl_mass = power;
	res.mass = res.expl_mass;
    res.other_factors = { 0.5, 0.5, 0.5 };
    res.obj_factors = {1, 1};
    res.concrete_factors = {1, 1, 1};
    res.concrete_obj_factor = 2.0;
	res.cumulative_factor = 0.0;    
    res.cumulative_thickness = 0.0;
    
    calcPiercingMass(res)
	return res;
end


local AGM_65F =
{
	category		= CAT_MISSILES,
	name			= "AGM_65F", -- AGM-65F
	user_name		= _("AGM-65F"),
	class_name = "wAmmunitionSelfHoming",
	scheme = "AGM-65",
	wsTypeOfWeapon 	= {wsType_Weapon,wsType_Missile,wsType_AS_Missile,WSTYPE_PLACEHOLDER},
	model			= "AGM-65F",
	
	mass			= 301,
	Escort = 0,
    Head_Type = 5,
	sigma = {5, 5, 5},
	M = 301.0,
	H_max = 10000.0,
	H_min = -1,
	Diam = 305.0,
	Cx_pil = 4,
	D_max = 12964.0,
	D_min = 500.0,
	Head_Form = 0,
	Head_Form = 0,
	Life_Time = 110,
	Nr_max = 16,
	v_min = 50.0,
	v_mid = 290.0,
	Mach_max = 1.5,
	t_b = 0.0,
	t_acc = 4.0,
	t_marsh = 0.0,
	Range_max = 24076.0,
	H_min_t = 0.0,
	Fi_start = 0.5,
	Fi_rak = 3.14152,
	Fi_excort = 1.05,
	Fi_search = 99.9,
	OmViz_max = 99.9,
	exhaust = {0.75, 0.75, 0.75, 0.1},
	X_back = -0.9,
	Y_back = -0.15,
	Z_back = 0.0,
	Reflection = 0.063,
	KillDistance = 0.0,

	shape_table_data =
	{
		{
			name		= "AGM_65F",
			file		= "AGM-65F",
			life		= 1,
			fire		= { 0, 1},
			username	= _("AGM-65F Maverick"),
			index		= WSTYPE_PLACEHOLDER,
		},
	},
	
	fm = {
		mass        = 301,  
		caliber     = 0.305,  
		cx_coeff    = {1,0.39,0.38,0.236,1.31},
		L           = 2.49,
		I           = 1 / 12 * 209 * 2.49 * 2.49,
		Ma          = 0.68,
		Mw          = 1.116,
		wind_sigma	= 0.0,
		wind_time	= 0.0,
		Sw			= 0.55,
		dCydA		= {0.07, 0.036},
		A			= 0.36,
		maxAoa		= 0.23,
		finsTau		= 0.1,
		Ma_x		= 3,
		Kw_x		= 0.03,
		I_x			= 40,
	},
	
	seeker = {
		delay			= 0.0,
		op_time			= 105,
		FOV				= math.rad(60),
		max_w_LOS		= 0.06,
		max_w_LOS_surf	= 0.03,
	
		max_target_speed			= 33,
		max_target_speed_rnd_coeff	= 10,
		
		ship_track_by_default		= 1,
		ship_track_board_vis_angle	= math.rad(60),
	},
	
	PN_autopilot = {
		K			= 0.02,
		Ki			= 0,
		Kg			= 2.5,
		Kx			= 0.02,
		fins_limit	= 0.3,
		K_GBias		= 0.28,
	},
	
	march = {
		smoke_color			= {0.8, 0.8, 0.8},
		smoke_transparency	= 0.7,
	},
	
	warhead = predefined_warhead("AGM_65G"),
	warhead_air = predefined_warhead("AGM_65G"),
}

declare_weapon(AGM_65F)
	
local LAU_117_mass = 59

local agm65_variants =
{
	["AGM-65F"]  		= {display_name = _("AGM-65F"), Picture	=	"agm65.png", ws_type = AGM_65F.wsTypeOfWeapon,	category = CAT_MISSILES, mass = 301.0},
}

local function lau_117_Maverick(clsid,element)

	local var 	   = agm65_variants[element] or agm65_variants["AGM-65F"]
	local var_mass = var.mass or 301.0
	
	local ret = {
		category			=	CAT_MISSILES,
		CLSID				=	clsid,
		Picture				=	var.Picture,
		wsTypeOfWeapon		=	var.ws_type,
		attribute     		=  {4,	4,	32,	WSTYPE_PLACEHOLDER},
		Count 			  	= 1,
		displayName	  		=	"LAU-117,"..element,
		Weight  		  	= LAU_117_mass + var.mass,
		Cx_pil 		  		= 0.0009765625,
		Elements		  = 
		{
			{
				Position	=	{0,	0,	0},
				ShapeName	=	"LAU-117",
			}, 
			{
				DrawArgs	=	
				{
					[1]	=	{1,	1},
					[2]	=	{2,	1},
				}, -- end of DrawArgs
				Position	=	{0.18,	-0.078,	0},
				ShapeName	=	"AGM-65F",
			}
		}
	}

	declare_loadout(ret)
end

lau_117_Maverick("LAU_117_AGM_65F"	,"AGM-65F")