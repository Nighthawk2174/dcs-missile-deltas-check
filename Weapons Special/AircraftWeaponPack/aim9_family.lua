
local GAR_8 =
{
	category		= CAT_AIR_TO_AIR,
	name			= "GAR-8", -- AIM-9B
	user_name		= _("GAR-8"),
	wsTypeOfWeapon 	= {wsType_Weapon,wsType_Missile,wsType_AA_Missile,WSTYPE_PLACEHOLDER},
	Escort 			= 0,
	Head_Type 		= 1,
	sigma 			= {3, 3, 3},
	M 				= 74.39,
	H_max 			= 18000.0,
	H_min 			= -1,
	Diam 			= 127.0,
	Cx_pil			= 2.58,
	D_max 			= 4000.0,
	D_min 			= 300.0,
	Head_Form 		= 0,
	Life_Time 		= 30.0,
	Nr_max 			= 10,
	v_min 			= 140.0,
	v_mid 			= 350.0,
	Mach_max 		= 2.2,
	t_b 			= 0.0,
	t_acc			= 2.2,
	t_marsh			= 0.0,
	Range_max		= 11000.0,
	H_min_t		    = 1.0,
	Fi_start 		= 0.3,
	Fi_rak 		    = 3.14152,
	Fi_excort 		= 0.79,
	Fi_search 		= 0.09,
	OmViz_max 		= 0.2,
	warhead 		= simple_aa_warhead(10),
	exhaust 		=  { 1, 1, 1, 1 },
	X_back 			= -1.55,
	Y_back 			= 0.0,
	Z_back			= 0.0,
	Reflection		= 0.0182,
	KillDistance 	= 5.0,
	ccm_k0 = 10.0,  -- Counter Countermeasures Probability Factor. Value = 0 - missile has absolutely resistance to countermeasures. Default = 1 (medium probability)
	--seeker sensivity params
	SeekerSensivityDistance = 4000, -- The range of target with IR value = 1. In meters. In forward hemisphere.
	SeekerCooled	 = false, -- True is cooled seeker and false is not cooled seeker.				
	shape_table_data =
	{
		{
			name	 = "GAR-8",
			file	 = "aim-9b",
			life	 = 1,
			fire	 = { 0, 1},
			username = "GAR-8",
			index = WSTYPE_PLACEHOLDER,
		},
	},
	ModelData = 
	{   58 ,  -- model params count
		0.35 ,   -- characteristic square (характеристическая площадь)
		
		-- параметры зависимости Сx
		0.04 , -- Cx_k0 планка Сx0 на дозвуке ( M << 1)
		0.08 , -- Cx_k1 высота пика волнового кризиса
		0.02 , -- Cx_k2 крутизна фронта на подходе к волновому кризису
		0.05, -- Cx_k3 планка Cx0 на сверхзвуке ( M >> 1)
		1.2 , -- Cx_k4 крутизна спада за волновым кризисом 
		1.2 , -- коэффициент отвала поляры (пропорционально sqrt (M^2-1))
		
		-- параметры зависимости Cy
		0.5 , -- Cy_k0 планка Сy0 на дозвуке ( M << 1)
		0.4	 , -- Cy_k1 планка Cy0 на сверхзвуке ( M >> 1)
		1.2  , -- Cy_k2 крутизна спада(фронта) за волновым кризисом  
		
		0.29 , -- 7 Alfa_max  максимальный балансировачный угол, радианы
		0.0, --угловая скорость создаваймая моментом газовых рулей
		
	-- Engine data. Time, fuel flow, thrust.	
	--	t_statr		t_b		t_accel		t_march		t_inertial		t_break		t_end			-- Stage
		-1.0,		-1.0,	2.2,  		0.0,		0.0,			0.0,		1.0e9,         -- time of stage, sec
		 0.0,		0.0,	8.45,		0.0,		0.0,			0.0,		0.0,           -- fuel flow rate in second, kg/sec(секундный расход массы топлива кг/сек)
		 0.0,		0.0,	17150.0,	0.0,	0.0,			0.0,		0.0,           -- thrust, newtons
	
		 25.0, -- таймер самоликвидации, сек
		 18.0, -- время работы энергосистемы, сек
		 0, -- абсолютная высота самоликвидации, м
		 2.0, -- время задержки включения управления (маневр отлета, безопасности), сек
		 1.0e9, -- дальность до цели в момент пуска, при превышении которой ракета выполняется маневр "горка", м
		 1.0e9, -- дальность до цели, при которой маневр "горка" завершается и ракета переходит на чистую пропорциональную навигацию (должен быть больше или равен предыдущему параметру), м 
		 0.0,  -- синус угла возвышения траектории набора горки
		 30.0, -- продольное ускорения взведения взрывателя
		 0.0, -- модуль скорости сообщаймый катапультным устройством, вышибным зарядом и тд
		 1.19, -- характристика системы САУ-РАКЕТА,  коэф фильтра второго порядка K0
		 1.0, -- характристика системы САУ-РАКЕТА,  коэф фильтра второго порядка K1
		 2.0, -- характристика системы САУ-РАКЕТА,  полоса пропускания контура управления
		 0.0,
		 0.0,
		 0.0,
		 0.0,
		 0.0,
		  -- DLZ. Данные для рассчета дальностей пуска (индикация на прицеле)
		 9000.0, -- дальность ракурс   180(навстречу) град,  Н=10000м, V=900км/ч, м
		 4000.0, -- дальность ракурс 0(в догон) град,  Н=10000м, V=900км/ч, м
		 4000.0, -- дальность ракурс 	180(навстречу) град, Н=1000м, V=900км/ч, м
		 0.2, -- Уменьшение разрешенной дальности пуска при отклонении вектора скорости носителя от линии визирования цели.
		 1.0, -- Вертикальная плоскость. Наклон кривой разрешенной дальности пуска в нижнюю полусферу. Уменьшение дальности при стрельбе вниз.
		 1.4, -- Вертикальная плоскость. Наклон кривой разрешенной дальности пуска в верхнюю полусферу. Увеличение дальности при стрельбе вверх.
		-3.0, -- Вертикальная плоскость. Угол перегиба кривой разрешенной дальности, верхняя - нижняя полусфера.
		 0.5, -- Изменение коэффициентов наклона кривой в верхнюю и нижнюю полусферы от высоты носителя.
	},
}

declare_weapon(GAR_8)

local AIM_9P5 =
{
	category		= CAT_AIR_TO_AIR,
	name			= "AIM-9P5", -- AIM-9P5
	user_name		= _("AIM-9P5"),
	wsTypeOfWeapon 	= {wsType_Weapon,wsType_Missile,wsType_AA_Missile,WSTYPE_PLACEHOLDER},
	Escort 			= 0,
	Head_Type 		= 1,
	sigma 			= {3, 3, 3},
	M 				= 85.5,
	H_max 			= 18000.0,
	H_min 			= -1,
	Diam 			= 127.0,
	Cx_pil			= 2.58,
	D_max 			= 4000.0,
	D_min 			= 300.0,
	Head_Form 		= 0,
	Life_Time 		= 30.0,
	Nr_max 			= 16,
	v_min 			= 140.0,
	v_mid 			= 350.0,
	Mach_max 		= 2.2,
	t_b 			= 0.0,
	t_acc			= 3.0,
	t_marsh			= 0.0,
	Range_max		= 11000.0,
	H_min_t		    = 1.0,
	Fi_start 		= 0.3,
	Fi_rak 		    = 3.14152,
	Fi_excort 		= 0.79,
	Fi_search 		= 0.09,
	OmViz_max 		= 0.61,
	warhead 		= simple_aa_warhead(11.0),
	exhaust 		=  { 0.7, 0.7, 0.7, 0.1 };
	X_back 			= -1.55,
	Y_back 			= 0.0,
	Z_back			= 0.0,
	Reflection		= 0.0182,
	KillDistance 	= 5.0,
	ccm_k0 = 0.5,  -- Counter Countermeasures Probability Factor. Value = 0 - missile has absolutely resistance to countermeasures. Default = 1 (medium probability)
	--seeker sensivity params
	SeekerSensivityDistance = 20000, -- The range of target with IR value = 1. In meters. In forward hemisphere.
	SeekerCooled	 		= true, -- True is cooled seeker and false is not cooled seeker.				
	shape_table_data =
	{
		{
			name	 = "aim-9p5",
			file	 = "aim-9p5",
			life	 = 1,
			fire	 = { 0, 1},
			username = "AIM-9P5",
			index 	 = WSTYPE_PLACEHOLDER,
		},
	},
	ModelData = 
	{
		58 ,  -- model params count
		0.35 ,   -- characteristic square (характеристическая площадь)
		
		-- параметры зависимости Сx
		0.04 , -- Cx_k0 планка Сx0 на дозвуке ( M << 1)
		0.08 , -- Cx_k1 высота пика волнового кризиса
		0.02 , -- Cx_k2 крутизна фронта на подходе к волновому кризису
		0.05, -- Cx_k3 планка Cx0 на сверхзвуке ( M >> 1)
		1.2 , -- Cx_k4 крутизна спада за волновым кризисом 
		1.2 , -- коэффициент отвала поляры (пропорционально sqrt (M^2-1))
		
		-- параметры зависимости Cy
		0.5 , -- Cy_k0 планка Сy0 на дозвуке ( M << 1)
		0.4	 , -- Cy_k1 планка Cy0 на сверхзвуке ( M >> 1)
		1.2  , -- Cy_k2 крутизна спада(фронта) за волновым кризисом  
		
		0.29 , -- 7 Alfa_max  максимальный балансировачный угол, радианы
		0.0, --угловая скорость создаваймая моментом газовых рулей
		
	-- Engine data. Time, fuel flow, thrust.	
	--	t_statr		t_b		t_accel		t_march		t_inertial		t_break		t_end			-- Stage
		-1.0,		-1.0,	3.0,  		0.0,		0.0,			0.0,		1.0e9,         -- time of stage, sec
		 0.0,		0.0,	8.0,		0.0,		0.0,			0.0,		0.0,           -- fuel flow rate in second, kg/sec(секундный расход массы топлива кг/сек)
		 0.0,		0.0,	18800.0,	0.0,	0.0,			0.0,		0.0,           -- thrust, newtons
	
		 1.0e9, -- таймер самоликвидации, сек
		 30.0, -- время работы энергосистемы, сек
		 0, -- абсолютная высота самоликвидации, м
		 0.5, -- время задержки включения управления (маневр отлета, безопасности), сек
		 1.0e9, -- дальность до цели в момент пуска, при превышении которой ракета выполняется маневр "горка", м
		 1.0e9, -- дальность до цели, при которой маневр "горка" завершается и ракета переходит на чистую пропорциональную навигацию (должен быть больше или равен предыдущему параметру), м 
		 0.0,  -- синус угла возвышения траектории набора горки
		 30.0, -- продольное ускорения взведения взрывателя
		 0.0, -- модуль скорости сообщаймый катапультным устройством, вышибным зарядом и тд
		 1.19, -- характристика системы САУ-РАКЕТА,  коэф фильтра второго порядка K0
		 1.0, -- характристика системы САУ-РАКЕТА,  коэф фильтра второго порядка K1
		 2.0, -- характристика системы САУ-РАКЕТА,  полоса пропускания контура управления
		 0.0,
		 0.0,
		 0.0,
		 0.0,
		 0.0,
		  -- DLZ. Данные для рассчета дальностей пуска (индикация на прицеле)
		 9000.0, -- дальность ракурс   180(навстречу) град,  Н=10000м, V=900км/ч, м
		 4000.0, -- дальность ракурс 0(в догон) град,  Н=10000м, V=900км/ч, м
		 4000.0, -- дальность ракурс 	180(навстречу) град, Н=1000м, V=900км/ч, м
		 0.2, -- Уменьшение разрешенной дальности пуска при отклонении вектора скорости носителя от линии визирования цели.
		 1.0, -- Вертикальная плоскость. Наклон кривой разрешенной дальности пуска в нижнюю полусферу. Уменьшение дальности при стрельбе вниз.
		 1.4, -- Вертикальная плоскость. Наклон кривой разрешенной дальности пуска в верхнюю полусферу. Увеличение дальности при стрельбе вверх.
		-3.0, -- Вертикальная плоскость. Угол перегиба кривой разрешенной дальности, верхняя - нижняя полусфера.
		 0.5, -- Изменение коэффициентов наклона кривой в верхнюю и нижнюю полусферы от высоты носителя.
	},
}

declare_weapon(AIM_9P5)

local AIM_9L =
{
	category		= CAT_AIR_TO_AIR,
	name			= "AIM-9L", -- AIM-9L
	user_name		= _("AIM-9L"),
	wsTypeOfWeapon 	= {wsType_Weapon,wsType_Missile,wsType_AA_Missile,WSTYPE_PLACEHOLDER},
	Escort = 0,
	Head_Type = 1,
	sigma = {3, 3, 3},
	M = 84.46,
	H_max = 18000.0,
	H_min = -1,
	Diam = 127.0,
	Cx_pil = 2.58,
	D_max = 7000.0,
	D_min = 300.0,
	Head_Form = 0,
	Life_Time = 60.0,
	Nr_max = 40,
	v_min = 140.0,
	v_mid = 350.0,
	Mach_max = 2.7,
	t_b = 0.0,
	t_acc = 5.0,
	t_marsh = 0.0,
	Range_max = 14000.0,
	H_min_t = 1.0,
	Fi_start = 0.3,
	Fi_rak = 3.14152,
	Fi_excort = 0.79,
	Fi_search = 0.09,
	OmViz_max = 0.61,
	warhead = simple_aa_warhead(11.0),
	exhaust = { 0.7, 0.7, 0.7, 1.0 },
	X_back = -1.66,
	Y_back = 0.0,
	Z_back = 0.0,
	Reflection = 0.0182,
	KillDistance = 7.0,
	--seeker sensivity params
	SeekerSensivityDistance = 20000, -- The range of target with IR value = 1. In meters. In forward hemisphere.
	ccm_k0 = 0.75,  -- Counter Countermeasures Probability Factor. Value = 0 - missile has absolutely resistance to countermeasures. Default = 1 (medium probability)
	SeekerCooled			= true, -- True is cooled seeker and false is not cooled seeker.				
	shape_table_data =
	{
		{
			name	 = "aim-9L",
			file	 = "aim-9L",
			life	 = 1,
			fire	 = { 0, 1},
			username = "AIM-9L",
			index 	 = WSTYPE_PLACEHOLDER,
		},
	},

	supersonic_A_coef_skew = 0.25, -- наклон прямой коэффициента отвала поляры на сверхзвуке
	nozzle_exit_area =	0.0068, -- площадь выходного сечения сопла
		
	ModelData = {   58,  -- model params count
					0.35,   -- characteristic square (характеристическая площадь)
	
					-- параметры зависимости Сx
					0.049, -- Cx_k0 планка Сx0 на дозвуке ( M << 1)
					0.082, -- Cx_k1 высота пика волнового кризиса
					0.010, -- Cx_k2 крутизна фронта на подходе к волновому кризису
					0.001, -- Cx_k3 планка Cx0 на сверхзвуке ( M >> 1)
					0.550, -- Cx_k4 крутизна спада за волновым кризисом 
					0.8, -- коэффициент отвала поляры (пропорционально sqrt (M^2-1))
						
					-- параметры зависимости Cy
					2.5, -- Cy_k0 планка Сy0 на дозвуке ( M << 1)
					0.8, -- Cy_k1 планка Cy0 на сверхзвуке ( M >> 1)
					1.2, -- Cy_k2 крутизна спада(фронта) за волновым кризисом  
						
					0.13, -- 7 Alfa_max  максимальный балансировачный угол, радианы
					0.00, --угловая скорость создаваймая моментом газовых рулей
						
				-- Engine data. Time, fuel flow, thrust.	
				--	t_statr		t_b		t_accel		t_march		t_inertial		t_break		t_end			-- Stage
					-1.0,	   -1.0,	5.20,  		0.0,		0.0,			0.0,		1.0e9,         -- time of stage, sec
					 0.0,		0.0,	5.27,		0.0,		0.0,			0.0,		0.0,           -- fuel flow rate in second, kg/sec(секундный расход массы топлива кг/сек)
					 0.0,		0.0,	11890.0,	0.0,		0.0,			0.0,		0.0,           -- thrust, newtons
					
					1.0e9, -- таймер самоликвидации, сек
					60.0, -- время работы энергосистемы, сек
					0, -- абсолютная высота самоликвидации, м
					0.3, -- время задержки включения управления (маневр отлета, безопасности), сек
					1.0e9, -- дальность до цели в момент пуска, при превышении которой ракета выполняется маневр "горка", м
					1.0e9, -- дальность до цели, при которой маневр "горка" завершается и ракета переходит на чистую пропорциональную навигацию (должен быть больше или равен предыдущему параметру), м 
					0.0,  -- синус угла возвышения траектории набора горки
					30.0, -- продольное ускорения взведения взрывателя
					0.0, -- модуль скорости сообщаймый катапультным устройством, вышибным зарядом и тд
					1.19, -- характристика системы САУ-РАКЕТА,  коэф фильтра второго порядка K0
					1.0, -- характристика системы САУ-РАКЕТА,  коэф фильтра второго порядка K1
					2.0, -- характристика системы САУ-РАКЕТА,  полоса пропускания контура управления
					0.0,
					0.0,
					0.0,
					0.0,
					0.0,
					-- DLZ. Данные для рассчета дальностей пуска (индикация на прицеле)
					26800.0, -- дальность ракурс   180(навстречу) град,  Н=10000м, V=900км/ч, м
					11400.0, -- дальность ракурс 0(в догон) град,  Н=10000м, V=900км/ч, м
					13300.0, -- дальность ракурс 	180(навстречу) град, Н=1000м, V=900км/ч, м
					0.2, -- Уменьшение разрешенной дальности пуска при отклонении вектора скорости носителя от линии визирования цели.
					1.0, -- Вертикальная плоскость. Наклон кривой разрешенной дальности пуска в нижнюю полусферу. Уменьшение дальности при стрельбе вниз.
					1.4, -- Вертикальная плоскость. Наклон кривой разрешенной дальности пуска в верхнюю полусферу. Увеличение дальности при стрельбе вверх.
					-3.0, -- Вертикальная плоскость. Угол перегиба кривой разрешенной дальности, верхняя - нижняя полусфера.
					0.5, -- Изменение коэффициентов наклона кривой в верхнюю и нижнюю полусферы от высоты носителя.
				},
	
	
} 
declare_weapon(AIM_9L)

local desc_sidwinder = " ".._("Sidewinder IR AAM")

local aim9_variants =
{
	["AIM-9"]  		= {CLSID = "{6CEB49FC-DED8-4DED-B053-E1F033FF72D3}", display_name = _("AIM-9M") ..desc_sidwinder	  	 			,wstype = {4,	4,	7	, AIM_9 },	category = CAT_AIR_TO_AIR, mass = 86.64	},
	["AIM-9P"] 		= {display_name = _("AIM-9P") ..desc_sidwinder	  	 			,wstype = {4,	4,	7	, AIM_9P},	category = CAT_AIR_TO_AIR, mass = 86.18	},
	["AIM-9X"] 		= {CLSID = "{5CE2FF2A-645A-4197-B48D-8720AC69394F}", display_name = _("AIM-9X") ..desc_sidwinder	  	 			,wstype = {4,	4,	7	, AIM_9X},	category = CAT_AIR_TO_AIR, mass = 84.46	},
	["CATM-9M"] 	= {CLSID = "CATM-9M", 								 display_name = _("CAP-9M")	  	 			,wstype = {4,	4,	100 ,	 143},	category = CAT_AIR_TO_AIR	},
	["ais-pod-t50"] = {display_name = _("AN/ASQ-T50 TCTS Pod")	 	,wstype = {4,	15,	47	,	 108},	category = CAT_PODS		,mass = 62.6},
	["GAR-8"]		= {display_name = GAR_8.user_name  ..desc_sidwinder, wstype = GAR_8.wsTypeOfWeapon,		category = CAT_AIR_TO_AIR	},
	["AIM-9P5"]		= {CLSID = "{AIM-9P5}",								 display_name = AIM_9P5.user_name  ..desc_sidwinder			,wstype = AIM_9P5.wsTypeOfWeapon,	category = CAT_AIR_TO_AIR	},
	["AIM-9L"]		= {CLSID = "{AIM-9L}", 								 display_name = AIM_9L.user_name ..desc_sidwinder 				,wstype = AIM_9L.wsTypeOfWeapon,	category = CAT_AIR_TO_AIR	},
}

local AIM9_DRAG  			  = AIM_9L.Cx_pil / 4096.0 -- 4096 - magic number from long time ago when flanker was 1.5 

local AIM9_DRAG_WITH_ADAPTER  = 0.001959765625 -- TODO verify this value 
local function aim_9_with_adapter(CLSID,aim_9_variant)
	local var 	   = aim9_variants[aim_9_variant] or aim9_variants["AIM-9"]
	local var_mass = var.mass or 85.5
	declare_loadout({
		category			= var.category,
		CLSID 				= CLSID,
		Picture				=	"aim9p.png",
		displayName			=	"LAU-7 "..var.display_name,
		wsTypeOfWeapon		=   var.wstype,
		attribute			=	{4,	4,	32,	111},
		Cx_pil				=	AIM9_DRAG_WITH_ADAPTER,
		Count				=	1,
		Weight				=	15 + var_mass,
		Elements			=	
		{
			{	ShapeName	=	"aero-3b"	   	  ,	IsAdapter  	   =   true  }, 
			{	ShapeName	=	aim_9_variant	  ,	connector_name =  "Point"},
		}-- end of Elements
	})
end


local function aim_9_without_adapter(CLSID,aim_9_variant)
	local var = aim9_variants[aim_9_variant] or aim9_variants["AIM-9"]
	local var_mass = var.mass or 85.5
	declare_loadout({
		category			= 	var.category,
		CLSID 				= 	CLSID,
		Picture				=	"aim9p.png",
		displayName			=	var.display_name,
		attribute			=	var.wstype,
		Cx_pil				=	AIM9_DRAG,
		Count				=	1,
		Weight				=	var_mass,
		Elements			=  {{ShapeName = aim_9_variant}}-- end of Elements
	})
end

local function lau_105_sidewinder(clsid,element,left,right)
	local var 	   = aim9_variants[element] or aim9_variants["AIM-9"]
	local var_mass = var.mass or 85.5
	local ret = {
		category			=	var.category,
		CLSID				=	clsid,
		Picture				=	"aim9m.png",
		wsTypeOfWeapon		=	var.wstype,
		attribute			=	{4,	4,	32,	50},
		Cx_pil				=	AIM9_DRAG_WITH_ADAPTER,
		Elements 			= {{ShapeName	=	"lau-105",IsAdapter  	   =   true  }}
	}
	if left then 	ret.Elements[#ret.Elements + 1] = {ShapeName	    =	 element,connector_name =	"Point_Pilon L" , Rotation		= 	{ 90,0,0}}	end --rotation because LAU-105 have bugged connector pos
	if right then	ret.Elements[#ret.Elements + 1] = {ShapeName		=	 element,connector_name =	"Point_Pilon R" , Rotation		= 	{ 90,0,0}}	end --rotation because LAU-105 have bugged connector pos
	
	local sz = #ret.Elements - 1
	ret.Count  = sz
	ret.Weight = 30 + var_mass *  sz

	if sz > 1 then
		ret.displayName =	_("LAU-105").." - 2 "..var.display_name
	else
		ret.displayName =	_("LAU-105").." "..var.display_name
	end
	declare_loadout(ret)
end

local function lau_115_2x127_sidewinder(clsid,element,left,right)
	local var 	   = aim9_variants[element] or aim9_variants["AIM-9P"]
	local var_mass = var.mass or 85.5
	
	local ret = {
		category			=	var.category,
		CLSID				=	clsid,
		Picture				=	"aim9m.png",
		wsTypeOfWeapon		=	var.wstype,
		attribute			=	{4,	4,	32,	146},
		Cx_pil				=	AIM9_DRAG_WITH_ADAPTER,
		Elements 			= {{ShapeName	=	"LAU-115C+2_LAU127",IsAdapter  	   =   true  }}
	}
	if left then 	ret.Elements[#ret.Elements + 1] = {ShapeName	    =	 element,connector_name =	"Point03" }	end --rotation because LAU-127 have bugged connector pos
	if right then	ret.Elements[#ret.Elements + 1] = {ShapeName		=	 element,connector_name =	"Point02" }	end --rotation because LAU-127 have bugged connector pos
	
	local sz = #ret.Elements - 1
	ret.Count  = sz
	ret.Weight = 54.4 + 45.3 * sz + var_mass * sz

	if sz > 1 then
		ret.displayName =	_("LAU-115").." - 2 "..var.display_name
	else
		ret.displayName =	_("LAU-115").." "..var.display_name
	end
	declare_loadout(ret)
end


local function lau_127_aim_9(clsid,element)
	local var 	   = aim9_variants[element] or aim9_variants["AIM-9P"]
	local var_mass = var.mass or 85.5
	
	local ret = {
		category			=	CAT_AIR_TO_AIR,
		CLSID				=	clsid,
		Picture				=	"aim9m.png",
		wsTypeOfWeapon		=	element.wsTypeOfWeapon,
		attribute			=	{4,	4,	32,	WSTYPE_PLACEHOLDER},
		Cx_pil				=	AIM9_DRAG_WITH_ADAPTER,
		Count				=	1,
		Weight_Empty		= 	45.3,
		Weight				=	45.3 + var_mass,
		Elements			=	
		{
			{	ShapeName	=	"LAU_127"	   	  ,	IsAdapter  	   =   true  }, 
--			{	ShapeName	= element.shape_table_data.name,	connector_name =  "Point01"},
			{	payload_CLSID =	 var.CLSID,	connector_name =  "Point01"}
		}-- end of Elements
	}
	ret.displayName =	_("LAU-127").." "..var.display_name
	declare_loadout(ret)
	
	return ret
end

local function lau_115_lau_127_aim_9(clsid,element)
	local ret = {
		category			=	CAT_AIR_TO_AIR,
		CLSID				=	clsid,
		Picture				=	"aim9m.png",
		wsTypeOfWeapon		=	element.attribute,
		attribute			=	{4,	4,	32,	WSTYPE_PLACEHOLDER},
		Cx_pil				=	AIM9_DRAG_WITH_ADAPTER,
		Count				=	1,
		Weight_Empty		= 	54.4 + 45.3,
		Weight				=	54.4 + element.Weight,
		Elements			=	
		{
			{	ShapeName	=	"LAU-115C"	   	  ,	IsAdapter  	   =   true  }, 
			{	payload_CLSID = element.CLSID, connector_name =  "Point01"}
		}-- end of Elements
	}
	ret.displayName =	_("LAU-115C").." "..element.displayName
	declare_loadout(ret)
end


local function f4e_sidewinder(clsid,element,left,right)

	local var	   = aim9_variants[element] or aim9_variants["AIM-9"]
	local var_mass = var.mass or 85.5
	local ret = {
		category			=	var.category,
		CLSID				=	clsid,
		Picture				=	"aim9m.png",
		wsTypeOfWeapon		=	var.wstype,
		attribute			=	{4,	4,	32,	61},
		Cx_pil				=	AIM9_DRAG_WITH_ADAPTER,
		Elements 			= {{ShapeName	=	"F4-PILON",IsAdapter  	   =   true  }}
	}
	if left then 	ret.Elements[#ret.Elements + 1] = {ShapeName	    =	 element,Rotation		= 	{-90,0,0},Position	= {0.465,	0.00,	-0.28}}	end
	if right then	ret.Elements[#ret.Elements + 1] = {ShapeName		=	 element,Rotation		= 	{ 90,0,0},Position	= {0.465,	0.00,	 0.28}}	end
	
	local sz = #ret.Elements - 1
	ret.Count  = sz
	ret.Weight = 30 + var_mass *  sz
	
	if sz > 1 then
		ret.displayName =	_("LAU-7").." - 2 "..var.display_name
	else
		ret.displayName =	_("LAU-7").." "..var.display_name
	end
	declare_loadout(ret)
end

---------------------------------------------------------------------------------
aim_9_without_adapter("{AIM-9B}"							  ,"GAR-8")
aim_9_without_adapter("{9BFD8C90-F7AE-4e90-833B-BFD0CED0E536}","AIM-9P")
aim_9_without_adapter("{AIM-9P5}"							  ,"AIM-9P5")
aim_9_without_adapter("{6CEB49FC-DED8-4DED-B053-E1F033FF72D3}","AIM-9")
aim_9_without_adapter("{5CE2FF2A-645A-4197-B48D-8720AC69394F}","AIM-9X")
aim_9_without_adapter("CATM-9M"								  ,"CATM-9M")
aim_9_without_adapter("{AIS_ASQ_T50}"						  ,"ais-pod-t50")
aim_9_without_adapter("{AIM-9L}"						  	  ,"AIM-9L")
---------------------------------------------------------------------------------
aim_9_with_adapter	 ("{GAR-8}"								  ,"GAR-8")
aim_9_with_adapter	 ("{AIM-9P-ON-ADAPTER}"					  ,"AIM-9P")
aim_9_with_adapter	 ("{AIM-9P5-ON-ADAPTER}"				  ,"AIM-9P5")
aim_9_with_adapter	 ("{AIM-9M-ON-ADAPTER}"					  ,"AIM-9")
aim_9_with_adapter	 ("{AIM-9X-ON-ADAPTER}"					  ,"AIM-9X")
---------------------------------------------------------------------------------
lau_105_sidewinder("{DB434044-F5D0-4F1F-9BA9-B73027E18DD3}","AIM-9",true ,true)
lau_105_sidewinder("LAU-105_1*AIM-9M_L"					   ,"AIM-9",true ,false)
lau_105_sidewinder("LAU-105_1*AIM-9M_R"					   ,"AIM-9"	,false,true)
lau_105_sidewinder("{3C0745ED-8B0B-42eb-B907-5BD5C1717447}","AIM-9P",true ,true)
lau_105_sidewinder("LAU-105_2*AIM-9P5"					   ,"AIM-9P5",true ,true)
lau_105_sidewinder("LAU-105_2*AIM-9L"					   ,"AIM-9L",true ,true)
lau_105_sidewinder("LAU-105_1*AIM-9L_L"					   ,"AIM-9L",true ,false)
lau_105_sidewinder("LAU-105_1*AIM-9L_R"					   ,"AIM-9L",false,true)
---------------------------------------------------------------------------------
lau_105_sidewinder("LAU-105_2*CATM-9M"					   ,"CATM-9M",true,true)
lau_105_sidewinder("LAU-105_1*CATM-9M_L"				   ,"CATM-9M",true,false)
lau_105_sidewinder("LAU-105_1*CATM-9M_R"				   ,"CATM-9M",false,true)
lau_105_sidewinder("LAU-105_AIS_ASQ_T50_L"				   ,"ais-pod-t50",true,false)
lau_105_sidewinder("LAU-105_AIS_ASQ_T50_R"				   ,"ais-pod-t50",false,true)
---------------------------------------------------------------------------------
f4e_sidewinder("{F4-2-AIM9B}"						   ,"GAR-8"  ,true,true)
f4e_sidewinder("{773675AB-7C29-422f-AFD8-32844A7B7F17}","AIM-9P" ,true,true)
f4e_sidewinder("{F4-2-AIM9P5}"						   ,"AIM-9P5",true,true)
f4e_sidewinder("{9DDF5297-94B9-42FC-A45E-6E316121CD85}","AIM-9"  ,true,true)
f4e_sidewinder("{F4-2-AIM9L}"						   ,"AIM-9L"  ,true,true)
f4e_sidewinder("{LAU-7_AIS_ASQ_T50}"  			   	   ,"ais-pod-t50",true,false)
---------------------------------------------------------------------------------

lau_115_2x127_sidewinder("LAU-115_2*LAU-127_AIM-9M"	,"AIM-9",true,true)
lau_115_2x127_sidewinder("LAU-115_2*LAU-127_CATM-9M","CATM-9M",true,true)
lau_115_2x127_sidewinder("LAU-115_2*LAU-127_AIM-9L"	,"AIM-9L",true,true)
lau_115_2x127_sidewinder("LAU-115_2*LAU-127_AIM-9X"	,"AIM-9X",true,true)


lau_127_9X = lau_127_aim_9("LAU-127_AIM-9X"	,"AIM-9X")
lau_115_lau_127_aim_9("LAU-115_LAU-127_AIM-9X", lau_127_9X)

lau_127_9M = lau_127_aim_9("LAU-127_AIM-9M"	,"AIM-9")
lau_115_lau_127_aim_9("LAU-115_LAU-127_AIM-9M", lau_127_9M)

lau_127_9L = lau_127_aim_9("LAU-127_AIM-9L"	,"AIM-9L")
lau_115_lau_127_aim_9("LAU-115_LAU-127_AIM-9L", lau_127_9L)

lau_127_CATM9 = lau_127_aim_9("LAU-127_CATM-9M"	,"CATM-9M")
lau_115_lau_127_aim_9("LAU-115_LAU-127_CATM-9M", lau_127_CATM9)