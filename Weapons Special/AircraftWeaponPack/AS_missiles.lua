
--					Kh-25 loadouts					--

declare_loadout({
	CLSID	 		= "{X-25ML}",
	category 		= CAT_MISSILES,
	wsTypeOfWeapon	= {4,	4,	8,	45},
	attribute		= {4,	4,	32,	99},
	Count 			= 1,
	Cx_pil			= 0.001,
	Picture			= "kh25ml.png",
	displayName		= _("Kh-25ML"),
	Weight			= 360,
	Elements  		= {{	ShapeName	= "X-25ML",
							Position	= {0, -0.2, 0},	}},
})

declare_loadout({
	CLSID	 		= "{X-25MR}",
	category 		= CAT_MISSILES,
	wsTypeOfWeapon	= {4,	4,	8,	74},
	attribute		= {4,	4,	32,	170},
	Count 			= 1,
	Cx_pil			= 0.001,
	Picture			= "kh25mr.png",
	displayName		= _("Kh-25MR"),
	Weight			= 360,
	Elements  		= {{	ShapeName	= "X-25MR",
							Position	= {0, -0.2, 0},	}},
})

declare_loadout({
	CLSID	 		= "{X-25MPU}",
	category 		= CAT_MISSILES,
	wsTypeOfWeapon	= {4,	4,	8,	47},
	attribute		= {4,	4,	32,	100},
	Count 			= 1,
	Cx_pil			= 0.001,
	Picture			= "kh25mpu.png",
	displayName		= _("Kh-25MPU"),
	Weight			= 370,
	Elements  		= {{	ShapeName	= "X-25ML",
							Position	= {0, -0.2, 0},	}},
})


--					Kh-29 loadouts					--

declare_loadout({
	CLSID	 		= "{X-29T}",
	category 		= CAT_MISSILES,
	wsTypeOfWeapon	= {4,	4,	8,	75},
	attribute		= {4,	4,	32,	92},
	Count 			= 1,
	Cx_pil			= 0.001,
	ejectImpulse    = 2000,
	Picture			= "kh29T.png",
	displayName		= _("Kh-29T"),
	Weight			= 760,
	Elements  		=	{{
							ShapeName	= "X-29T",
							Position	= {-0.482, 0, 0},
						}},
})

declare_loadout({
	CLSID	 		= "{X-29L}",
	category 		= CAT_MISSILES,
	wsTypeOfWeapon	= {4,	4,	8,	49},
	attribute		= {4,	4,	32,	93},
	Count 			= 1,
	Cx_pil			= 0.001,
	ejectImpulse    = 2000,
	Picture			= "kh29L.png",
	displayName		= _("Kh-29L"),
	Weight			= 747,
	Elements  		= {{	ShapeName	= "X-29L",
							Position	= {-0.482, 0, 0},	}},
})


--					Kh-29 loadouts					--

declare_loadout({
	CLSID	 		= "{X-31A}",
	category 		= CAT_MISSILES,
	wsTypeOfWeapon	= {4,	4,	8,	53},
	attribute		= {4,	4,	32,	96},
	Count 			= 1,
	Cx_pil			= 0.001,
	ejectImpulse    = 2000,
	Picture			= "kh31a.png",
	displayName		= _("Kh-31A"),
	NatoName		= "(AS-17)",
	Weight			= 690,
	Elements  		= {{	ShapeName	= "X-31",
							Position	= {0.245, 0, 0},	}},
})

declare_loadout({
	CLSID	 		= "{X-31P}",
	category 		= CAT_MISSILES,
	wsTypeOfWeapon	= {4,	4,	8,	76},
	attribute		= {4,	4,	32,	97},
	Count 			= 1,
	Cx_pil			= 0.001,
	ejectImpulse    = 2000,
	Picture			= "kh31p.png",
	displayName		= _("Kh-31P"),
	NatoName		= "(AS-17)",
	Weight			= 690,
	Elements  		= {{	ShapeName	= "X-31",
							Position	= {0.245, 0, 0},	}},
})


--