MISTRAL_MBDA =
{
	category		= CAT_MISSILES,
	name			= "Mistral",
	user_name		= _("Mistral"),
	scheme			= "self_homing_spin_missile",
	class_name		= "wAmmunitionSelfHoming",
	model			= "mim-72",--mbda_mistral
	mass			= 18.7,
	
	wsTypeOfWeapon 	= {wsType_Weapon,wsType_Missile,wsType_AA_Missile,WSTYPE_PLACEHOLDER},

	Escort			= 0,
	Head_Type		= 1,
	sigma			= {10, 10, 10},
	M				= 18.7,
	H_max			= 3000.0,
	H_min			= -1,
	Diam			= 93.0,
	Cx_pil			= 1,
	D_max			= 6000.0,
	D_min			= 200.0,
	Head_Form		= 1,
	Life_Time		= 15,
	Nr_max			= 18,
	v_min			= 140.0,
	v_mid			= 450.0,
	Mach_max		= 2.5,
	t_b				= 0.0,
	t_acc			= 3.0,
	t_marsh			= 0.0,
	Range_max		= 6000.0,
	H_min_t			= 10.0,
	Fi_start		= math.rad(1),
	Fi_rak			= 3.14152,
	Fi_excort		= 0.7,
	Fi_search		= 99.9,
	OmViz_max		= 99.9,
	X_back			= 0,
	Y_back			= 0,
	Z_back			= 0,
	Reflection		= 0.0182,
	KillDistance	= 5.0,
	SeekerSensivityDistance = 10000, 
	ccm_k0					= 0.5,
	SeekerCooled			= true,	
	
	shape_table_data =
	{
		{
			name		= "Mistral",
			file		= "mbda_mistral",
			life		= 1,
			fire		= { 0, 1},
			username	= _("Mistral"),
			index		= WSTYPE_PLACEHOLDER,
		},
	},
	
	controller = {
		boost_start = 0.001,
		march_start = 0.20,
	},
	
	booster = {
		impulse								= 170,
		fuel_mass							= 0.42,
		work_time							= 0.048,
		boost_time							= 0,
		boost_factor						= 0,
		nozzle_position						= {{-0.8, 0.0, 0}},
		nozzle_orientationXYZ				= {{0.0, 0.0, 0.0}},
		tail_width							= 0.38,
		smoke_color							= {1.0, 1.0, 1.0},
		smoke_transparency					= 0.9,
		custom_smoke_dissipation_factor		= 0.3,				
	},
		
	march = {
		impulse								= 200,
		fuel_mass							= 6,
		work_time							= 3,
		boost_time							= 0,
		boost_factor						= 0,
		nozzle_position						= {{-0.8, 0.0, 0}},
		nozzle_orientationXYZ				= {{0.0, 0.0, 0.0}},
		tail_width							= 0.2,
		smoke_color							= {1.0, 1.0, 1.0},
		smoke_transparency					= 0.9,
		custom_smoke_dissipation_factor		= 0.3,	
	},

	fm = {
		mass        = 18.7,  
		caliber     = 0.093,  
		cx_coeff    = {1,0.45,1.1,0.65,1.2},
		L           = 1.86,
		I           = 1 / 12 * 18.7 * 1.86 * 1.86,
		Ma          = 0.6,
		Mw          = 1.2,
		Sw			= 0.2,
		dCydA		= {0.07, 0.036},
		A			= 0.6,
		maxAoa		= 0.3,
		finsTau		= 0.1,
		freq		= 20,
	},
	
	simple_IR_seeker = {
		sensitivity		= 9500,
		cooled			= true,
		delay			= 0.0,
		GimbLim			= math.rad(30),
		FOV				= math.rad(7)*2;
		opTime			= 15.0,
		target_H_min	= 0.0,
		flag_dist		= 150.0,
	},
	
	simple_gyrostab_seeker = {
		omega_max	= math.rad(8)
	},
	
	fuze_proximity = {
		ignore_inp_armed	= 0,
		radius				= 0,
	},
	
	autopilot = {
		K				= 2.0,
		Kg				= 0.1,
		Ki				= 0.0,
		finsLimit		= 0.2,
		delay 			= 0.5,
		fin2_coeff		= 0.5,
	},
	
	
	warhead = predefined_warhead("Mistral_MBDA"),
	warhead_air = predefined_warhead("Mistral_MBDA")
}

declare_weapon(MISTRAL_MBDA)
