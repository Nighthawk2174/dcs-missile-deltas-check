local function KAB(main, fm, autopilot, tgdata)
	local t = main
	
	t.category  = CAT_BOMBS
	t.wsTypeOfWeapon = {wsType_Weapon, wsType_Bomb, wsType_Bomb_Guided, WSTYPE_PLACEHOLDER}
    t.hMin    = 1000.0
    t.VyHold  = -80.0
    t.Ag      = -1.0
	
	t.fm				= fm
	
	t.fm.mass			= t.mass
	t.fm.wind_time		= 1000.000000
	t.fm.wind_sigma		= 0
	t.fm.A				= 0.6
	t.fm.maxAoa			= math.rad(7)
	t.fm.finsTau		= 0.1
	t.fm.dCydA			= {0.066, 0.036}
	
	t.bang_bang_autopilot = autopilot
	t.targeting_data = tgdata

	t.shape_table_data ={
		{
			name     = t.name,
			file     = t.model,
			life     = 1,
			fire     = {0, 1},
			username = t.user_name,
			index    = WSTYPE_PLACEHOLDER,
		},
	}
	
	declare_weapon(t)
	return t
end



KAB_1500T = KAB({
	--main--
	name			= "KAB_1500T",
	type			= 3,
	model			= "kab-1500t",
	user_name		= _("KAB-1500Kr"),
	mass			= 1525,
	hMax    		= 15000.0,
    Cx      		= 0.00436,
	scheme			= "bomb_guided_pn",
	class_name		= "wAmmunitionSelfHoming",
   	warhead			= predefined_warhead("KAB_1500F"),
},
{	--fm--
	caliber			= 0.58,
	cx_coeff        = {1.000000, 0.400000, 0.370000, 0.288000, 1.310000},
	L               = 4.63,
	Sw				= 3.4,
	dCydA			= {0.066, 0.036}
},
{	--bang_bang_autopilot--
	Kg			= 0.25,
	Ki			= 0.0,
	finsLimit	= 0.35,
	delay		= 2.0
},
{	--targeting_data--
	char_time = 20.46
})

declare_loadout({
	category 		= CAT_BOMBS,
	CLSID	 		= "{KAB_1500Kr_LOADOUT}",
	attribute		= KAB_1500T.wsTypeOfWeapon,
	Count 			= 1,
	Cx_pil			= KAB_1500T.Cx,
	Picture			= "kab1500kr.png",
	displayName		= KAB_1500T.user_name,
	Weight			= KAB_1500T.mass,
	Elements  		= {{ShapeName = "kab-1500t"}},
})



KAB_1500LG = KAB({
	name			= "KAB_1500LG",
	type			= 4,
	model			= "kab-1500lg",
	user_name		= _("KAB-1500LG-Pr"),
	mass			= 1525,
	hMax    		= 15000.0,
    Cx      		= 0.00436,
	scheme			= "bomb-paveway-III-afm",
	class_name		= "wAmmunitionLaserHoming",
   	warhead			= predefined_warhead("KAB_1500Kr"),
},
{	--fm--
	caliber			= 0.58,
	cx_coeff        = {1.000000, 0.400000, 0.370000, 0.288000, 1.310000},
	L               = 4.28,
	Sw				= 3.2
},
{	--bang_bang_autopilot-- pn --(bomb-paveway-III-afm scheme)
	Kg			= 0.25,
	Ki			= 0.0,
	finsLimit	= 0.35,
	delay		= 2.0
},
{	--targeting_data--
	char_time = 20.25
})

declare_loadout({
	category 		= CAT_BOMBS,
	CLSID	 		= "{KAB_1500LG_LOADOUT}",
	attribute		= KAB_1500LG.wsTypeOfWeapon,
	Count 			= 1,
	Cx_pil			= KAB_1500LG.Cx,
	Picture			= "kab1500lg.png",
	displayName		= KAB_1500LG.user_name,
	Weight			= KAB_1500LG.mass,
	Elements  		= {{ShapeName = "kab-1500lg"}},
})


KAB_500S =
{
	category		= CAT_BOMBS,
	wsTypeOfWeapon	= {wsType_Weapon, wsType_Bomb, wsType_Bomb_Guided, WSTYPE_PLACEHOLDER},
	type			= 6,
	mass			= 500.0,
	hMin			= 1000.0,
	hMax			= 10000.0,
	Cx				= 0.00133,
	VyHold			= -80.0,
	Ag				= -1.0,
	
	name			= "KAB_500S",
	model			= "kab-500s",
	user_name		= _("KAB-500S"),
	scheme			= "bomb_sat_guided",
	class_name		= "wAmmunitionChangeableTrajectory",
	
	warhead = predefined_warhead("KAB_500S"),
	
	fm =
    {
        mass			= 500,
		wind_time		= 1000.0,
		wind_sigma		= 0,
		A				= 0.6,
		maxAoa			= math.rad(7),
		finsTau			= 0.1,
		dCydA			= {0.066, 0.036},
		caliber			= 0.4,
		cx_coeff        = {1.0, 0.39, 0.6, 0.168, 1.31},
		L               = 3.00,
		Sw				= 0.8,
		Ma_x			= 0.5,
		Ma_z			= 0.5,
		Kw_x			= 0.03,
    },  
	
	pos_conv = {
		char_time				= 20.22,
		LOS_conv_forward_mult	= 10.0,
		straight_nav_dist		= 1200,
		straight_nav_pitch		= -1.32,
		max_dist_calc			= 4000,
	},
	
	autopilot = {
		delay				= 2.0,
		op_time				= 9000,
		NR					= 10.0,
		Ac_limit 			= 8.0,	
		K					= 0.06,
		Kg					= 5.6,
		Ki					= 0.0,
		Kx					= 0.05,
		Kdx					= 0.01,
		fins_limit			= 0.3,
		PN_dist_data 		= {	4000,	0.0,
								2500,	1.0	},
		rotated_WLOS_input	= 1,
		conv_input			= 1,
		side_3deg_table		= {},
		side_6deg_table		= {},
		use_w_diff			= 1,	
	},
	
	shape_table_data =
	{
		{
			name     = "KAB_500S",
			file     = "kab-500s",
			life     = 1,
			fire     = {0, 1},
			username = _("KAB-500S"),
			index    = WSTYPE_PLACEHOLDER,
		},
	},
	
	targeting_data = 
	{
		char_time				= 20.5,
	}
}

declare_weapon(KAB_500S)

declare_loadout({
	category 		= CAT_BOMBS,
	CLSID	 		= "{KAB_500S_LOADOUT}",
	attribute		= KAB_500S.wsTypeOfWeapon,
	Count 			= 1,
	Cx_pil			= KAB_500S.Cx,
	Picture			= "kab500s.png",
	displayName		= KAB_500S.user_name,
	Weight			= KAB_500S.mass,
	Elements  		= {{ShapeName = "kab-500s"}},
})
