
local function Mk_bomb(main, warhead, picture, clsid)
	local t = main
	
	t.category			= CAT_BOMBS
	t.wsTypeOfWeapon	= {wsType_Weapon, wsType_Bomb, wsType_Bomb_A, WSTYPE_PLACEHOLDER}
	t.type				= 0
	t.hMin				= 100.0
	t.hMax				= 10000.0
	t.VyHold			= -100.0
	t.Ag      			= -1.23
	
	t.shape_table_data =
	{
		{
			name     = t.name,
			file     = t.model,
			life     = 1,
			fire     = {0, 1},
			username = t.user_name,
			index    = WSTYPE_PLACEHOLDER,
		},
	}
	
	t.warhead = warhead
	
	return t
end

local function Mk_82(main, warhead, picture, clsid)
	
	local t = Mk_bomb(main, warhead, picture, clsid)
	
	t.mass				= 232.0
	t.Cx				= 0.00025
	t.scheme			= "bomb-common"
	t.class_name		= "wAmmunition"

	t.fm = 
	{
		mass            = 232.0,
		caliber         = 0.273,
		cx_coeff        = {1.0, 0.29, 0.71, 0.14, 1.28},
		L               = 2.21,
		I               = 94.425933,
		Ma              = 2.746331,
		Mw              = 2.146083,
		wind_time       = 1000.0,
		wind_sigma      = 80.0
	}

	t.targeting_data = 
	{
		char_time = 20.32
	}
	
	declare_weapon(t)
	
	declare_loadout({
		category 		= CAT_BOMBS,
		CLSID	 		= clsid,
		attribute		= t.wsTypeOfWeapon,
		Count 			= 1,
		Cx_pil			= t.Cx,
		Picture			= picture,
		displayName		= t.user_name,
		Weight			= t.mass,
		Elements  		= {{ShapeName = t.model}},
	})

	return t
end

local function Mk_83(main, warhead, picture, clsid)
	
	local t = Mk_bomb(main, warhead, picture, clsid)
	
	t.mass				= 454.0
	t.Cx				= 0.00035
	t.scheme			= "bomb-common"
	t.class_name		= "wAmmunition"

	t.fm = 
	{
		mass		= 454.0,
		caliber		= 0.356,
		cx_coeff	= {1.0, 0.29, 0.71, 0.13, 1.28},
		L			= 3.0,
		I			= 340.5,
		Ma			= 2.746331,
		Mw			= 2.146083,
		wind_time	= 1000.0,
		wind_sigma	= 150.0,
	}
	
	t.targeting_data = 
	{
		char_time = 20.32
	}
	
	declare_weapon(t)
	
	declare_loadout({
		category 		= CAT_BOMBS,
		CLSID	 		= clsid,
		attribute		= t.wsTypeOfWeapon,
		Count 			= 1,
		Cx_pil			= t.Cx,
		Picture			= picture,
		displayName		= t.user_name,
		Weight			= t.mass,
		Elements  		= {{ShapeName = t.model}},
	})

	return t
end

local function Mk_84(main, warhead, picture, clsid)
	
	local t = Mk_bomb(main, warhead, picture, clsid)
	
	t.mass				= 908.0
	t.Cx				= 0.00056
	t.scheme			= "bomb-common"
	t.class_name		= "wAmmunition"

	t.fm = 
	{
		mass		= 908.0,
		caliber		= 0.457,
		cx_coeff	= {1.0, 0.39, 0.6, 0.168, 1.31},
		L			= 2.5,
		I			= 864.446267,
		Ma			= 2.746331,
		Mw			= 2.146083,
		wind_time	= 1000.0,
		wind_sigma	= 220.0,
	}
	
	t.targeting_data = 
	{
		char_time = 20.32
	}
	
	declare_weapon(t)
	
	declare_loadout({
		category 		= CAT_BOMBS,
		CLSID	 		= clsid,
		attribute		= t.wsTypeOfWeapon,
		Count 			= 1,
		Cx_pil			= t.Cx,
		Picture			= picture,
		displayName		= t.user_name,
		Weight			= t.mass,
		Elements  		= {{ShapeName = t.model}},
	})

	return t
end

local function Mk_Air(main, warhead, picture, clsid)

	local t = Mk_bomb(main, warhead, picture, clsid)
	
	t.scheme			= "bomb-parashute"
	t.class_name		= "wAmmunitionBallute"

	declare_weapon(t)
	
	declare_loadout({
		category 		= CAT_BOMBS,
		CLSID	 		= clsid,
		attribute		= t.wsTypeOfWeapon,
		Count 			= 1,
		Cx_pil			= t.Cx,
		Picture			= picture,
		displayName		= t.user_name,
		Weight			= t.mass,
		Elements  		= {{ShapeName = t.model}},
	})
	return t
end

Mk_82({name = "Mk_82B",		model = "mk-82", user_name= _("Mk-82B")},	predefined_warhead("BDU"),		"mk82.png", "{Mk_82B}")
Mk_82({name = "Mk_82YT",	model = "mk-82", user_name= _("Mk-82YT")},	predefined_warhead("BDU"),		"mk82.png", "{Mk_82YT}")
Mk_82({name = "Mk_82BT",	model = "mk-82", user_name= _("Mk-82BT")},	predefined_warhead("Mk_82"),	"mk82.png", "{Mk_82BT}")
Mk_82({name = "Mk_82P",		model = "mk-82", user_name= _("Mk-82P")},	predefined_warhead("Mk_82P"),	"mk82.png", "{Mk_82P}")
Mk_82({name = "Mk_82PT",	model = "mk-82", user_name= _("Mk-82PT")},	predefined_warhead("Mk_82P"),	"mk82.png", "{Mk_82PT}")
Mk_82({name = "Mk_82SB",	model = "mk-82", user_name= _("Mk-82SB")},	predefined_warhead("Mk_82"),	"mk82.png", "{Mk_82SB}")
Mk_82({name = "Mk_82SP",	model = "mk-82", user_name= _("Mk-82SP")},	predefined_warhead("Mk_82"),	"mk82.png", "{Mk_82SP}")

Mk_83({name = "Mk_82BT",	model = "mk-83", user_name= _("Mk-83BT")},	predefined_warhead("Mk_83"),	"mk83.png", "{Mk_83BT}")
Mk_83({name = "Mk_82P",		model = "mk-83", user_name= _("Mk-83P")},	predefined_warhead("Mk_83P"),	"mk83.png", "{Mk_83P}")
Mk_83({name = "Mk_82PT",	model = "mk-83", user_name= _("Mk-83PT")},	predefined_warhead("Mk_83P"),	"mk83.png", "{Mk_83PT}")

Mk_84({name = "Mk_84T",		model = "MK_84T",	user_name= _("Mk-84T")},	predefined_warhead("Mk_84"),	"mk84.png", "{Mk_84T}")
Mk_84({name = "Mk_84P",		model = "MK_84P",	user_name= _("Mk-84P")},	predefined_warhead("Mk_84P"),	"mk84.png", "{Mk_84P}")

MK_82Y = Mk_Air({
	name = "Mk_82Y",
	model = "mk-82air",
	user_name= _("Mk-82Y"),
	
	mass	= 232,
	Cx	= 0.00035,
	
	fm = {
		mass		= 232.0,
		caliber		= 0.273,
		cx_coeff	= {1.0, 0.29, 0.71, 0.14, 1.28},
		L			= 2.21,
		I			= 94.42593,
		cx_factor	= 100,
		wind_time	= 1000,
		wind_sigma	= 80,
	},
	
	control =	{open_delay = 0.2,},
	targeting_data =
	{	
		v0 = 200,
		data = 
		{
			{1,  20.579122, 0.004739},
			{10,  27.600642, 0.002003},
			{20,  29.328940, -0.004424},
			{30,  30.059589, -0.006725},
			{40,  30.520328, -0.004943},
			{50,  30.912878, -0.004930},
			{60,  31.221442, -0.005446},
			{70,  31.422306, -0.005585},
			{80,  31.573219, -0.005794},
			{90,  31.708002, -0.004384},
			{100,  31.839802, -0.005046},
			{200,  32.427146, -0.005338},
			{300,  32.750157, -0.004803},
			{400,  32.922025, -0.004677},
			{500,  33.050001, -0.004728},
			{600,  33.153851, -0.004508},
			{700,  33.229023, -0.004472},
			{800,  33.290361, -0.004441},
			{900,  33.343554, -0.004351},
			{1000,  33.384372, -0.004319},
			{1100,  33.422482, -0.004256},
			{1200,  33.453501, -0.004197},
			{1300,  33.481140, -0.004152},
			{1400,  33.505796, -0.004116},
			{1500,  33.525683, -0.004096},
			{1600,  33.544858, -0.004067},
			{1700,  33.561442, -0.004040},
			{1800,  33.576009, -0.004017},
			{1900,  33.588945, -0.003996},
			{2000,  33.600958, -0.003986},
			{3000,  33.679993, -0.003917},
			{4000,  33.727701, -0.003948},
			{5000,  33.763968, -0.004037},
			{6000,  33.792891, -0.004179},
			{7000,  33.815086, -0.004374},
			{8000,  33.830301, -0.004632},
			{9000,  33.837968, -0.004958},
			{10000,  33.837296, -0.005365},
		}
	}
	}, predefined_warhead("Mk_82"), "mk82AIR.png", "{Mk_82Y}")
	
Mk_Air({
	name = "Mk_83CT",
	model = "mk-82air",
	user_name= _("Mk-83CT"),
	
	mass = 454,
	Cx	= 0.00035,
	
	fm =
    {
		mass            = 454.0,
		caliber         = 0.356,
		cx_coeff        = {1.0, 0.29, 0.71, 0.14, 1.28},
		L               = 3.0,
		I               = 340.5,
		Ma              = 0.42,
		Mw              = 1.42,
		wind_time       = 1000,
		wind_sigma      = 150,
		
		cx_factor   	= 100,
    },
	
	control =	{open_delay = 0.2,},
	
	targeting_data = 
	{
		v0 = 200,
		data = 
		{
			{1, 20.3, -0}, 
			{10, 26.848369220695, 0.0065198779048461}, 
			{20, 28.090804322839, -0.0044561291081538}, 
			{30, 28.890559605394, -0.00063475144506512}, 
			{40, 29.394303712732, -0.003911407227598}, 
			{50, 29.66696339575, -0.0023667109549778}, 
			{60, 29.899647029484, -0.0018743877156784}, 
			{70, 30.041485104879, -0.0036344620048457}, 
			{80, 30.152632490286, -0.0030566776881518}, 
			{90, 30.255106602708, -0.0045496879997425}, 
			{100, 30.360381430637, -0.0045112754512532}, 
			{200, 30.929791881257, -0.0038920509545615}, 
			{300, 31.211180889225, -0.0037145379232542}, 
			{400, 31.368451492498, -0.0036316271890217}, 
			{500, 31.480282832596, -0.0035485703513668}, 
			{600, 31.560361812376, -0.0035419995561879}, 
			{700, 31.631150870344, -0.0034911914870077}, 
			{800, 31.686835425738, -0.0034614329992723}, 
			{900, 31.729974864076, -0.0034250262680168}, 
			{1000, 31.769585576517, -0.003412514245643}, 
			{1100, 31.803255013264, -0.0033504414844969}, 
			{1200, 31.830150749018, -0.0033292338713832}, 
			{1300, 31.85454582317, -0.0032807784294834}, 
			{1400, 31.876737974733, -0.0032695802470935}, 
			{1500, 31.895529431507, -0.0032316010769656}, 
			{1600, 31.912919279573, -0.003202915505944}, 
			{1700, 31.928940048331, -0.0031804297773856}, 
			{1800, 31.941997399355, -0.0031670678572907}, 
			{1900, 31.954303841804, -0.0031458601638285}, 
			{2000, 31.96522802755, -0.0031264382740315}, 
			{3000, 32.037583315793, -0.0030430934924432}, 
			{4000, 32.077208972679, -0.0030443428718279}, 
			{5000, 32.104632243164, -0.0030956723852699}, 
			{6000, 32.124815008809, -0.0031884094187045}, 
			{7000, 32.138842090829, -0.0033232805628381}, 
			{8000, 32.146664560993, -0.0035043103145522}, 
			{9000, 32.147791205826, -0.003737869053341}, 
			{10000, 32.141441485971, -0.0040325264845922}, 
		}
	}
	}, predefined_warhead("Mk_83"), "mk82AIR.png", "{Mk_83CT}")
	
	
bombs_data =
{
	["MK-82"]			= {name = "Mk-82",			mass = 241, wsType = {4, 5, 9, 31},  Cx = 0.00025, picture = "mk82.png"},
	["MK-82_Snakeye"]	= {name = "Mk-82 SnakeEye",	mass = 232, wsType = {4, 5, 9, 79},  Cx = 0.00035, picture = "mk82AIR.png"},
	["MK-82Y"]			= {name = "Mk-82Y",			mass = 232, wsType = MK_82Y.wsTypeOfWeapon,  Cx = 0.00035, picture = "mk82AIR.png"},
	["ROCKEYE"]			= {name = "Mk-20 Rockeye",	mass = 222, wsType = {4, 5, 38, 45}, Cx = 0.000413, picture = "Mk20.png"},
	["BDU-33"]			= {name = "BDU-33",			mass = 11.3, wsType = {4, 5, 9, 69}, Cx = 0.00025, picture = "bdu-33.png"},
	["MK-83"] 			= {name = "Mk-83", 			mass = 447, wsType = {4, 5, 9, 32}, Cx = 0.00035, picture = "mk83.png"},
	["GBU-12"] 			= {name = "GBU-12", 		mass = 275, wsType = {4, 5, 36, 38}, Cx = 0.000413, picture = "GBU12.png"},
	["GBU-16"] 			= {name = "GBU-16", 		mass = 564, wsType = {4, 5, 36, 39}, Cx = 0.000413, picture = "GBU16.png"},
	
	["GBU-31"] 			= {name = "GBU-31", 		mass = 894, wsType = {4, 5,	36,	85}, Cx = 0.000413, picture = "GBU31.png"},
	["GBU-31V"] 		= {name = "GBU-31V3B", 		mass = 981, wsType = {4, 5,	36,	92}, Cx = 0.000413, picture = "GBU-31V3B.png"},
	["GBU-38"] 			= {name = "GBU-38", 		mass = 241, wsType = {4, 5,	36,	86}, Cx = 0.00035, picture = "GBU38.png"},
}

local function bru_33_2x_bombs(element)
	local bomb_variant = bombs_data[element] or bombs_data["MK-82"]
	local data = {
		category		=	CAT_BOMBS,
		CLSID			=	"{BRU33_2X_"..element.."}",
		Picture			=	bomb_variant.picture,
		wsTypeOfWeapon	=	bomb_variant.wsType,
		displayName		=  _("BRU-33 - 2 x "..bomb_variant.name),
		attribute		=	{wsType_Weapon,wsType_Bomb,wsType_Container,WSTYPE_PLACEHOLDER},
		Cx_pil			=	0.00244140625 + bomb_variant.Cx,		-- TODO
		Count			=	2,
		Weight			=	bru_33VER_mass + 2 * bomb_variant.mass,			-- TODO
		Elements		= {	{ShapeName	= "BRU_33A",	IsAdapter  	   = true},
							{ShapeName	= element,		connector_name = "Point02"},
							{ShapeName	= element,		connector_name = "Point01"}
		}, -- end of Elements
	}
	declare_loadout(data)
end

bru_33_2x_bombs("MK-82")
bru_33_2x_bombs("MK-82_Snakeye")
bru_33_2x_bombs("MK-82Y")
bru_33_2x_bombs("ROCKEYE")
bru_33_2x_bombs("MK-83")
bru_33_2x_bombs("GBU-12")
bru_33_2x_bombs("GBU-16")

local function bru_55_with_2x(element)
	local bomb_variant = bombs_data[element] or bombs_data["GBU-38"]
	local data = {
		category		=	CAT_BOMBS,
		CLSID			=	"{BRU55_2*"..element.."}",
		Picture			=	bomb_variant.picture,
		wsTypeOfWeapon	=	bomb_variant.wsType,
		displayName		=  _("BRU-55 - 2 x "..bomb_variant.name),
		attribute		=	{wsType_Weapon,wsType_Bomb,wsType_Container,WSTYPE_PLACEHOLDER},
		Cx_pil			=	0.00244140625 + 2*bomb_variant.Cx,		-- TODO
		Count			=	2,
		Weight			=	bru_33VER_mass + 2*bomb_variant.mass,			-- TODO
		Elements		= {	{ShapeName	= "BRU_55",	IsAdapter  	   = true},
							{ShapeName	= element,		connector_name = "Point02"},
							{ShapeName	= element,		connector_name = "Point01"}
		}, -- end of Elements
	}
	declare_loadout(data)
end

bru_55_with_2x("GBU-38")

local function MER_BRU_41A_Bombs(element)
	local bomb_variant = bombs_data[element] or bombs_data["MK-82"]
	local data = {
		category		=	CAT_BOMBS,
		CLSID			=	"{BRU41_6X_"..element.."}",
		Picture			=	bomb_variant.picture,
		wsTypeOfWeapon	=	bomb_variant.wsType,
		displayName		=  _("BRU-41A - 6 x "..bomb_variant.name),
		attribute		=	{wsType_Weapon,wsType_Bomb,wsType_Container,WSTYPE_PLACEHOLDER},
		Cx_pil			=	bomb_variant.Cx,		-- TODO
		Count			=	6,
		Weight			=	bru_41MER_mass + 6 * bomb_variant.mass,			-- TODO
		Elements		= {	{ShapeName	= "BRU_41A",	IsAdapter  	   = true},
							{ShapeName	= element,		connector_name = "Point02"},
							{ShapeName	= element,		connector_name = "Point01"},
							{ShapeName	= element,		connector_name = "Point05"},
							{ShapeName	= element,		connector_name = "Point06"},
							{ShapeName	= element,		connector_name = "Point03"},
							{ShapeName	= element,		connector_name = "Point04"}
		}, -- end of Elements
	}
	declare_loadout(data)
end


MER_BRU_41A_Bombs("MK-82")
MER_BRU_41A_Bombs("BDU-33")



-- MBD3-U6 FABs 
local mbd3_u6_conn = 
{
	[1] = {Position = {-0.986625, -0.360123, -0.000000}  , Rotation = { 0,0,0} },-- задний нижний 
	[2] = {Position = { 0.898015, -0.360123, -0.000000}  , Rotation = { 0,0,0} },-- головной нижний 
	[3] = {Position = {-0.986493, -0.122812, -0.160811}  , Rotation = { 40,0,0} },-- задний левый 
	[4] = {Position = {-0.986493, -0.124806,  0.159743}  , Rotation = {-40,0,0} },-- задний правый
	[5] = {Position = { 0.898147, -0.122812, -0.160811}  , Rotation = { 40,0,0} },-- головной левый 
	[6] = {Position = { 0.898147, -0.124806,  0.159743}  , Rotation = {-40,0,0} },-- головной правый
}

local mbd3_u6_adapter =  {
	ShapeName	=	"mbd3-u6-68",
}

local function mbd3_u6_element(shape,i)
	return 	{
				Position	=	mbd3_u6_conn[i].Position,
				Rotation	=	mbd3_u6_conn[i].Rotation,	
				ShapeName	=	shape,
			}
end

local function mbd3_three_fwd(shape)
	return	{
		mbd3_u6_adapter,
		mbd3_u6_element(shape,6),	
		mbd3_u6_element(shape,5),
		mbd3_u6_element(shape,2),
	}
end

local function mbd3_four_fwd(shape)
	return	{
		mbd3_u6_adapter,
		mbd3_u6_element(shape,6),	
		mbd3_u6_element(shape,5),
		mbd3_u6_element(shape,2),
		mbd3_u6_element(shape,1),
	}
end

local function mbd3_five(shape)
	return	{
		mbd3_u6_adapter,
		mbd3_u6_element(shape,6),	
		mbd3_u6_element(shape,5),
		mbd3_u6_element(shape,4),
		mbd3_u6_element(shape,3),
		mbd3_u6_element(shape,2),
	}
end

declare_loadout({
		category 		= CAT_BOMBS,
		CLSID	 		= "{MBD3_U6_3*FAB-250_fwd}",
		attribute		= {4, 5, 32, WSTYPE_PLACEHOLDER},
		wsTypeOfWeapon	= {4, 5, 9, 6},
		Count 			= 3,
		Cx_pil			= 0.005,
		Picture			= "FAB250.png",
		displayName		= _("MER 6*3 FAB-250"),
		Weight			= 810,
		Elements		= mbd3_three_fwd("fab-250-n1"),
	})

declare_loadout({
		category 		= CAT_BOMBS,
		CLSID	 		= "{MBD3_U6_4*FAB-250_fwd}",
		attribute		= {4, 5, 32, WSTYPE_PLACEHOLDER},
		wsTypeOfWeapon	= {4, 5, 9, 6},
		Count 			= 4,
		Cx_pil			= 0.005,
		Picture			= "FAB250.png",
		displayName		= _("MER 6*4 FAB-250"),
		Weight			= 1060,
		Elements		= mbd3_four_fwd("fab-250-n1"),
	})
	
declare_loadout({
		category 		= CAT_BOMBS,
		CLSID	 		= "{MBD3_U6_5*FAB-250}",
		attribute		= {4, 5, 32, WSTYPE_PLACEHOLDER},
		wsTypeOfWeapon	= {4, 5, 9, 6},
		Count 			= 5,
		Cx_pil			= 0.005,
		Picture			= "FAB250.png",
		displayName		= _("MER 6*5 FAB-250"),
		Weight			= 1310,
		Elements		= mbd3_five("fab-250-n1"),
	})
	