declare_loadout({
	category 			= 	CAT_AIR_TO_AIR,
	CLSID 				= 	"{LAU-115 - AIM-120B}",
	Picture				=	"aim120.png",
	displayName			=	"LAU-115 - AIM-120B",
	wsTypeOfWeapon		=   {4,	4,	7,	24},
	attribute			=	{4,	4,	32,	WSTYPE_PLACEHOLDER},
	Cx_pil				=	0.001959765625,
	Count				=	1,
	Weight				=	54.4 + 157.8,
	Elements			=	
	{
		{	ShapeName	=	"LAU-115C"	   	  ,	IsAdapter  	   =   true  }, 
		{	payload_CLSID = "{C8E06185-7CD6-4C90-959F-044679E90751}" ,	connector_name =  "Point01"},
	}-- end of Elements
})

declare_loadout({
	category 			= 	CAT_AIR_TO_AIR,
	CLSID 				= 	"{LAU-115 - AIM-120C}",
	Picture				=	"aim120c.png",
	displayName			=	"LAU-115 - AIM-120C",
	wsTypeOfWeapon		=   {4,	4,	7,	106},
	attribute			=	{4,	4,	32,	WSTYPE_PLACEHOLDER},
	Cx_pil				=	0.001959765625,
	Count				=	1,
	Weight				=	54.4 + 161.5 ,
	Elements			=	
	{
		{	ShapeName	=	"LAU-115C"	   	  ,	IsAdapter  	   =   true  }, 
		{	payload_CLSID = "{40EF17B7-F508-45de-8566-6FFECC0C1AB8}" ,	connector_name =  "Point01"},
	}-- end of Elements
})

local aim120_variants =
{
	["AIM-120"]  		= {display_name = _("AIM-120B"), Picture	=	"aim120.png", wstype = {4,	4,	7	, 24 },	category = CAT_AIR_TO_AIR, mass = 157.8},
	["AIM-120C"] 		= {display_name = _("AIM-120C"), Picture	=	"aim120c.png", wstype = {4,	4,	7	, 106},	category = CAT_AIR_TO_AIR, mass = 161.5},
}

local function lau_115_2x127_amraam(clsid,element,left,right)
	local var 	   = aim120_variants[element] or aim120_variants["AIM-120"]
	local var_mass = var.mass or 85.5
	
	local ret = {
		category			=	var.category,
		CLSID				=	clsid,
		Picture				=	var.Picture,
		wsTypeOfWeapon		=	var.wstype,
		attribute			=	{4,	4,	32,	146},
		Cx_pil				=	0.001959765625,
		Elements 			= {{ShapeName	=	"LAU-115C+2_LAU127",IsAdapter  	   =   true  }}
	}
	if left then 	ret.Elements[#ret.Elements + 1] = {ShapeName	    =	 element,connector_name =	"Point03" }	end --rotation because LAU-127 have bugged connector pos
	if right then	ret.Elements[#ret.Elements + 1] = {ShapeName		=	 element,connector_name =	"Point02" }	end --rotation because LAU-127 have bugged connector pos
	
	local sz = #ret.Elements - 1
	ret.Count  = sz
	ret.Weight = 54.4 + 45.3 * sz + var_mass * sz

	if sz > 1 then
		ret.displayName =	_("LAU-115").." - 2 "..var.display_name
	else
		ret.displayName =	_("LAU-115").." "..var.display_name
	end
	declare_loadout(ret)
end

lau_115_2x127_amraam("LAU-115_2*LAU-127_AIM-120B"	,"AIM-120",true,true)
lau_115_2x127_amraam("LAU-115_2*LAU-127_AIM-120C"	,"AIM-120C",true,true)