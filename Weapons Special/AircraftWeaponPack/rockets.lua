local function RocketContainer(shape)
	return 	{{ ShapeName	=	shape , IsAdapter = true}}
end


rockets_data =
{
	["LAU-68_M151"]		= {name = "LAU-68 - 7 2.75' rockets M151 (HE)", payload_CLSID = "{A021F29D-18AB-4d3e-985C-FC9C60E35E9E}",	mass = 114.53, wsType = {4, 7,	33,	147}, Cx = 0.00146484375,  picture = "LAU68.png"},
	["LAU-68_MK5"]		= {name = "LAU-68 - 7 2.75' rockets MK5 (HE)",	payload_CLSID = "{174C6E6D-0C3D-42ff-BCB3-0853CB371F5C}",	mass = 103.2, wsType = {4, 7, 33, 145}, Cx = 0.00146484375,  picture = "LAU68.png"},
	["LAU-10"]			= {name = "LAU-10 - 4 ZUNI MK 71",		 		payload_CLSID = "{F3EFE0AB-E91A-42D8-9CA2-B63C91ED570A}",	mass = 440, wsType = {4, 7, 33, 37},  Cx = 0.001708984375, picture = "LAU10.png"},
	["LAU-61"]			= {name = "LAU-61 - 19 2.75' rockets MK151 HE",	payload_CLSID = "{FD90A1DC-9147-49FA-BF56-CB83EF0BD32B}",	mass = 290.59, wsType = {4, 7, 33, 145}, Cx = 0.001708984375, picture = "LAU61.png"},
}

declare_loadout({
				category		=	CAT_ROCKETS,
				CLSID			=	"{LAU_61R}",
				Picture			=	"LAU61.png",
				Cx_pil			=	0.001708984375,
				displayName		=	_("LAU-61R - 19 2.75' rockets MK151 HE"),
				Count			=	19,
				Elements		=	RocketContainer("LAU-61"),
				Weight			=	92.99+19*10.4,
				wsTypeOfWeapon	=	{4,	7,	33,	145},
				attribute		=	{4,	7,	32,	9},
})

declare_loadout({
				category		=	CAT_ROCKETS,
				CLSID			=	"{LAU_10R}",
				Picture			=	"LAU10.png",
				Cx_pil			=	0.001708984375,
				displayName		=	_("LAU-10R - 4 ZUNI MK 71"),
				Count			=	4,
				Weight			=	440,
				wsTypeOfWeapon	=	{4,	7,	33,	37},
				attribute		=	{4,	7,	32,	8},
				Elements		=	{
					{ ShapeName = "LAU-10" , IsAdapter = true},
					--rockets itself 
					{ ShapeName = "zuni", connector_name = "tube_01"},
					{ ShapeName = "zuni", connector_name = "tube_02"},
					{ ShapeName = "zuni", connector_name = "tube_03"},
					{ ShapeName = "zuni", connector_name = "tube_04"},
				},
})

local function bru_33_2xlau(clsid,element,left,right)
	local lau_variant = rockets_data[element] or rockets_data["LAU-68_M151"]
	local ret = {
		category			=	CAT_ROCKETS,
		CLSID				=	clsid,
		Picture				=	lau_variant.picture,
		wsTypeOfWeapon		=	lau_variant.wsType,
		attribute			=	{4,	7,	32,	WSTYPE_PLACEHOLDER},
		Cx_pil				=	0.00244140625,
		Elements 			= {{ShapeName	=	"BRU_33A",IsAdapter  	   =   true  },}
	}
	if left then 	ret.Elements[#ret.Elements + 1] = {payload_CLSID = lau_variant.payload_CLSID, connector_name =	"Point02"}	end 
	if right then	ret.Elements[#ret.Elements + 1] = {payload_CLSID = lau_variant.payload_CLSID, connector_name =	"Point01"}	end
	
	local sz = #ret.Elements - 1
	ret.Count  = sz
	ret.Weight = bru_33VER_mass +  sz * lau_variant.mass
	
	ret.Cx_pil = ret.Cx_pil + sz * lau_variant.Cx

	if sz > 1 then
		ret.displayName =	_("BRU-33").." - 2 "..lau_variant.name
	else
		ret.displayName =	_("BRU-33").." "..lau_variant.name
	end
	declare_loadout(ret)
end

bru_33_2xlau("{BRU33_2*LAU68}"	,	"LAU-68_M151",true,true)
bru_33_2xlau("{BRU33_LAU68}"	,	"LAU-68_M151",true, false)
bru_33_2xlau("{BRU33_LAU68_MK5}",	"LAU-68_MK5",true,false)
bru_33_2xlau("{BRU33_2*LAU68_MK5}",	"LAU-68_MK5",true,true)
bru_33_2xlau("{BRU33_LAU10}",		"LAU-10",true,false)
bru_33_2xlau("{BRU33_2*LAU10}",		"LAU-10",true,true)
bru_33_2xlau("{BRU33_LAU61}",		"LAU-61",true,false)
bru_33_2xlau("{BRU33_2*LAU61}",		"LAU-61",true,true)


declare_loadout({
	category 		= CAT_ROCKETS,
	CLSID	 		= "{TWIN_B13L_5OF}",
	wsTypeOfWeapon	= {4,	7,	33,	33},
	attribute		= {4,	4,	32,	WSTYPE_PLACEHOLDER},
	Count 			= 10,
	Cx_pil			= 0.0004,
	Picture			= "B13.png",
	displayName		=	"2x".._("B-13L - 5 S-13 OF"),
	Weight			= 32 + 2*(160+5*69),
	JettisonSubmunitionOnly = true,
	Elements		=	{
	   {ShapeName = "su-27-twinpylon",IsAdapter = true},
	   {payload_CLSID = "{FC56DF80-9B09-44C5-8976-DCFAFF219062}", connector_name = "S-25-L"},
	   {payload_CLSID = "{FC56DF80-9B09-44C5-8976-DCFAFF219062}", connector_name = "S-25-R"},
	}
})

declare_loadout({
	category 		= CAT_ROCKETS,
	CLSID	 		= "{TWIN_B_8M1_S_8KOM}",
	wsTypeOfWeapon	= {4,	7,	33,	32},
	attribute		= {4,	4,	32,	WSTYPE_PLACEHOLDER},
	Count 			= 40,
	Cx_pil			= 0.0004,
	Picture			= "B8V20A.png",
	displayName		=	"2x".._("B-8M1 - 20 S-8KOM"),
	Weight			= 32+ 2*(137.5 + 20 * 11.3),
	JettisonSubmunitionOnly = true,
	Elements		=	{
	   {ShapeName = "su-27-twinpylon",IsAdapter = true},
	   {payload_CLSID = "{F72F47E5-C83A-4B85-96ED-D3E46671EE9A}", DrawArgs = {{3,0.5}}, connector_name = "S-25-L"},
	   {payload_CLSID = "{F72F47E5-C83A-4B85-96ED-D3E46671EE9A}", DrawArgs = {{3,0.5}}, connector_name = "S-25-R"},
	}
})

declare_loadout({
	category 		= CAT_ROCKETS,
	CLSID	 		= "{TWIN_B_8M1_S_8_OFP2}",
	wsTypeOfWeapon	= {4,	7,	33,	155},
	attribute		= {4,	4,	32,	WSTYPE_PLACEHOLDER},
	Count 			= 40,
	Cx_pil			= 0.0004,
	Picture			= "B8V20A.png",
	displayName		=	"2x".._("B-8V20A - 20 S-8OFP2"),
	Weight			= 32+2*(137.5 + 20 * 16.7),
	JettisonSubmunitionOnly = true,
	Elements		=	{
	   {ShapeName = "su-27-twinpylon",IsAdapter = true},
	   {payload_CLSID = "B-8M1 - 20 S-8OFP2", DrawArgs = {{3,0.5}},connector_name = "S-25-L"},
	   {payload_CLSID = "B-8M1 - 20 S-8OFP2", DrawArgs = {{3,0.5}},connector_name = "S-25-R"},
	}
})

declare_loadout({
	category 		= CAT_ROCKETS,
	CLSID	 		= "{TWIN_B_8M1_S_8TsM}",
	wsTypeOfWeapon	= {4,	7,	33,	30},
	attribute		= {4,	4,	32,	WSTYPE_PLACEHOLDER},
	Count 			= 40,
	Cx_pil			= 0.0004,
	Picture			= "B8V20A.png",
	displayName		=	"2x".._("B-8V20A - 20 S-8TsM"),
	Weight			= 32+2*(137.5 + 20 * 11.1),
	JettisonSubmunitionOnly = true,
	Elements		=	{
	   {ShapeName = "su-27-twinpylon",IsAdapter = true},
	   {payload_CLSID = "{3DFB7320-AB0E-11d7-9897-000476191836}", DrawArgs = {{3,0.5}}, connector_name = "S-25-L"},
	   {payload_CLSID = "{3DFB7320-AB0E-11d7-9897-000476191836}", DrawArgs = {{3,0.5}}, connector_name = "S-25-R"},
	}
})
