ADM_141A =
{
	category		= CAT_MISSILES,
	name			= "ADM_141A",
	user_name		= _("ADM-141A"),
	scheme			= "glide_decoy",
	class_name		= "wAmmunitionDecoy",
	model			= "ADM_141",
	mass			= 180,
	
	wsTypeOfWeapon 	= {wsType_Weapon,wsType_Missile,wsType_AS_Missile,WSTYPE_PLACEHOLDER},

	Escort			= 0,
	Head_Type		= 5,
	sigma			= {20, 20, 20},
	M				= 180.0,
	H_max			= 12000.0,
	H_min			= -1,
	Diam			= 340.0,
	Cx_pil			= 8,
	D_max			= 120000.0,
	D_min			= 5000.0,
	Head_Form		= 0,
	Life_Time		= 100000,
	Nr_max			= 6,
	v_min			= 80.0,
	v_mid			= 200.0,
	Mach_max		= 0.95,
	t_b				= 0.0,
	t_acc			= 5.0,
	t_marsh			= 10000.0,
	Range_max		= 120000.0,
	H_min_t			= 500.0,
	Fi_start		= 1.0,
	Fi_rak			= 3.14152,
	Fi_excort		= 1.0,
	Fi_search		= 99.9,
	OmViz_max		= 99.9,
	X_back			= 0,
	Y_back			= 0,
	Z_back			= 0,
	Reflection		= 1.2,
	KillDistance	= 0.0,
	
	manualWeaponFlag = 7,

	add_attributes = {"Cruise missiles"},
	
	shape_table_data =
	{
		{
			name		= "ADM_141A",
			file		= "ADM_141",
			life		= 1,
			fire		= { 0, 1},
			username	= _("ADM-141A"),
			index		= WSTYPE_PLACEHOLDER,
		},
	},
	
	fm = {
		mass        = 180.0,  
		caliber     = 0.343,  
		cx_coeff    = {1,0.2,0.7,0.05,1.31},
		L           = 2.34,
		I           = 1 / 12 * 180.0 * 2.34 * 2.34,
		Ma          = 0.68,
		Mw          = 1.116,
		wind_sigma	= 0.0,
		wind_time	= 1000.0,
		Sw			= 1.1,
		dCydA		= {0.07, 0.036},
		A			= 0.5,
		maxAoa		= 0.3,
		finsTau		= 0.05,
		Ma_x		= 3,
		Ma_z		= 3,
		Kw_x		= 0.05,
		Kp_ret		= 4.5,
	},
	
	control_block ={
		seeker_activation_dist		= 0,
		default_cruise_height		= 500,
		obj_sensor					= 1,
		can_update_target_pos		= 0,
		turn_before_point_reach		= 1,
		turn_hor_N					= 0.8,
		turn_max_calc_angle_deg		= 90,
		turn_point_trigger_dist		= 100,
	},
	
	autopilot =		{
		delay				= 2.5,
		start_fins_vert_val	= -0.0015,
		Kpv					= 0.015,
		Kdv					= 1.0,
		Kiv					= 0.00002,
		Kp_hor_err			= 100,
		Kp_hor_err_croll	= 0.04,
		max_roll			= 0.7,
		glide_height		= 500,
		max_vert_speed		= 55,
		altim_vel_k			= 1,
		finsLimit			= 0.8,
		inertial_km_error	= 4,
		stab_vel			= 200,
		vel_save_k			= 1.7,
	},
	
	march = {
		impulse			= 0,
		fuel_mass		= 0,
		work_time		= 0,
		min_fuel_rate	= 0,
		min_thrust		= 0,
		max_thrust		= 0,
		thrust_Tau		= 0,
	},
	
	engine_control = {
		default_speed	= 0,
		K				= 0,
		Kd				= 0,
		Ki				= 0,
	},

	snare_block = {
		delay			= 0,
		activate_by_rad	= 0,
		spawn_pos		= {0,0,0},
		spawn_interval	= 0,
		sens_dist		= 0,
		chaff_mass		= 0,
		chaff_sum_mass	= 0,
	},
	
	warhead		= predefined_warhead("BDU"),
}

declare_weapon(ADM_141A)

declare_loadout({
	category 		= CAT_MISSILES,
	CLSID	 		= "{ADM_141A}",
	attribute		= ADM_141A.wsTypeOfWeapon,
	Count 			= 1,
	Cx_pil			= ADM_141A.Cx,
	Picture			= "agm154.png",
	displayName		= ADM_141A.user_name,
	Weight			= ADM_141A.mass,
	Elements  		= {{ShapeName = "ADM_141"}},
})

ADM_141B =
{
	category		= CAT_MISSILES,
	name			= "ADM_141B",
	user_name		= _("ADM-141B"),
	scheme			= "glide_decoy",
	class_name		= "wAmmunitionDecoy",
	model			= "ADM_141",
	mass			= 180,
	
	wsTypeOfWeapon 	= {wsType_Weapon,wsType_Missile,wsType_AS_Missile,WSTYPE_PLACEHOLDER},

	Escort			= 0,
	Head_Type		= 5,
	sigma			= {20, 20, 20},
	M				= 180.0,
	H_max			= 12000.0,
	H_min			= -1,
	Diam			= 340.0,
	Cx_pil			= 8,
	D_max			= 120000.0,
	D_min			= 5000.0,
	Head_Form		= 0,
	Life_Time		= 100000,
	Nr_max			= 6,
	v_min			= 80.0,
	v_mid			= 200.0,
	Mach_max		= 0.95,
	t_b				= 0.0,
	t_acc			= 5.0,
	t_marsh			= 10000.0,
	Range_max		= 120000.0,
	H_min_t			= 500.0,
	Fi_start		= 1.0,
	Fi_rak			= 3.14152,
	Fi_excort		= 1.0,
	Fi_search		= 99.9,
	OmViz_max		= 99.9,
	X_back			= 0,
	Y_back			= 0,
	Z_back			= 0,
	Reflection		= 0.9,
	KillDistance	= 0.0,
	
	manualWeaponFlag = 7,

	
	add_attributes = {"Cruise missiles"},
	
	shape_table_data =
	{
		{
			name		= "ADM_141B",
			file		= "ADM_141",
			life		= 1,
			fire		= { 0, 1},
			username	= _("ADM-141B"),
			index		= WSTYPE_PLACEHOLDER,
		},
	},
	
	fm = {
		mass        = 180.0,  
		caliber     = 0.34,  
		cx_coeff    = {1,0.2,0.7,0.05,1.31},
		L           = 2.34,
		I           = 1 / 12 * 180.0 * 2.34 * 2.34,
		Ma          = 0.68,
		Mw          = 1.116,
		wind_sigma	= 0.0,
		wind_time	= 1000.0,
		Sw			= 1.1,
		dCydA		= {0.07, 0.036},
		A			= 0.5,
		maxAoa		= 0.3,
		finsTau		= 0.05,
		Ma_x		= 3,
		Ma_z		= 3,
		Kw_x		= 0.05,
		Kp_ret		= 4.5,
	},
	

	control_block ={
		seeker_activation_dist		= 0,
		default_cruise_height		= 500,
		obj_sensor					= 1,
		can_update_target_pos		= 0,
		turn_before_point_reach		= 1,
		turn_hor_N					= 0.8,
		turn_max_calc_angle_deg		= 90,
		turn_point_trigger_dist		= 100,
	},
	
	autopilot =		{
		delay				= 2.5,
		start_fins_vert_val	= -0.0015,
		Kpv					= 0.015,
		Kdv					= 1.0,
		Kiv					= 0.00002,
		Kp_hor_err			= 100,
		Kp_hor_err_croll	= 0.04,
		max_roll			= 0.7,
		glide_height		= 500,
		max_vert_speed		= 55,
		altim_vel_k			= 1,
		finsLimit			= 0.8,
		inertial_km_error	= 4,
		stab_vel			= 200,
		vel_save_k			= 1.7,
	},
	
	march = {
		impulse			= 0,
		fuel_mass		= 0,
		work_time		= 0,
		min_fuel_rate	= 0,
		min_thrust		= 0,
		max_thrust		= 0,
		thrust_Tau		= 0,
	},
	
	engine_control = {
		default_speed	= 0,
		K				= 0,
		Kd				= 0,
		Ki				= 0,
	},

	snare_block = {
		delay			= 1,
		activate_by_rad	= 1,
		spawn_pos		= {-4,0,0},
		spawn_interval	= 0.5,
		sens_dist		= 30000,
		chaff_mass		= 0.05,
		chaff_sum_mass	= 36,
	},
	
	warhead		= predefined_warhead("BDU"),
}

declare_weapon(ADM_141B)

declare_loadout({
	category 		= CAT_MISSILES,
	CLSID	 		= "{ADM_141B}",
	attribute		= ADM_141B.wsTypeOfWeapon,
	Count 			= 1,
	Cx_pil			= ADM_141B.Cx,
	Picture			= "agm154.png",
	displayName		= ADM_141B.user_name,
	Weight			= ADM_141B.mass,
	Elements  		= {{ShapeName = "ADM_141"}},
})
